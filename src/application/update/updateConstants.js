// @flow strict

class UpdateServiceError extends Error {
  name: string = "Update Service Error";
}

export class UpdateEnableFailedError extends UpdateServiceError {
  message: string = "Failed to enable";
}

export class UpdateEnableCancelledError extends UpdateServiceError {
  message: string = "Failed to enable";
}

export class UpdateDisableFailedError extends UpdateServiceError {
  message: string = "Failed to disable";
}
