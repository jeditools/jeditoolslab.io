// @flow strict

import type { PracticeEntry } from "application/practice/practiceTypes";
import type { PracticeManagerService } from "application/practiceManager/practiceManagerTypes";

import { practiceServiceProd } from "../../practice/services/practiceServiceProd";

export const practiceManagerServiceProd: PracticeManagerService = {
  async getData(practiceId) {
    const practice = await practiceServiceProd.getPractice(practiceId);

    const practiceEntry: PracticeEntry = [practiceId, practice];

    return {
      practiceEntry,
    };
  },
};
