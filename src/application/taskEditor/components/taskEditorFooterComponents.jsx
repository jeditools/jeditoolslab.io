// @flow strict

import * as React from "react";

import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { CardComponent } from "ui/card/cardComponents";
import { useIntlDictionary } from "../../intl/intlHooks";

type Props = {
  disabled: boolean,
};

export const TaskEditorFooterComponent = (props: Props): React.Node => {
  const { disabled } = props;

  const { taskEditor: dictionary } = useIntlDictionary();

  return (
    <GridLayoutComponent mainAxis="x" primaryAlignment="end">
      <ActionControlComponent disabled={disabled} type="submit">
        <CardComponent floating>
          <BoxComponent>{dictionary.submitActionText}</BoxComponent>
        </CardComponent>
      </ActionControlComponent>
    </GridLayoutComponent>
  );
};
