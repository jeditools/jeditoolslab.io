// @flow strict

export type Dispatcher<EventType> = (event: EventType) => void;

export type CallbackDispatcher<EventType, ReturnType> = (
  callback: (dispatch: Dispatcher<EventType>) => ReturnType
) => ReturnType;
