// @flow strict

import * as React from "react";

import "./numberInputControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  placeholder: string,
  value: number,
  min?: number,
  max?: number,
  disabled?: boolean,
  autoFocus?: boolean,
  onValueChange(nextValue: number): void,
|};

export const NumberInputControlComponent = (props: Props): React.Node => {
  const { placeholder, value, min, max, disabled, autoFocus, onValueChange } =
    props;

  const inputRef = React.useRef<null | HTMLInputElement>(null);

  const handleChange = React.useCallback(
    (event: Event) => {
      const { target } = event;

      if (target instanceof HTMLInputElement) {
        onValueChange(Number(target.value));
      }
    },
    [onValueChange]
  );

  const classNameConstructor = createClassName("numberInputControl");

  React.useEffect(() => {
    const { current: inputElement } = inputRef;

    if (autoFocus && inputElement) {
      inputElement.focus();
    }
  }, [autoFocus]);

  const inputClassName = classNameConstructor("input");

  return (
    <input
      ref={inputRef}
      type="number"
      disabled={disabled}
      className={inputClassName}
      placeholder={placeholder}
      value={value}
      min={min}
      max={max}
      onChange={handleChange}
    />
  );
};
