// @flow strict

import * as React from "react";

import "./textControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  placeholder: string,
  value: string,
  disabled?: boolean,
  multiline?: boolean,
  autoFocus?: boolean,
  onValueChange(nextValue: string): void,
|};

export const TextControlComponent = (props: Props): React.Node => {
  const { placeholder, value, disabled, multiline, autoFocus, onValueChange } =
    props;

  const textAreaRef = React.useRef<null | HTMLTextAreaElement>(null);

  const inputRef = React.useRef<null | HTMLInputElement>(null);

  const handleChange = React.useCallback(
    (event: Event) => {
      const { target } = event;
      if (
        target instanceof HTMLInputElement ||
        target instanceof HTMLTextAreaElement
      ) {
        onValueChange(target.value);
      }
    },
    [onValueChange]
  );

  const classNameConstructor = createClassName("text-control");

  React.useEffect(() => {
    const { current: textAreaElement } = textAreaRef;
    const { current: inputElement } = inputRef;

    if (autoFocus) {
      if (textAreaElement) {
        textAreaElement.focus();
      }

      if (inputElement) {
        inputElement.focus();
      }
    }
  }, [autoFocus]);

  if (multiline) {
    const textAreaClassName = classNameConstructor("text-area");

    return (
      <textarea
        ref={textAreaRef}
        disabled={disabled}
        className={textAreaClassName}
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
      />
    );
  }

  const inputClassName = classNameConstructor("input");

  return (
    <input
      ref={inputRef}
      type="text"
      disabled={disabled}
      className={inputClassName}
      placeholder={placeholder}
      value={value}
      onChange={handleChange}
    />
  );
};
