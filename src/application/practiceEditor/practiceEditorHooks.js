// @flow strict

import type { PracticeId } from "../practice/practiceTypes";

import { useRouteMatch } from "react-router";
import { createActivityRoutePattern } from "../activity/activityUtils";

export const useEditedPracticeId = (): null | PracticeId => {
  const match = useRouteMatch(createActivityRoutePattern("PRACTICE_EDITING"));

  if (!match) {
    return null;
  }

  const {
    params: { practiceId },
  } = match;

  if (typeof practiceId === "string") {
    return practiceId;
  }

  return null;
};
