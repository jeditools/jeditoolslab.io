// @flow strict

export type ImportManagerDictionary = {|
  sectionLabelText: string,
  actionLabelText: string,
  descriptionText: string,
|};

export type ImportManagerService = {|
  uploadData(): Promise<void>,
|};
