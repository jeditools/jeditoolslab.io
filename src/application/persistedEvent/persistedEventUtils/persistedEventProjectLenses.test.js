// @flow strict

import type {
  PersistedEventEntries,
  PersistedEvent,
} from "application/persistedEvent/persistedEventTypes";

import { createEntriesConstructor } from "utility/entry/entryUtils";
import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createPersistedEventEntriesProjectLense } from "application/persistedEvent/persistedEventUtils/persistedEventProjectLenses";

// Project 1

const project1Id = createUniqueId();

const project1Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: Date.now(),
  projectId: project1Id,
  project: {
    name: "Project1",
    details: "",
  },
};

const project1Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: Date.now(),
  projectId: project1Id,
  project: {
    name: "Project1 Updated",
    details: "Project1 Details",
  },
};

const project1Deleted: PersistedEvent = {
  name: "ProjectDeleted",
  timestamp: Date.now(),
  projectId: project1Id,
};

// Project 2

const project2Id = createUniqueId();

const project2Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: Date.now(),
  projectId: project2Id,
  project: {
    name: "Project2",
    details: "",
  },
};

const project2Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: Date.now(),
  projectId: project2Id,
  project: {
    name: "Project2 Updated",
    details: "Project2 Details",
  },
};

const project2Deleted: PersistedEvent = {
  name: "ProjectDeleted",
  timestamp: Date.now(),
  projectId: project2Id,
};

// Task 1

const task1Id = createUniqueId();

const task1Created: PersistedEvent = {
  name: "TaskCreated",
  timestamp: Date.now(),
  taskId: task1Id,
  task: {
    description: "Task1",
    projectId: project1Id,
  },
};

const task1Updated: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: Date.now(),
  taskId: task1Id,
  task: {
    description: "Task1 Updated",
    projectId: project1Id,
  },
};

const task1Deleted: PersistedEvent = {
  name: "TaskDeleted",
  timestamp: Date.now(),
  taskId: task1Id,
};

describe("selectProjectRelatedEventEntries", () => {
  test("should select all project related entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), project2Deleted)
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task1Deleted)
        .return();

    const projectRelatedEventEntries = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProjectRelatedEventEntries();

    expect(projectRelatedEventEntries.map(([id, event]) => event)).toEqual([
      project1Created,
      project1Updated,
      project1Deleted,
      project2Created,
      project2Updated,
      project2Deleted,
    ]);
  });
  test("should select project 2 related entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), project2Deleted)
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task1Deleted)
        .return();

    const projectRelatedEventEntries = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProjectRelatedEventEntries(project2Id);

    expect(projectRelatedEventEntries.map(([id, event]) => event)).toEqual([
      project2Created,
      project2Updated,
      project2Deleted,
    ]);
  });
});

describe("selectProject", () => {
  test("should select created project", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project2Created)
        .return();

    const project = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProject(project1Id);

    expect(project).toEqual({
      name: "Project1",
      details: "",
      creationTimestamp: project1Created.timestamp,
    });
  });

  test("should select updated project", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project1Updated)
        .return();

    const project = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProject(project1Id);

    expect(project).toEqual({
      name: "Project1 Updated",
      details: "Project1 Details",
      creationTimestamp: project1Created.timestamp,
    });
  });

  test("should not select deleted project", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        // Artifact events
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .return();

    const project = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProject(project1Id);

    expect(project).toEqual(null);
  });
});

describe("selectProjectEntries", () => {
  test("should select all project entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project1Updated)
        .return();

    const projectEntries = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProjectEntries();

    expect(projectEntries.map(([id, project]) => project)).toEqual([
      {
        name: "Project1 Updated",
        details: "Project1 Details",
        creationTimestamp: project1Created.timestamp,
      },
      {
        name: "Project2",
        details: "",
        creationTimestamp: project2Created.timestamp,
      },
    ]);
  });
  test("should select all project entries except deleted ones", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        // Artifact events
        .setEntry(createUniqueId(), project1Created)
        .return();

    const projectEntries = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProjectEntries();

    expect(projectEntries.map(([id, project]) => project)).toEqual([
      {
        name: "Project2",
        details: "",
        creationTimestamp: project2Created.timestamp,
      },
    ]);
  });
});
