// @flow strict

export type SelectControlOption<ValueType> = {|
  value: ValueType,
  name: string,
  description?: string,
|};

export type SelectControl<ValueType> = {|
  selectedValues: ValueType[],
  options: SelectControlOption<ValueType>[],
  select(value: ValueType): void,
|};
