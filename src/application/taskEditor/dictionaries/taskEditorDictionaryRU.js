// @flow strict

import type { TaskEditorDictionary } from "application/taskEditor/taskEditorTypes";

export const taskEditorDictionaryRU: TaskEditorDictionary = {
  requiredFieldHintText: "Поле обязательно для заполнения",
  descriptionLabelText: "Описание",
  descriptionPlaceholderText: "Описание задачи",
  projectIdLabelText: "Проект",
  submitActionText: "Сохранить",
};
