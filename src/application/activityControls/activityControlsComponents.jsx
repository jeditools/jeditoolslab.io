// @flow strict

import * as React from "react";
import { useHistory, Switch, Route } from "react-router-dom";

import { TextComponent } from "ui/text/textComponents";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { useActivityControlsDictionary } from "application/activityControls/activityControlsHooks";
import {
  createActivityRoutePattern,
  createActivityRoute,
} from "application/activity/activityUtils";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { MenuIconComponent, ArrowBackIOSIconComponent } from "ui/icon";
import { CardComponent } from "ui/card/cardComponents";
import { useCurrentActivity } from "application/activity/activityHooks";
import { IndicatorComponent } from "../indicator/indicatorComponents.jsx";
import { InnerLinkControlComponent } from "../../ui/innerLinkControl/innerLinkControlComponents.jsx";

export const ActivityControlsComponent = (): React.Node => {
  const history = useHistory();

  const dictionary = useActivityControlsDictionary();

  const activity = useCurrentActivity();

  const goToNavigation = React.useCallback(() => {
    history.push(createActivityRoute({ name: "NAVIGATION" }));
  }, [history]);

  const prevActivityRoute = React.useMemo(() => {
    switch (activity.name) {
      case "TASK_MANAGEMENT":
        return createActivityRoute({
          name: "PROJECT_MANAGEMENT",
          projectId: activity.projectId,
        });
      case "TASK_EDITING":
        return createActivityRoute({
          name: "TASK_MANAGEMENT",
          projectId: activity.projectId,
          taskId: activity.taskId,
        });
      case "TASK_CREATION":
        return createActivityRoute({
          name: "PROJECT_MANAGEMENT",
          projectId: activity.projectId,
        });
      case "PROJECT_MANAGEMENT":
        return createActivityRoute({
          name: "PROJECTS_MANAGEMENT",
        });
      case "PROJECT_EDITING":
        return createActivityRoute({
          name: "PROJECT_MANAGEMENT",
          projectId: activity.projectId,
        });
      case "PRACTICE_CREATION":
        return createActivityRoute({
          name: "PRACTICES_MANAGEMENT",
        });
      case "PRACTICE_EDITING":
        return createActivityRoute({
          name: "PRACTICE_MANAGEMENT",
          practiceId: activity.practiceId,
        });
      case "PRACTICE_MANAGEMENT":
        return createActivityRoute({
          name: "PRACTICES_MANAGEMENT",
        });
      default:
        return "/";
    }
  }, [activity]);

  const linkLessTitlePaths = [
    createActivityRoutePattern("NAVIGATION"),
    createActivityRoutePattern("PROJECTS_MANAGEMENT"),
    createActivityRoutePattern("PRACTICES_MANAGEMENT"),
  ];

  const titleNode = React.useMemo(() => {
    return (
      <Switch>
        <Route path={createActivityRoutePattern("TASK_CREATION")}>
          {dictionary.taskCreationScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("TASK_MANAGEMENT")}>
          {dictionary.taskManagementScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("PROJECT_CREATION")}>
          {dictionary.projectCreationScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("PROJECT_MANAGEMENT")}>
          {dictionary.projectManagementScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("PROJECTS_MANAGEMENT")}>
          {dictionary.projectsManagementScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("PRACTICE_CREATION")}>
          {dictionary.practiceCreationScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("PRACTICE_MANAGEMENT")}>
          {dictionary.practiceManagementScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("PRACTICES_MANAGEMENT")}>
          {dictionary.practicesManagementScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("NAVIGATION")}>
          {dictionary.navigationScreenTitle}
        </Route>
        <Route path={createActivityRoutePattern("SETTINGS_MANAGEMENT")}>
          {dictionary.settingsManagementScreenTitle}
        </Route>
      </Switch>
    );
  }, [
    dictionary.navigationScreenTitle,
    dictionary.practiceCreationScreenTitle,
    dictionary.practiceManagementScreenTitle,
    dictionary.practicesManagementScreenTitle,
    dictionary.projectCreationScreenTitle,
    dictionary.projectManagementScreenTitle,
    dictionary.projectsManagementScreenTitle,
    dictionary.settingsManagementScreenTitle,
    dictionary.taskCreationScreenTitle,
    dictionary.taskManagementScreenTitle,
  ]);

  return (
    <GridLayoutComponent
      mainAxis="x"
      primaryAlignment="apart"
      secondaryAlignment="center"
    >
      <BoxComponent padding="vertical">
        <Switch>
          <Route path={linkLessTitlePaths}>
            <TextComponent size="l">{titleNode}</TextComponent>
          </Route>
          <Route>
            <InnerLinkControlComponent to={prevActivityRoute} replace>
              <GridLayoutComponent mainAxis="x" secondaryAlignment="center">
                <TextComponent>
                  <ArrowBackIOSIconComponent />
                </TextComponent>
                <TextComponent size="l">{titleNode}</TextComponent>
              </GridLayoutComponent>
            </InnerLinkControlComponent>
          </Route>
        </Switch>
      </BoxComponent>
      <GridLayoutComponent mainAxis="x" secondaryAlignment="center">
        <Switch>
          <Route path={createActivityRoutePattern("NAVIGATION")}>
            <BoxComponent>
              <IndicatorComponent />
            </BoxComponent>
          </Route>
          <Route path="/">
            <IndicatorComponent />
            <ActionControlComponent type="button" onClick={goToNavigation}>
              <CardComponent floating>
                <BoxComponent>
                  <MenuIconComponent />
                </BoxComponent>
              </CardComponent>
            </ActionControlComponent>
          </Route>
        </Switch>
      </GridLayoutComponent>
    </GridLayoutComponent>
  );
};
