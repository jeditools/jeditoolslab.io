// @flow strict

import type { PersistedEventService } from "application/persistedEvent/persistedEventTypes";

import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createEntriesConstructor } from "utility/entry/entryUtils";
import { persistedEventLocalStorageService } from "application/persistedEvent/persistedEventStorageServices/persistedEventLocalStorageServices";
import { persistedEventGoogleStorageService } from "application/persistedEvent/persistedEventStorageServices/persistedEventGoogleStorageServices";
import { createPersistedEventEntriesConstructor } from "application/persistedEvent/persistedEventUtils/persistedEventConstructors";

export const persistedEventProdService: PersistedEventService = {
  async addEvents(events) {
    const persistedEventEntries =
      await persistedEventLocalStorageService.read();

    const nextPersistedEventEntries = events
      .reduce((acc, event) => {
        const eventId = createUniqueId();

        return acc.setEntry(eventId, event);
      }, createEntriesConstructor(persistedEventEntries))
      .return();

    await persistedEventLocalStorageService.write(
      createPersistedEventEntriesConstructor(nextPersistedEventEntries)
        .sortInChronologicalOrder()
        .get()
    );
  },
  async getEventEntries() {
    return persistedEventLocalStorageService.read();
  },
  async setEventEntries(nextEntries) {
    await persistedEventLocalStorageService.write(nextEntries);
  },
  async sync() {
    const googleDriveEventEntries =
      await persistedEventGoogleStorageService.read();

    const localStorageEventEntries =
      await persistedEventLocalStorageService.read();

    const nextEntries = createPersistedEventEntriesConstructor(
      createEntriesConstructor(localStorageEventEntries)
        .mergeEntries(googleDriveEventEntries)
        .return()
    )
      .sortInChronologicalOrder()
      .cleanUpObsoleteEvents()
      .get();

    await Promise.all([
      persistedEventGoogleStorageService.write(nextEntries),
      persistedEventLocalStorageService.write(nextEntries),
    ]);
  },
};
