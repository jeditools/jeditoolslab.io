// @flow strict

import type { ExportManagerDictionary } from "application/exportManager/exportManagerTypes";

export const exportManagerDictRU: ExportManagerDictionary = {
  sectionLabelText: "Экспорт данных",
  actionLabelText: "Скачать данные",
  descriptionText:
    "Вы можете скачать данные, чтобы иметь возможность восстановить их в случае потери или перенeсти на другое устройство.",
};
