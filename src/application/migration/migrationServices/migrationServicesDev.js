// @flow strict

import type { MigrationService } from "application/migration/migrationTypes";

import { createMigrationService } from "application/migration/migrationUtils";
import { persistedEventDevService } from "application/persistedEvent/persistedEventServices/persistedEventDevServices";

export const migrationServiceDev: MigrationService = createMigrationService({
  persistedEventService: persistedEventDevService,
});
