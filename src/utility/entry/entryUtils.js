// @flow strict

import type {
  EntriesLense,
  EntriesConstructor,
} from "utility/entry/entryTypes";

export function createEntriesConstructor<IdType, ItemType>(
  entries: [IdType, ItemType][]
): EntriesConstructor<IdType, ItemType> {
  return {
    return() {
      return entries;
    },
    setEntry(id, item) {
      const map = new Map(entries);

      map.set(id, item);

      const nextEntries = Array.from(map);

      return createEntriesConstructor(nextEntries);
    },
    deleteEntry(id) {
      const map = new Map(entries);

      map.delete(id);

      const nextEntries = Array.from(map);

      return createEntriesConstructor(nextEntries);
    },
    mergeEntries(extraEntries) {
      const nextEntries = extraEntries
        .reduce((acc, [id, event]) => {
          return acc.setEntry(id, event);
        }, createEntriesConstructor(entries))
        .return();

      return createEntriesConstructor(nextEntries);
    },
  };
}

export function createEntriesLense<IdType, ItemType>(
  entries: [IdType, ItemType][]
): EntriesLense<IdType, ItemType> {
  return {
    selectItem(id) {
      const map = new Map(entries);

      return map.get(id) || null;
    },
    selectItems(ids) {
      return entries
        .filter(([id, item]) => {
          return ids.includes(id);
        })
        .map(([id, item]) => item);
    },
    selectAllItems() {
      const map = new Map(entries);

      return Array.from(map.values());
    },
    selectAllIds() {
      const map = new Map(entries);

      return Array.from(map.keys());
    },
  };
}
