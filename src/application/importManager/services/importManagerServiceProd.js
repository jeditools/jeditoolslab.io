// @flow strict

import type { ImportManagerService } from "application/importManager/importManagerTypes";

import { persistedEventProdService } from "application/persistedEvent/persistedEventServices/persistedEventProdServices";
import {
  createPersistedEventEntriesConstructor,
  typePersistedEventEntries,
} from "application/persistedEvent/persistedEventUtils/persistedEventConstructors";
import { fileService } from "utility/file/fileServices";

export const importManagerServiceProd: ImportManagerService = {
  async uploadData(params) {
    const files = await fileService.browse();

    for (let file of files) {
      const content = await fileService.readAsText(file);

      const parsedContent = JSON.parse(content);

      const initialPersistedEventEntries =
        typePersistedEventEntries(parsedContent);

      const persistedEventEntries = createPersistedEventEntriesConstructor(
        initialPersistedEventEntries
      )
        .cleanUpObsoleteEvents()
        .sortInChronologicalOrder()
        .get();

      await persistedEventProdService.setEventEntries(persistedEventEntries);
    }
  },
};
