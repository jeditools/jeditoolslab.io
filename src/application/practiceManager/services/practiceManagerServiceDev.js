// @flow strict

import type { PracticeEntry } from "application/practice/practiceTypes";
import type { PracticeManagerService } from "application/practiceManager/practiceManagerTypes";

import { devServicesAsyncTimeoutNormal } from "../../services/servicesUtils";
import { practiceServiceDev } from "../../practice/services/practiceServiceDev";

export const practiceManagerServiceDev: PracticeManagerService = {
  async getData(practiceId) {
    await devServicesAsyncTimeoutNormal.start();

    const practice = await practiceServiceDev.getPractice(practiceId);

    const practiceEntry: PracticeEntry = [practiceId, practice];

    return {
      practiceEntry,
    };
  },
};
