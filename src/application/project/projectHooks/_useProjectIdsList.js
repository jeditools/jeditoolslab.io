// @flow strict

import type { ProjectId } from "../projectTypes";

import React from "react";

import { createEntriesLense } from "utility/entry/entryUtils";
import { useState } from "../../state/stateHooks";

export const useProjectIdsList = (): ProjectId[] => {
  const { projectByIdCache } = useState();

  return React.useMemo(() => {
    return createEntriesLense(projectByIdCache.readEntries()).selectAllIds();
  }, [projectByIdCache]);
};
