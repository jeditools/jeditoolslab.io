// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { useSyncEnabled, useSyncInProgress } from "application/sync/syncHooks";
import { InnerLinkControlComponent } from "ui/innerLinkControl/innerLinkControlComponents";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import { useEventDispatcher } from "../../event/eventHooks";
import { createActivityRoute } from "../../activity/activityUtils";
import { ProjectsManagerItemComponent } from "./projectsManagerItemComponents.jsx";
import { useServices } from "../../services/servicesHooks";
import { useProjectLatestCompletionEntries } from "../../project/projectHooks/_useProjectLatestCompletionEntries";
import { useIntlDictionary } from "../../intl/intlHooks";
import { useSafeAsyncController } from "../../../utility/safeAsyncController/safeAsyncControllerHooks";

export const ProjectsManagerComponent = (): React.Node => {
  const dispatch = useEventDispatcher();

  const { wrapAsyncSafely } = useSafeAsyncController();

  const { repositoryService } = useServices();

  const syncEnabled = useSyncEnabled();

  const syncInProgress = useSyncInProgress();

  React.useLayoutEffect(() => {
    const promise = repositoryService
      .getData({ allProjects: true, allTasks: true })
      .then((data) => {
        dispatch({
          name: "PROJECTS_MANAGER__DATA_RECIEVED",
          data,
        });
      });

    wrapAsyncSafely(promise);

    // WARNING: implicit syncInProgress dependency
  }, [
    dispatch,
    wrapAsyncSafely,
    repositoryService,
    syncEnabled,
    syncInProgress,
  ]);

  const { projectsManager: intl } = useIntlDictionary();

  const projectIds = useProjectLatestCompletionEntries();

  return (
    <GridLayoutComponent mainAxis="y">
      <InlineLayoutComponent>
        <InnerLinkControlComponent
          to={createActivityRoute({ name: "PROJECT_CREATION" })}
        >
          <TextComponent color="accent">
            {intl.newProjectLinkText}
          </TextComponent>
        </InnerLinkControlComponent>
      </InlineLayoutComponent>
      {projectIds.length === 0 && (
        <GridLayoutComponent mainAxis="y">
          <TextComponent color="secondary">{intl.noProjectsText}</TextComponent>
        </GridLayoutComponent>
      )}
      {projectIds.length > 0 && (
        <GridLayoutComponent mainAxis="y">
          {projectIds.map(([projectId, latestCompletionTimestamp]) => (
            <ProjectsManagerItemComponent
              key={projectId}
              projectId={projectId}
              latestCompletionTimestamp={latestCompletionTimestamp}
            />
          ))}
        </GridLayoutComponent>
      )}
    </GridLayoutComponent>
  );
};
