// @flow strict

export class FileError extends Error {
  name: string = "File Error";
}
