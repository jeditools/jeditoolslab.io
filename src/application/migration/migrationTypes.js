// @flow strict

export type MigrationService = {|
  complete(): Promise<void>,
|};
