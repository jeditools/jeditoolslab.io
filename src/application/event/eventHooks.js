// @flow strict

import type { EventDispatcher } from "application/event/eventTypes";

import * as React from "react";

import { EventDispatcherContext } from "application/event/eventContexts";

export const useEventDispatcher = (): EventDispatcher => {
  return React.useContext(EventDispatcherContext);
};
