// @flow strict

import type { PracticeId } from "../../practice/practiceTypes";

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { ArrowForwardIconComponent, WarningAmberIconComponent } from "ui/icon";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import { usePractice } from "../../practice/practiceHooks";
import { createActivityRoute } from "../../activity/activityUtils";
import { createPracticeLense } from "../../practice/practiceLenses";
import { useIntlDictionary } from "../../intl/intlHooks";
import { InnerLinkControlComponent } from "../../../ui/innerLinkControl/innerLinkControlComponents.jsx";

type Props = {|
  practiceId: PracticeId,
|};

export const PracticesManagerItemComponent = (props: Props): React.Node => {
  const { practiceId } = props;

  const { practicesManager: dictionary } = useIntlDictionary();

  const practice = usePractice(practiceId);

  const practiceManagementRoute = createActivityRoute({
    name: "PRACTICE_MANAGEMENT",
    practiceId,
  });

  const practiceLense = React.useMemo(() => {
    return createPracticeLense(practice);
  }, [practice]);

  const nextCompletionText = React.useMemo(() => {
    if (practiceLense.shouldBeCompletedToday()) {
      return null;
    }

    const date = practiceLense.selectNextCompletionDate();

    if (date) {
      return dictionary.nextCompletionText(date.getTime());
    }

    return dictionary.initialCompletionText;
  }, [dictionary, practiceLense]);

  const nextCompletionIcon = React.useMemo(() => {
    if (practiceLense.shouldBeAlreadyCompleted()) {
      return <WarningAmberIconComponent />;
    }

    return null;
  }, [practiceLense]);

  const nextCompletionTextColor = React.useMemo(() => {
    if (practiceLense.shouldBeAlreadyCompleted()) {
      return "critical";
    }

    return "secondary";
  }, [practiceLense]);

  return (
    <InnerLinkControlComponent to={practiceManagementRoute}>
      <BoxComponent padding="vertical">
        <GridLayoutComponent mainAxis="x" primaryAlignment="apart">
          <TextComponent>{practice.name}</TextComponent>
          <BoxComponent padding="horizontal">
            <TextComponent>
              <ArrowForwardIconComponent />
            </TextComponent>
          </BoxComponent>
        </GridLayoutComponent>
        {nextCompletionText !== null && (
          <TextComponent color={nextCompletionTextColor} size="s">
            <InlineLayoutComponent>
              {nextCompletionIcon}
              {nextCompletionIcon ? " " : null}
              {nextCompletionText}
            </InlineLayoutComponent>
          </TextComponent>
        )}
      </BoxComponent>
    </InnerLinkControlComponent>
  );
};
