// @flow strict

import type { PracticeService } from "application/practice/practiceTypes";
import type { PersistedEventService } from "application/persistedEvent/persistedEventTypes";

import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { PracticeNotFoundError } from "application/practice/practiceErrors";
import { createPersistedEventEntriesPracticeLense } from "application/persistedEvent/persistedEventUtils/persistedEventPracticeLenses";

export const createPracticeService = (
  persistedEventService: PersistedEventService
): PracticeService => ({
  async addPractice(practice, params) {
    const practiceId = createUniqueId();

    await persistedEventService.addEvents([
      {
        name: "PracticeCreated",
        timestamp: Date.now(),
        practiceId,
        practice: {
          name: practice.name,
          details: practice.details,
          regularityInDays: practice.regularityInDays,
        },
      },
    ]);

    return practiceId;
  },
  async getPractice(practiceId, params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    const practice = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPractice(practiceId);

    if (!practice) {
      throw new PracticeNotFoundError(practiceId);
    }

    return practice;
  },
  async getPracticeEntries(practiceIds, params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    return createPersistedEventEntriesPracticeLense(persistedEventEntries)
      .selectPracticeEntries()
      .filter(([practiceId]) => practiceIds.includes(practiceId));
  },
  async getAllPracticeEntries(params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    return createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPracticeEntries();
  },
  async deletePractice(practiceId, params) {
    await persistedEventService.addEvents([
      {
        name: "PracticeDeleted",
        timestamp: Date.now(),
        practiceId,
      },
    ]);
  },
  async putPractice(practiceId, practice, params) {
    await persistedEventService.addEvents([
      {
        name: "PracticeUpdated",
        timestamp: Date.now(),
        practiceId,
        practice: {
          name: practice.name,
          details: practice.details,
          regularityInDays: practice.regularityInDays,
        },
      },
    ]);
  },
  async completePractice(practiceId, params) {
    await persistedEventService.addEvents([
      {
        name: "PracticeCompleted",
        timestamp: Date.now(),
        practiceId,
      },
    ]);
  },
});
