// @flow strict

import type { Dispatcher } from "utility/dispatcher/dispatcherTypes";
import type { AppearanceManagerEvent } from "application/appearanceManager/appearanceManagerTypes";
import type { RootEvent } from "application/root/rootTypes";
import type { ProjectEditorEvent } from "../projectEditor/projectEditorTypes";
import type { ProjectManagerEvent } from "../projectManager/projectManagerTypes";
import type { ProjectsManagerEvent } from "../projectsManager/projectsManagerTypes";
import type { TaskEditorEvent } from "../taskEditor/taskEditorTypes";
import type { TaskManagerEvent } from "../taskManager/taskManagerTypes";
import type { PracticesManagerEvent } from "../practicesManager/practicesManagerTypes";
import type { PracticeEditorEvent } from "../practiceEditor/practiceEditorTypes";
import type { PracticeManagerEvent } from "../practiceManager/practiceManagerTypes";
import type { SyncManagerEvent } from "../syncManager/syncManagerTypes";
import type { ConnectionEvent } from "../connection/connectionTypes";
import type { SyncEvent } from "../sync/syncTypes";

export type Event =
  | ProjectEditorEvent
  | ProjectManagerEvent
  | ProjectsManagerEvent
  | TaskEditorEvent
  | TaskManagerEvent
  | PracticesManagerEvent
  | AppearanceManagerEvent
  | PracticeEditorEvent
  | PracticeManagerEvent
  | SyncManagerEvent
  | RootEvent
  | ConnectionEvent
  | SyncEvent;

export type EventDispatcher = Dispatcher<Event>;

export type CallbackEventDispatcher<ReturnType> = (
  dispatch: Dispatcher<Event>
) => ReturnType;
