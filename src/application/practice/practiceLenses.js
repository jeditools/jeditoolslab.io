// @flow strict

import type {
  Practice,
  PracticeLense,
} from "application/practice/practiceTypes";

import {
  addDays,
  isFuture,
  isPast,
  isToday,
  isTomorrow,
  isYesterday,
} from "date-fns";

export const createPracticeLense = (practice: Practice): PracticeLense => ({
  selectNextCompletionDate() {
    if (practice.latestCompletionTimestamp) {
      return addDays(
        new Date(practice.latestCompletionTimestamp),
        practice.regularityInDays
      );
    }

    return null;
  },
  shouldBeAlreadyCompleted() {
    const nextCompletionDate =
      createPracticeLense(practice).selectNextCompletionDate();

    return (
      !!nextCompletionDate &&
      !isToday(nextCompletionDate) &&
      isPast(nextCompletionDate)
    );
  },
  shouldBeCompletedYesterday() {
    const nextCompletionDate =
      createPracticeLense(practice).selectNextCompletionDate();

    return !!nextCompletionDate && isYesterday(nextCompletionDate);
  },
  shouldBeCompletedToday() {
    const nextCompletionDate =
      createPracticeLense(practice).selectNextCompletionDate();

    return !!nextCompletionDate && isToday(nextCompletionDate);
  },
  shouldBeCompletedTomorrow() {
    const nextCompletionDate =
      createPracticeLense(practice).selectNextCompletionDate();

    return !!nextCompletionDate && isTomorrow(nextCompletionDate);
  },
  shouldNotYetBeCompleted() {
    const nextCompletionDate =
      createPracticeLense(practice).selectNextCompletionDate();

    return (
      !!nextCompletionDate &&
      !isToday(nextCompletionDate) &&
      isFuture(nextCompletionDate)
    );
  },
});
