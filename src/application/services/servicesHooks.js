// @flow strict

import type { Services } from "application/services/servicesTypes";

import React from "react";

import { ServicesContext } from "application/services/servicesContexts";

export const useServices = (): Services => {
  return React.useContext(ServicesContext);
};
