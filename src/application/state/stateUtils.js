// @flow strict

import type { State } from "application/state/stateTypes";

import { createActivity } from "application/activity/activityUtils";
import { intlDictionaryRU } from "../intl/dictionaries/intlDictionaryRU";
import { DictionaryCache } from "@draftup/utility.js/dist/main";

export const createState = (): State => ({
  migrationComplete: false,
  sync: null,
  visible: false,
  online: false,
  updateStatus: "UNKNOWN",
  currentActivity: createActivity({
    name: "PROJECTS_MANAGEMENT",
  }),
  themeMode: "auto",
  intlDictionary: intlDictionaryRU,
  taskByIdCache: new DictionaryCache(),
  projectByIdCache: new DictionaryCache(),
  practiceByIdCache: new DictionaryCache(),
});
