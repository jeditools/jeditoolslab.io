// @flow strict

import type { Cache } from "./cacheTypes";

export function createCache<ValueType>(
  service: () => Promise<ValueType>
): Cache<ValueType> {
  let value: ValueType | Promise<void> | Error | void = undefined;

  const listeners = new Set();

  function notify() {
    listeners.forEach((listener) => {
      listener();
    });
  }

  const cache: Cache<ValueType> = {
    get() {
      if (value === undefined) {
        value = service()
          .then((nextValue) => {
            value = nextValue;
          })
          .catch((error) => {
            value = error;
          })
          .finally(() => {
            notify();
          });
      }

      return value;
    },

    invalidate() {
      value = undefined;

      notify();
    },

    listen(listener) {
      listeners.add(listener);

      return () => {
        listeners.delete(listener);
      };
    },
  };

  return cache;
}
