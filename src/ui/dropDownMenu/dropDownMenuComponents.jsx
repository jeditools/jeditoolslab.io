// @flow

import * as React from "react";

import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { TextComponent } from "ui/text/textComponents";

type Props = {|
  children: React.Node,
|};

export function DropDownMenuComponent(props: Props): React.Node {
  const { children } = props;

  return (
    <GridLayoutComponent mainAxis="x" gap="auto">
      {children}
    </GridLayoutComponent>
  );
}

type ItemProps = {|
  text: React.Node,
  onSelect(): void,
|};

export function DropDownMenuItemComponent(props: ItemProps): React.Node {
  const { text, onSelect } = props;

  return (
    <ActionControlComponent type="button" onClick={onSelect}>
      <TextComponent color="accent">{text}</TextComponent>
    </ActionControlComponent>
  );
}
