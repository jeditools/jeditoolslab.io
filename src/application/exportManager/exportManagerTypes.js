// @flow strict

export type ExportManagerDictionary = {|
  sectionLabelText: string,
  actionLabelText: string,
  descriptionText: string,
|};

export type ExportManagerService = {|
  downloadData(): Promise<void>,
|};
