// @flow strict

import type { Task } from "../../task/taskTypes";
import type { ProjectId } from "../projectTypes";

import React from "react";

import { useState } from "../../state/stateHooks";
import { createEntriesLense } from "utility/entry/entryUtils";

type ProjectLatestCompletionEntry = [ProjectId, null | number];

const reduceTasksToLatestCompletionTimestamp = (
  latestTimestamp: null | number,
  task: Task
) => {
  const { completionTimestamp } = task;

  if (!latestTimestamp) {
    return completionTimestamp;
  }

  if (completionTimestamp && completionTimestamp > latestTimestamp) {
    return completionTimestamp;
  }

  return latestTimestamp;
};

export const useProjectLatestCompletionEntries =
  (): ProjectLatestCompletionEntry[] => {
    const { projectByIdCache, taskByIdCache } = useState();

    return React.useMemo(() => {
      const allItems = projectByIdCache
        .readEntries()
        .reduce((acc, [projectId, project]) => {
          const tasks = createEntriesLense(taskByIdCache.readEntries())
            .selectAllItems()
            .filter((task) => task.projectId === projectId);

          const latestCompletionTimestamp = tasks.reduce(
            reduceTasksToLatestCompletionTimestamp,
            null
          );

          acc.push({ projectId, project, latestCompletionTimestamp });

          return acc;
        }, []);

      return allItems
        .sort(
          (
            { latestCompletionTimestamp: timestamp1, project: project1 },
            { latestCompletionTimestamp: timestamp2, project: project2 }
          ) => {
            if (!timestamp1 && !timestamp2) {
              return project1.creationTimestamp - project2.creationTimestamp;
            }

            if (!timestamp1) {
              return -1;
            }

            if (!timestamp2) {
              return 1;
            }

            return timestamp1 - timestamp2;
          }
        )
        .map(({ projectId, latestCompletionTimestamp }) => [
          projectId,
          latestCompletionTimestamp,
        ]);
    }, [projectByIdCache, taskByIdCache]);
  };
