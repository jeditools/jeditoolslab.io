// @flow strict

export const createTypedString = (value: any): string => {
  if (typeof value === "string") {
    return value;
  }

  throw new TypeError();
};
