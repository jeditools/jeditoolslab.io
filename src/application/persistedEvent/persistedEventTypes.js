// @flow strict

import type { Entries } from "utility/entry/entryTypes";
import type {
  PersistedEventV1,
  PersistedEventIdV1,
} from "application/persistedEvent/persistedEventV1Types";

import type { Practice, PracticeId } from "application/practice/practiceTypes";
import type { Task, TaskId } from "../task/taskTypes";
import type { Project, ProjectId } from "../project/projectTypes";

export type PersistedEvent = PersistedEventV1;

export type PersistedEventId = PersistedEventIdV1;

export type PersistedEventEntries = Entries<PersistedEventId, PersistedEvent>;

export type PersistedEventStorageService = {|
  write(entries: PersistedEventEntries): Promise<void>,
  read(): Promise<PersistedEventEntries>,
|};

export type PersistedEventService = {|
  addEvents(persistedEvents: PersistedEvent[]): Promise<void>,
  getEventEntries(): Promise<PersistedEventEntries>,
  setEventEntries(nextEntries: PersistedEventEntries): Promise<void>,
  sync(): Promise<void>,
|};

export type PersistedEventEntriesTaskLense = {|
  selectTaskRelatedEventEntries(taskId?: TaskId): PersistedEventEntries,
  selectTask(taskId: TaskId): null | Task,
  selectProjectRelatedTaskIds(projectId: ProjectId): TaskId[],
  selectTaskEntries(taskIds?: TaskId[]): [TaskId, Task][],
|};

export type PersistedEventEntriesProjectLense = {|
  selectProjectRelatedEventEntries(
    projectId?: ProjectId
  ): PersistedEventEntries,
  selectProject(projectId: ProjectId): null | Project,
  selectProjectEntries(): [ProjectId, Project][],
  selectDeletedProjectIds(): ProjectId[],
|};

export type PersistedEventEntriesPracticeLense = {|
  selectPracticeRelatedEventEntries(
    practiceId?: PracticeId
  ): PersistedEventEntries,
  selectPractice(projectId: PracticeId): null | Practice,
  selectPracticeEntries(): [PracticeId, Practice][],
|};

export type PersistedEventEntriesConstructor = {|
  sortInChronologicalOrder(): PersistedEventEntriesConstructor,
  cleanUpObsoleteEvents(): PersistedEventEntriesConstructor,
  get(): PersistedEventEntries,
|};
