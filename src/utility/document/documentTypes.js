// @flow strict

export type DocumentService = {|
  injectScript(src: string): Promise<void>,
  setThemeColor(color: string): Promise<void>,
  listenToVisibilityChange(listener: (visible: boolean) => void): () => void,
  listenToConnectionStatusChange(
    listener: (online: boolean) => void
  ): () => void,
|};
