// @flow strict

import type {
  PersistedEventEntries,
  PersistedEvent,
} from "application/persistedEvent/persistedEventTypes";

import { createEntriesConstructor } from "utility/entry/entryUtils";
import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createPersistedEventEntriesTaskLense } from "application/persistedEvent/persistedEventUtils/persistedEventTaskLenses";

// Project 1

const project1Id = createUniqueId();

const project1Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: Date.now(),
  projectId: project1Id,
  project: {
    name: "Project1",
    details: "",
  },
};

const project1Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: Date.now(),
  projectId: project1Id,
  project: {
    name: "Project1 Updated",
    details: "Project1 Details",
  },
};

const project1Deleted: PersistedEvent = {
  name: "ProjectDeleted",
  timestamp: Date.now(),
  projectId: project1Id,
};

// Project 2

const project2Id = createUniqueId();

const project2Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: Date.now(),
  projectId: project2Id,
  project: {
    name: "Project2",
    details: "",
  },
};

const project2Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: Date.now(),
  projectId: project2Id,
  project: {
    name: "Project2 Updated",
    details: "Project2 Details",
  },
};

const project2Deleted: PersistedEvent = {
  name: "ProjectDeleted",
  timestamp: Date.now(),
  projectId: project2Id,
};

// Task 1

const task1Id = createUniqueId();

const task1Created: PersistedEvent = {
  name: "TaskCreated",
  timestamp: Date.now(),
  taskId: task1Id,
  task: {
    description: "Task1",
    projectId: project1Id,
  },
};

const task1Updated: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: Date.now(),
  taskId: task1Id,
  task: {
    description: "Task1 Updated",
    projectId: project1Id,
  },
};

const task1Completed: PersistedEvent = {
  name: "TaskCompleted",
  timestamp: Date.now(),
  taskId: task1Id,
};

const task1Deleted: PersistedEvent = {
  name: "TaskDeleted",
  timestamp: Date.now(),
  taskId: task1Id,
};

// Task 2

const task2Id = createUniqueId();

const task2Created: PersistedEvent = {
  name: "TaskCreated",
  timestamp: Date.now(),
  taskId: task2Id,
  task: {
    description: "Task2",
    projectId: project2Id,
  },
};

const task2Updated: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: Date.now(),
  taskId: task2Id,
  task: {
    description: "Task2 Updated",
    projectId: project2Id,
  },
};

const task2UpdatedAndReattached: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: Date.now(),
  taskId: task2Id,
  task: {
    description: "Task2 Updated And Reattached",
    projectId: project1Id,
  },
};

const task2UpdatedOnceAgain: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: Date.now(),
  taskId: task2Id,
  task: {
    description: "Task2 Updated Once Again",
    projectId: project2Id,
  },
};

const task2Completed: PersistedEvent = {
  name: "TaskCompleted",
  timestamp: Date.now(),
  taskId: task2Id,
};

const task2Deleted: PersistedEvent = {
  name: "TaskDeleted",
  timestamp: Date.now(),
  taskId: task2Id,
};

describe("selectTaskRelatedEventEntries", () => {
  test("should select all project related entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), project2Deleted)
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task1Deleted)
        .return();

    const projectRelatedEventEntries = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskRelatedEventEntries();

    expect(projectRelatedEventEntries.map(([id, event]) => event)).toEqual([
      task1Created,
      task1Updated,
      task1Deleted,
    ]);
  });
  test("should select task 2 related entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task1Deleted)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), project2Deleted)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task2Updated)
        .setEntry(createUniqueId(), task2Deleted)
        .return();

    const projectRelatedEventEntries = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskRelatedEventEntries(task2Id);

    expect(projectRelatedEventEntries.map(([id, event]) => event)).toEqual([
      task2Created,
      task2Updated,
      task2Deleted,
    ]);
  });
});

describe("selectTaskEntries", () => {
  test("should select all task entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .return();

    const taskEntries = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskEntries();

    expect(taskEntries.map(([id, task]) => task)).toEqual([
      {
        creationTimestamp: task1Created.timestamp,
        description: "Task1 Updated",
        completionTimestamp: null,
        projectId: project1Id,
      },
      {
        creationTimestamp: task2Created.timestamp,
        description: "Task2",
        completionTimestamp: null,
        projectId: project2Id,
      },
    ]);
  });
  test("should select all task entries except deleted ones", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task2Deleted)
        // Artifact events
        .setEntry(createUniqueId(), task2Updated)
        .return();

    const taskEntries = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskEntries();

    expect(taskEntries.map(([id, task]) => task)).toEqual([
      {
        creationTimestamp: task1Created.timestamp,
        description: "Task1 Updated",
        completionTimestamp: null,
        projectId: project1Id,
      },
    ]);
  });
});

describe("selectProjectRelatedTaskIds", () => {
  test("should select all task ids for given project id", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task1Deleted)
        .return();

    const taskIds = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectProjectRelatedTaskIds(project2Id);

    expect(taskIds).toEqual([task2Id]);
  });

  test("should correctly select reattached task ids", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task2UpdatedAndReattached)
        .setEntry(createUniqueId(), task1Deleted)
        .return();

    const taskIds = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectProjectRelatedTaskIds(project1Id);

    expect(taskIds).toEqual([task1Id, task2Id]);
  });
});

describe("selectTask", () => {
  test("should select created task", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task1Id);

    expect(task).toEqual({
      creationTimestamp: task1Created.timestamp,
      description: "Task1",
      completionTimestamp: null,
      projectId: project1Id,
    });
  });

  test("should select updated task", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task1Id);

    expect(task).toEqual({
      creationTimestamp: task1Created.timestamp,
      description: "Task1 Updated",
      completionTimestamp: null,
      projectId: project1Id,
    });
  });

  test("should correctly select task updated more than once", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task2Updated)
        .setEntry(createUniqueId(), task2UpdatedOnceAgain)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task2Id);

    expect(task).toEqual({
      creationTimestamp: task2Created.timestamp,
      description: "Task2 Updated Once Again",
      completionTimestamp: null,
      projectId: project2Id,
    });
  });

  test("should not select deleted task", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), task2Deleted)
        // Artifact events
        .setEntry(createUniqueId(), task2Updated)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task2Id);

    expect(task).toEqual(null);
  });

  test("should correctly select reattached task", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task2Updated)
        .setEntry(createUniqueId(), task2UpdatedAndReattached)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task2Id);

    expect(task).toEqual({
      creationTimestamp: task2Created.timestamp,
      description: "Task2 Updated And Reattached",
      completionTimestamp: null,
      projectId: project1Id,
    });
  });
});

describe("selectTask (completed)", () => {
  test("should correctly select completed task", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), task1Completed)
        .setEntry(createUniqueId(), task2Created)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task1Id);

    expect(task).toEqual({
      creationTimestamp: task1Created.timestamp,
      description: "Task1",
      completionTimestamp: task1Completed.timestamp,
      projectId: project1Id,
    });
  });

  test("should correctly select updated and completed task", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task2Updated)
        .setEntry(createUniqueId(), task2Completed)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task2Id);

    expect(task).toEqual({
      creationTimestamp: task2Created.timestamp,
      description: "Task2 Updated",
      completionTimestamp: task2Completed.timestamp,
      projectId: project2Id,
    });
  });

  test("should correctly select reattached and completed task", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task2Updated)
        .setEntry(createUniqueId(), task2UpdatedAndReattached)
        .setEntry(createUniqueId(), task2Completed)
        .return();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(task2Id);

    expect(task).toEqual({
      creationTimestamp: task2Created.timestamp,
      description: "Task2 Updated And Reattached",
      completionTimestamp: task2Completed.timestamp,
      projectId: project1Id,
    });
  });
});
