// @flow strict

import * as React from "react";

import "./delimiterStyles.css";

import { createClassName } from "utility/className/classNameUtils";

export const DelimiterComponent = (): React.Node => {
  const className = createClassName("delimiter")();

  return <div className={className} />;
};
