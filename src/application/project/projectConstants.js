// @flow strict

import type { ProjectId } from "./projectTypes";

class ProjectError extends Error {
  name: string = "Project Error";
}

export class ProjectNotFoundError extends ProjectError {
  constructor(projectId: ProjectId) {
    super();

    this.message = `Project with id ${projectId} was not found.`;
  }
}

export class IllegalProjectModification extends TypeError {
  message: string = "Illegal project modification.";
}

export class ProjectWithNameAlreadyExistsError extends ProjectError {
  constructor(projectName: string) {
    super();

    this.message = `Project with name "${projectName}" already exists.`;
  }
}
