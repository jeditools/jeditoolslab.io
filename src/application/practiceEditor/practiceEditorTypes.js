// @flow
import type { Form } from "utility/form/formTypes";
import type {
  Practice,
  PracticeId,
  PracticeEntry,
} from "application/practice/practiceTypes";
import type { PracticeFormViolation } from "application/practiceForm/practiceFormTypes";

export type PracticeEditorDictionary = {|
  nameLabelText: string,
  nameRequiredHintText: string,
  namePlaceholderText: string,
  detailsLabelText: string,
  detailsPlaceholderText: string,
  regularityInDaysLabelText: string,
  regularityInDaysRequiredHintText: string,
  regularityInDaysPlaceholderText: string,
  nameTakenHintText: string,
  submitActionText: string,
|};

export type PracticeEditorForm = Form<Practice, PracticeFormViolation>;

export type PracticeEditorData = {|
  practiceEntry: PracticeEntry,
|};

type FormSubmitted = {|
  name: "PRACTICE_EDITOR__PRACTICE_SUBMITTED",
  practiceId: PracticeId,
|};

export type PracticeEditorEvent = FormSubmitted;

export type PracticeEditorService = {|
  submit(
    practiceId: null | PracticeId,
    practice: Practice
  ): Promise<PracticeId>,
|};
