// @flow strict

import type { Reducer } from "../../../utility/reducer/reducerTypes";
import type { Event } from "../../event/eventTypes";
import type { Task } from "../../task/taskTypes";

import { DictionaryCache } from "@draftup/utility.js";

import { createReducer } from "utility/reducer/reducerUtils";

export const taskByIdCacheReducer: Reducer<
  DictionaryCache<string, Task>,
  Event
> = createReducer((state, event) => {
  switch (event.name) {
    case "TASK_MANAGER__DATA_RECIEVED": {
      const { taskId, task } = event;

      return new DictionaryCache().writeValue(taskId, task);
    }
    case "TASK_MANAGER__TASK_DELETED": {
      return new DictionaryCache(state).deleteEntry(event.taskId);
    }
    case "PROJECTS_MANAGER__DATA_RECIEVED":
    case "PROJECT_MANAGER__DATA_RECIEVED": {
      return event.data.taskEntries.reduce(
        (acc, [id, task]) => acc.writeValue(id, task),
        new DictionaryCache(state)
      );
    }
    default:
      return state;
  }
});
