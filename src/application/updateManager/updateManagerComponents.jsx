// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { CardComponent } from "ui/card/cardComponents";
import { useServices } from "../services/servicesHooks";
import { SettingsManagerSectionComponent } from "../settingsManager/settingsManagerSectionComponents.jsx";
import { useIntlDictionary } from "../intl/intlHooks";
import { useSafeAsyncController } from "../../utility/safeAsyncController/safeAsyncControllerHooks";

export const UpdateManagerComponent = (): React.Node => {
  const { updateManager: dictionary } = useIntlDictionary();

  const { updateService } = useServices();

  const { wrapAsyncSafely } = useSafeAsyncController();

  const applyUpdate = React.useCallback(() => {
    wrapAsyncSafely(updateService.applyUpdate());
  }, [updateService, wrapAsyncSafely]);

  return (
    <SettingsManagerSectionComponent label={dictionary.sectionLabelText}>
      <GridLayoutComponent mainAxis="y">
        <TextComponent>{dictionary.descriptionText}</TextComponent>
        <GridLayoutComponent mainAxis="x" primaryAlignment="start">
          <ActionControlComponent type="button" onClick={applyUpdate}>
            <CardComponent floating>
              <BoxComponent>{dictionary.actionLabelText}</BoxComponent>
            </CardComponent>
          </ActionControlComponent>
        </GridLayoutComponent>
      </GridLayoutComponent>
    </SettingsManagerSectionComponent>
  );
};
