// @flow strict

import * as React from "react";

type Props = {|
  children: React.Node,
|};

export const LabelComponent = (props: Props): React.Node => {
  const { children } = props;

  return <label>{children}</label>;
};
