// @flow strict

import type {
  PersistedEventEntries,
  PersistedEvent,
} from "application/persistedEvent/persistedEventTypes";

import { createEntriesConstructor } from "utility/entry/entryUtils";
import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createPersistedEventEntriesConstructor } from "application/persistedEvent/persistedEventUtils/persistedEventConstructors";

const createTimestampMock = (() => {
  let timestamp = 0;

  return () => {
    return (timestamp += 1);
  };
})();

const project1Id = createUniqueId();

const project2Id = createUniqueId();

const task1Id = createUniqueId();

const task2Id = createUniqueId();

const project1Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: createTimestampMock(),
  projectId: project1Id,
  project: {
    name: "Project1",
    details: "",
  },
};

const project1Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: createTimestampMock(),
  projectId: project1Id,
  project: {
    name: "Project1 Updated",
    details: "Project1 Details",
  },
};

const project2Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: createTimestampMock(),
  projectId: project2Id,
  project: {
    name: "Project1 Updated",
    details: "Project1 Details",
  },
};

const project2UpdatedOnceAgain: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: createTimestampMock(),
  projectId: project2Id,
  project: {
    name: "Project1 Updated Once Again",
    details: "Project1 Details",
  },
};

const project1Deleted: PersistedEvent = {
  name: "ProjectDeleted",
  timestamp: createTimestampMock(),
  projectId: project1Id,
};

const project2Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: createTimestampMock(),
  projectId: project2Id,
  project: {
    name: "Project2",
    details: "",
  },
};

const task1Created: PersistedEvent = {
  name: "TaskCreated",
  timestamp: createTimestampMock(),
  taskId: task1Id,
  task: {
    description: "Task1",
    projectId: project1Id,
  },
};

const task1Updated: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: createTimestampMock(),
  taskId: task1Id,
  task: {
    description: "Task1 updated",
    projectId: project1Id,
  },
};

const task1Completed: PersistedEvent = {
  name: "TaskCompleted",
  timestamp: createTimestampMock(),
  taskId: task1Id,
};

const task1Deleted: PersistedEvent = {
  name: "TaskDeleted",
  timestamp: createTimestampMock(),
  taskId: task1Id,
};

const task2Created: PersistedEvent = {
  name: "TaskCreated",
  timestamp: createTimestampMock(),
  taskId: task2Id,
  task: {
    description: "Task2",
    projectId: project2Id,
  },
};

const task2Updated: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: createTimestampMock(),
  taskId: task2Id,
  task: {
    description: "Task2 Updated",
    projectId: project2Id,
  },
};

const task2UpdatedOnceAgain: PersistedEvent = {
  name: "TaskUpdated",
  timestamp: createTimestampMock(),
  taskId: task2Id,
  task: {
    description: "Task2 Updated Once Again",
    projectId: project2Id,
  },
};

describe("sortInChronologicalOrder", () => {
  test("should sort in chronological order", () => {
    const project1CreatedId = createUniqueId();
    const project1UpdatedId = createUniqueId();
    const project1DeletedId = createUniqueId();

    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(project1UpdatedId, project1Updated)
        .setEntry(project1DeletedId, project1Deleted)
        .setEntry(project1CreatedId, project1Created)
        .return();

    const sortedPersistedEventEntries = createPersistedEventEntriesConstructor(
      persistedEventEntries
    )
      .sortInChronologicalOrder()
      .get();

    expect(sortedPersistedEventEntries).toEqual([
      [project1CreatedId, project1Created],
      [project1UpdatedId, project1Updated],
      [project1DeletedId, project1Deleted],
    ]);
  });
});

describe("cleanUpObsoleteEvents", () => {
  test("should filter deleted project events", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), task1Completed)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task2Updated)
        .setEntry(createUniqueId(), project1Deleted)
        .setEntry(createUniqueId(), task2UpdatedOnceAgain)
        .return();

    const cleanedPersistedEventEntries = createPersistedEventEntriesConstructor(
      persistedEventEntries
    )
      .cleanUpObsoleteEvents()
      .get();

    expect(cleanedPersistedEventEntries.map(([id, event]) => event)).toEqual([
      project2Created,
      project2Updated,
      task2Created,
      task2Updated,
      project1Deleted,
      task2UpdatedOnceAgain,
    ]);
  });

  test("should filter deleted tasks events", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), task1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), task2Created)
        .setEntry(createUniqueId(), task1Updated)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), task1Deleted)
        .setEntry(createUniqueId(), project2UpdatedOnceAgain)
        .return();

    const cleanedPersistedEventEntries = createPersistedEventEntriesConstructor(
      persistedEventEntries
    )
      .cleanUpObsoleteEvents()
      .get();

    expect(cleanedPersistedEventEntries.map(([id, event]) => event)).toEqual([
      project1Created,
      project1Updated,
      project2Created,
      task2Created,
      project2Updated,
      task1Deleted,
      project2UpdatedOnceAgain,
    ]);
  });
});
