// @flow strict

export type Entries<IdType, ItemType> = [IdType, ItemType][];

export type EntriesLense<IdType, ItemType> = {|
  selectAllIds(): IdType[],
  selectItem(id: IdType): null | ItemType,
  selectItems(ids: IdType[]): ItemType[],
  selectAllItems(): ItemType[],
|};

export type EntriesConstructor<IdType, ItemType> = {|
  return(): Entries<IdType, ItemType>,
  setEntry(id: IdType, item: ItemType): EntriesConstructor<IdType, ItemType>,
  deleteEntry(id: IdType): EntriesConstructor<IdType, ItemType>,
  mergeEntries(
    extraEntries: Entries<IdType, ItemType>
  ): EntriesConstructor<IdType, ItemType>,
|};
