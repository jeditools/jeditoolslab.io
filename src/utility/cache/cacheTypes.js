// @flow strict

export type Cache<ValueType> = {|
  get(): ValueType | Promise<void> | Error,
  invalidate(): void,
  listen(listener: () => void): () => void,
|};
