// @flow strict

import type { PersistedProject } from "application/persistedEvent/persistedEventV1Types";

export type ProjectId = string;

export type Project = {|
  name: string,
  details: string,
  creationTimestamp: number,
|};

export type ProjectEntry = [ProjectId, Project];

export type ProjectConstructor = {|
  return(): Project,
  changeName(nextValue: string): ProjectConstructor,
  changeDetails(nextDetails: string): ProjectConstructor,
  changeCreationTimestamp(nextCreationTimestamp: number): ProjectConstructor,
|};

export type ProjectService = {|
  addProject(project: Project): Promise<ProjectId>,
  getProject(projectId: ProjectId): Promise<Project>,
  getProjectEntries(projectIds: ProjectId[]): Promise<ProjectEntry[]>,
  getAllProjectEntries(): Promise<ProjectEntry[]>,
  deleteProject(projectId: ProjectId): Promise<void>,
  putProject(projectId: ProjectId, project: Project): Promise<void>,
|};

export type ProjectNormalizer = {|
  toPersistedProject(): PersistedProject,
|};

export type ProjectCreationActivityName = "PROJECT_CREATION";

export type ProjectCreationActivity = {|
  name: ProjectCreationActivityName,
|};

export type ProjectManagementActivityName = "PROJECT_MANAGEMENT";

export type ProjectManagementActivity = {|
  name: ProjectManagementActivityName,
  projectId: ProjectId,
|};

export type ProjectEditingActivityName = "PROJECT_EDITING";

export type ProjectEditingActivity = {|
  name: ProjectEditingActivityName,
  projectId: ProjectId,
|};

export type ProjectsManagementActivityName = "PROJECTS_MANAGEMENT";

export type ProjectsManagementActivity = {|
  name: ProjectsManagementActivityName,
|};
