// @flow strict

import type { AppearanceManagerDictionary } from "application/appearanceManager/appearanceManagerTypes";

export const appearanceManagerDictionaryRU: AppearanceManagerDictionary = {
  themeLabelText: "Тема",
  selectOptionAutoLabelText: "Системная",
  selectOptionLightLabelText: "Светлая",
  selectOptionDarkLabelText: "Тёмная",
};
