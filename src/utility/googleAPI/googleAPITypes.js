// @flow strict

type PopupClosed = {|
  error: "popup_closed_by_user",
|};

type AccessDenied = {|
  error: "access_denied",
|};

type ImmediateFailed = {|
  error: "immediate_failed",
|};

export type GoogleAPISignInErrorObject =
  | PopupClosed
  | AccessDenied
  | ImmediateFailed;
