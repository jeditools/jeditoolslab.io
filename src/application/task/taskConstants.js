// @flow strict

import type { TaskId } from "application/task/taskTypes";

export class TaskNotFoundError extends Error {
  constructor(taskId: TaskId) {
    super(taskId);

    this.message = `Task with id ${taskId} was not found.`;
  }
}

export class IllegalTaskModification extends TypeError {
  message: string = "Illegal task modification.";
}
