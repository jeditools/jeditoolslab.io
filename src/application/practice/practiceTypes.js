// @flow strict

export type PracticeId = string;

export type Practice = {|
  name: string,
  details: string,
  regularityInDays: number,
  latestCompletionTimestamp: null | number,
|};

export type PracticeEntry = [PracticeId, Practice];

export type PracticeConstructor = {|
  get(): Practice,
  setName(nextValue: string): PracticeConstructor,
  setDetails(nextValue: string): PracticeConstructor,
  setRegularityInDays(nextValue: number): PracticeConstructor,
  setLatestCompletionTimestamp(nextValue: number): PracticeConstructor,
|};

export type PracticeService = {|
  addPractice(practice: Practice): Promise<PracticeId>,
  getPractice(practiceId: PracticeId): Promise<Practice>,
  getPracticeEntries(practiceIds: PracticeId[]): Promise<PracticeEntry[]>,
  getAllPracticeEntries(): Promise<PracticeEntry[]>,
  deletePractice(practiceId: PracticeId): Promise<void>,
  putPractice(practiceId: PracticeId, practice: Practice): Promise<void>,
  completePractice(practiceId: PracticeId): Promise<void>,
|};

export type PracticeLense = {|
  selectNextCompletionDate(): null | Date,
  shouldBeAlreadyCompleted(): boolean,
  shouldBeCompletedYesterday(): boolean,
  shouldBeCompletedToday(): boolean,
  shouldBeCompletedTomorrow(): boolean,
  shouldNotYetBeCompleted(): boolean,
|};

export type PracticesManagementActivityName = "PRACTICES_MANAGEMENT";

export type PracticesManagementActivity = {|
  name: PracticesManagementActivityName,
|};

export type PracticeManagementActivityName = "PRACTICE_MANAGEMENT";

export type PracticeManagementActivity = {|
  name: PracticeManagementActivityName,
  practiceId: string,
|};

export type PracticeCreationActivityName = "PRACTICE_CREATION";

export type PracticeCreationActivity = {|
  name: PracticeCreationActivityName,
|};

export type PracticeEditingActivityName = "PRACTICE_EDITING";

export type PracticeEditingActivity = {|
  name: PracticeEditingActivityName,
  practiceId: string,
|};
