// @flow strict

export class UnreachableCodeError extends TypeError {
  constructor(value: empty) {
    super();

    this.message = "Unreachable code error";
  }
}
