// @flow strict

export type GoogleDriveFileId = number;

export type GoogleDriveFile = {|
  id: GoogleDriveFileId,
  name: string,
|};

export type GoogleDriveService = {|
  getFileId(name: string): Promise<GoogleDriveFileId>,
  createFile(name: string): Promise<GoogleDriveFile>,
  writeFile(id: GoogleDriveFileId, content: string): Promise<void>,
  readFile(id: GoogleDriveFileId): Promise<string>,
  deleteFile(id: GoogleDriveFileId): Promise<void>,
|};
