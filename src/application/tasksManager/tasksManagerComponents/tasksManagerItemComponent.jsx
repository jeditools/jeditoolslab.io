// @flow strict

import type { TaskId } from "application/task/taskTypes";

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { DoneIconComponent, ArrowForwardIconComponent } from "ui/icon";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import { useTask } from "../../task/taskHooks";
import { createActivityRoute } from "../../activity/activityUtils";
import { useIntlDictionary } from "../../intl/intlHooks";
import { useManagedProjectId } from "../../projectManager/projectManagerHooks";
import { ParagraphComponent } from "../../../ui/paragraph/paragraphComponents.jsx";
import { InnerLinkControlComponent } from "../../../ui/innerLinkControl/innerLinkControlComponents.jsx";

type Props = {|
  taskId: TaskId,
|};

export const TaskItemComponent = (props: Props): React.Node => {
  const { taskId } = props;

  const projectId = useManagedProjectId();

  const task = useTask(taskId);

  const { tasksManager: intl } = useIntlDictionary();

  const taskManagementRoute = createActivityRoute({
    name: "TASK_MANAGEMENT",
    taskId,
    projectId,
  });

  return (
    <InnerLinkControlComponent to={taskManagementRoute}>
      <BoxComponent padding="vertical">
        <GridLayoutComponent mainAxis="x" primaryAlignment="apart">
          <TextComponent>
            <ParagraphComponent text={task.description} preview />
          </TextComponent>
          <BoxComponent padding="horizontal">
            <TextComponent>
              <ArrowForwardIconComponent />
            </TextComponent>
          </BoxComponent>
        </GridLayoutComponent>
        {task.completionTimestamp !== null && (
          <TextComponent color="secondary" size="s">
            <InlineLayoutComponent>
              <DoneIconComponent /> {intl.completedText}
            </InlineLayoutComponent>
          </TextComponent>
        )}
      </BoxComponent>
    </InnerLinkControlComponent>
  );
};
