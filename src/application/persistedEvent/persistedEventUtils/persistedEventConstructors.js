// @flow strict

import type {
  PersistedEvent,
  PersistedEventEntries,
  PersistedEventEntriesConstructor,
} from "application/persistedEvent/persistedEventTypes";

import { createPersistedEventEntriesTaskLense } from "application/persistedEvent/persistedEventUtils/persistedEventTaskLenses";
import { createPersistedEventEntriesProjectLense } from "application/persistedEvent/persistedEventUtils/persistedEventProjectLenses";
import { PersistedEventError } from "application/persistedEvent/persistedEventUtils/persistedEventErrors";
import { createEntriesConstructor } from "utility/entry/entryUtils";

export const createPersistedEventEntriesConstructor = (
  entries: PersistedEventEntries
): PersistedEventEntriesConstructor => ({
  sortInChronologicalOrder() {
    const nextEntries = entries.sort(([, eventA], [, eventB]) => {
      return eventA.timestamp - eventB.timestamp;
    });

    return createPersistedEventEntriesConstructor(nextEntries);
  },
  cleanUpObsoleteEvents() {
    let nextEntries = entries;

    // 1. Delete all deleted project related events

    const deletedProjectIds =
      createPersistedEventEntriesProjectLense(
        nextEntries
      ).selectDeletedProjectIds();

    const deletedProjectRelatedEventIds = deletedProjectIds.reduce(
      (eventIds, deletedProjectId) => {
        const projectRelatedEventEntries =
          createPersistedEventEntriesProjectLense(
            nextEntries
          ).selectProjectRelatedEventEntries(deletedProjectId);

        for (const [eventId, event] of projectRelatedEventEntries) {
          // WARNING: it's important to keep ProjectDeleted event
          // to delete project across all devices

          if (event.name !== "ProjectDeleted") {
            eventIds.push(eventId);
          }
        }

        const projectRelatedTaskIds =
          createPersistedEventEntriesTaskLense(
            nextEntries
          ).selectProjectRelatedTaskIds(deletedProjectId);

        for (const taskId of projectRelatedTaskIds) {
          const taskRelatedEventEtries =
            createPersistedEventEntriesTaskLense(
              nextEntries
            ).selectTaskRelatedEventEntries(taskId);

          for (const [eventId] of taskRelatedEventEtries) {
            eventIds.push(eventId);
          }
        }

        return eventIds;
      },
      []
    );

    nextEntries = deletedProjectRelatedEventIds
      .reduce((acc, eventId) => {
        return acc.deleteEntry(eventId);
      }, createEntriesConstructor(nextEntries))
      .return();

    // 2. Delete all deleted tasks events

    const deletedTasksEventIds = nextEntries
      .reduce((acc, [eventId, event]) => {
        if (event.name === "TaskDeleted") {
          acc.push(event.taskId);
        }

        return acc;
      }, [])
      .map((deletedTaskId) =>
        createPersistedEventEntriesTaskLense(nextEntries)
          .selectTaskRelatedEventEntries(deletedTaskId)
          // WARNING: it's important to keep TaskDeleted event
          // to delete other events across all devices
          .filter(([, event]) => event.name !== "TaskDeleted")
          .map(([eventId]) => eventId)
      )
      .flat(1);

    nextEntries = deletedTasksEventIds
      .reduce((acc, eventId) => {
        return acc.deleteEntry(eventId);
      }, createEntriesConstructor(nextEntries))
      .return();

    return createPersistedEventEntriesConstructor(nextEntries);
  },
  get() {
    return entries;
  },
});

export const typePersistedEvent = (input: any): PersistedEvent => {
  // TODO: implement typed contract
  return input;
};

export const typePersistedEventEntries = (
  input: any
): PersistedEventEntries => {
  if (Array.isArray(input)) {
    return input.map((item) => typePersistedEvent(item));
  }

  throw new PersistedEventError("Failed to parse");
};
