// @flow strict

import type { Event } from "../../event/eventTypes";
import type { ThemeMode } from "../../theme/themeTypes";

import { DEFAULT_THEME_MODE } from "../../theme/themeConstants";

export const themeModeReducer = (state: ThemeMode, event: Event): ThemeMode => {
  switch (event.name) {
    case "APPEARANCE_MANAGER__THEME_CHANGED":
      return event.nextTheme;
    case "ROOT__PERSISTED_THEME_MODE_RECIEVED":
      return event.persistedThemeMode || DEFAULT_THEME_MODE;
    default:
      return state;
  }
};
