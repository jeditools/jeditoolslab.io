// @flow strict

import type { ProjectEditorService } from "../projectEditorTypes";

import { devServicesAsyncTimeoutNormal } from "../../services/servicesUtils";
import { ProjectWithNameAlreadyExistsError } from "../../project/projectConstants";
import { projectServiceDev } from "../../project/services/projectServiceDev";

export const projectEditorServiceDev: ProjectEditorService = {
  async submit(editedProjectId, project, params) {
    await devServicesAsyncTimeoutNormal.start();

    const projectEntries = await projectServiceDev.getAllProjectEntries();

    const projectEntryWithSameName = projectEntries.find(
      ([, currentProject]) => currentProject.name === project.name
    );

    if (
      projectEntryWithSameName &&
      projectEntryWithSameName[0] !== editedProjectId
    ) {
      throw new ProjectWithNameAlreadyExistsError(project.name);
    }

    if (editedProjectId) {
      await projectServiceDev.putProject(editedProjectId, project);

      return editedProjectId;
    } else {
      return projectServiceDev.addProject(project);
    }
  },
};
