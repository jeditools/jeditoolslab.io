// @flow strict

import type { ProjectEntry } from "../../project/projectTypes";
import type { ProjectManagerService } from "application/projectManager/projectManagerTypes";

import { devServicesAsyncTimeoutNormal } from "../../services/servicesUtils";
import { projectServiceDev } from "../../project/services/projectServiceDev";
import { taskServiceDev } from "../../task/services/taskServiceDev";
import { createTaskEntriesLense } from "../../task/taskCreators";

export const projectManagerServiceDev: ProjectManagerService = {
  async getData(projectId) {
    await devServicesAsyncTimeoutNormal.start();

    const project = await projectServiceDev.getProject(projectId);

    const projectEntry: ProjectEntry = [projectId, project];

    const taskEntries = await taskServiceDev.getAllTaskEntries();

    const projectTaskEntries = taskEntries.filter(
      ([taskId, task]) => task.projectId === projectId
    );

    const activeTaskIds =
      createTaskEntriesLense(projectTaskEntries).selectActiveIds();

    const completedTaskIds =
      createTaskEntriesLense(projectTaskEntries).selectCompletedIds();

    return {
      projectEntry,
      taskEntries,
      activeTaskIds,
      completedTaskIds,
    };
  },
};
