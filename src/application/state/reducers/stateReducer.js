// @flow strict

import type { Event } from "../../event/eventTypes";
import type { State } from "../stateTypes";
import type { Reducer } from "utility/reducer/reducerTypes";

import { currentActivityReducer } from "../../activity/activityReducers";
import { onlineReducer } from "../../connection/connectionReducers";
import { migrationCompleteReducer } from "../../migration/migrationReducers";
import { practiceByIdCacheReducer } from "./practiceByIdCacheReducer";
import { projectByIdCacheReducer } from "./projectByIdReducer";
import { syncReducer } from "./syncReducer";
import { taskByIdCacheReducer } from "./taskByIdCacheReducer";
import { themeModeReducer } from "./themeModeReducer";
import { updateStatusReducer } from "./updateStatusReducer";
import { visibleReducer } from "./visibleReducer";
import { createReducer } from "utility/reducer/reducerUtils";

export const stateReducer: Reducer<State, Event> = createReducer(
  (state, event) => ({
    migrationComplete: migrationCompleteReducer(state.migrationComplete, event),
    intlDictionary: state.intlDictionary,
    visible: visibleReducer(state.visible, event),
    online: onlineReducer(state.online, event),
    sync: syncReducer(state.sync, event),
    updateStatus: updateStatusReducer(state.updateStatus, event),
    currentActivity: currentActivityReducer(state.currentActivity, event),
    taskByIdCache: taskByIdCacheReducer(state.taskByIdCache, event),
    themeMode: themeModeReducer(state.themeMode, event),
    projectByIdCache: projectByIdCacheReducer(state.projectByIdCache, event),
    practiceByIdCache: practiceByIdCacheReducer(state.practiceByIdCache, event),
  }),
  (diff, event) => {
    if (process.env.NODE_ENV === "development") {
      console.log(event, diff);
    }
  }
);
