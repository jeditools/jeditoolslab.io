// @flow strict

export type ThemeMode = "light" | "dark" | "auto";

export type ThemeService = {|
  getPersistedThemeMode(): Promise<null | ThemeMode>,
  setPersistedThemeMode(nextThemeMode: ThemeMode): Promise<void>,
|};
