// @flow strict

import type { ErrorMessageDictionary } from "application/errorMessage/errorMessageTypes";

export const errorMessageDictionaryRU: ErrorMessageDictionary = {
  generalErrorMessage: "Возникла непредвиденная ошибка.",
  taskNotFoundErrorMessage: "Задача не найдена.",
  projectNotFoundErrorMessage: "Проект не найден.",
  practiceNotFoundErrorMessage: "Практика не найдена.",
};
