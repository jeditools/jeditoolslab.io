// @flow strict

import type { ProjectId } from "../projectTypes";
import type { SelectControl } from "utility/selectControl/selectControlTypes";

import React from "react";

import { useState } from "../../state/stateHooks";
import {
  createSelectControlOption,
  createSelectControl,
} from "utility/selectControl/selectControlUtils";

export const useProjectSelectControl = (
  onSelect: (value: ProjectId) => void
): SelectControl<ProjectId> => {
  const { projectByIdCache } = useState();

  const options = React.useMemo(() => {
    return projectByIdCache.readEntries().map(([projectId, project]) => {
      return createSelectControlOption<ProjectId>({
        value: projectId,
        name: project.name,
      });
    });
  }, [projectByIdCache]);

  const control = React.useMemo(() => {
    return createSelectControl<ProjectId>({
      selectedValues: [],
      options,
      select: onSelect,
    });
  }, [onSelect, options]);

  return control;
};
