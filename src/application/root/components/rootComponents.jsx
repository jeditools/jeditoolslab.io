// @flow strict

import * as React from "react";
import { HashRouter as Router } from "react-router-dom";

import { ErrorBoundaryComponent } from "application/errorBoundary/errorBoundaryComponents";
import { RootErrorComponent } from "./rootErrorComponents.jsx";
import { RootViewComponent } from "./rootViewComponents.jsx";
import { RootScreenComponent } from "./rootScreenComponents.jsx";
import { StoreProviderComponent } from "../../store/storeComponents.jsx";

export const RootComponent = (): React.Node => {
  return (
    <ErrorBoundaryComponent
      fallbackView={RootErrorComponent}
      errorConstructorsToCatch={[Error]}
    >
      <Router>
        <StoreProviderComponent>
          <RootViewComponent>
            <RootScreenComponent />
          </RootViewComponent>
        </StoreProviderComponent>
      </Router>
    </ErrorBoundaryComponent>
  );
};
