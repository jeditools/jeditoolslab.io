// @flow strict

import * as React from "react";

import "./inlineLayoutStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {
  align?: "left" | "center" | "right",
  children: React.Node,
};

export const InlineLayoutComponent = (props: Props): React.Node => {
  const { children, align = "left" } = props;

  const className = createClassName("inline-layout")({ align });

  return <span className={className}>{children}</span>;
};
