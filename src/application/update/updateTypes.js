// @flow strict

export type UpdateStatus = "UNKNOWN" | "DOWNLOADED";

export type UpdateService = {|
  subscribe(callback: (status: UpdateStatus) => void): Promise<void>,
  applyUpdate(): Promise<void>,
|};
