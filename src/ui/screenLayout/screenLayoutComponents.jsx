// @flow strict

import * as React from "react";

import "./screenLayoutStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  children: React.Node,
|};

export const ScreenLayoutComponent = (props: Props): React.Node => {
  const { children } = props;

  const className = createClassName("screen-layout");

  const wrapperClassName = className("wrapper");

  const innerClassName = className("inner");

  return (
    <div className={wrapperClassName}>
      <div className={innerClassName}>{children}</div>
    </div>
  );
};
