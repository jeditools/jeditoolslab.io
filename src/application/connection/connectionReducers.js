// @flow strict

import type { Event } from "../event/eventTypes";

export const onlineReducer = (state: boolean, event: Event): boolean => {
  switch (event.name) {
    case "CONNECTION__STATUS_CHANGED":
      return event.online;
    default:
      return state;
  }
};
