// @flow strict

import type { Project, ProjectId } from "../projectTypes";

import React from "react";

import { useState } from "../../state/stateHooks";
import { createProject } from "../projectCreators";
import { createEntriesLense } from "utility/entry/entryUtils";

export const useProject = (projectId: ProjectId): Project => {
  const { projectByIdCache } = useState();

  return React.useMemo(() => {
    const project = createEntriesLense(
      projectByIdCache.readEntries()
    ).selectItem(projectId);

    return project || createProject(Date.now());
  }, [projectByIdCache, projectId]);
};
