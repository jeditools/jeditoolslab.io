// @flow strict

import type { CollectionCache } from "./collectionCacheTypes";

export function createCollectionCache<IdType, ValueType>(
  service: (id: IdType) => Promise<ValueType>
): CollectionCache<IdType, ValueType> {
  let collection: Map<IdType, ValueType | Promise<void> | Error> = new Map();

  const listeners = new Set();

  function notify(id: IdType) {
    listeners.forEach((listener) => {
      listener(id);
    });
  }

  const cache: CollectionCache<IdType, ValueType> = {
    get(id) {
      let value = collection.get(id);

      if (value === undefined) {
        const promise = service(id)
          .then((nextValue) => {
            collection.set(id, nextValue);
          })
          .catch((error) => {
            collection.set(id, error);
          })
          .finally(() => {
            notify(id);
          });

        collection.set(id, promise);

        value = promise;
      }

      return value;
    },

    invalidate(id) {
      collection.delete(id);

      notify(id);
    },

    listen(listener) {
      listeners.add(listener);

      return () => {
        listeners.delete(listener);
      };
    },
  };

  return cache;
}
