// @flow strict

import type { MigrationService } from "application/migration/migrationTypes";
import type { PersistedEventService } from "application/persistedEvent/persistedEventTypes";

type MigrationServiceCreatorParams = {
  persistedEventService: PersistedEventService,
};

export const createMigrationService = (
  deps: MigrationServiceCreatorParams
): MigrationService => ({
  async complete() {},
});
