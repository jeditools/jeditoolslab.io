// @flow strict

import type { SyncService } from "application/sync/syncTypes";
import type { DocumentService } from "utility/document/documentTypes";
import type { MigrationService } from "application/migration/migrationTypes";
import type { PersistedEventService } from "application/persistedEvent/persistedEventTypes";
import type { ProjectManagerService } from "../projectManager/projectManagerTypes";
import type { ProjectEditorService } from "../projectEditor/projectEditorTypes";
import type { RepositoryService } from "../repository/repositoryTypes";
import type { ExportManagerService } from "../exportManager/exportManagerTypes";
import type { ImportManagerService } from "../importManager/importManagerTypes";
import type { PracticeEditorService } from "../practiceEditor/practiceEditorTypes";
import type { PracticeService } from "../practice/practiceTypes";
import type { PracticeManagerService } from "../practiceManager/practiceManagerTypes";
import type { TaskService } from "../task/taskTypes";
import type { ThemeService } from "../theme/themeTypes";
import type { UpdateService } from "../update/updateTypes";
import type { TaskEditorService } from "../taskEditor/taskEditorTypes";
import type { ProjectService } from "../project/projectTypes";

export type Services = {|
  syncService: SyncService,
  documentService: DocumentService,
  taskService: TaskService,
  themeService: ThemeService,
  updateService: UpdateService,
  projectService: ProjectService,
  projectManagerService: ProjectManagerService,
  taskEditorService: TaskEditorService,
  projectEditorService: ProjectEditorService,
  repositoryService: RepositoryService,
  migrationService: MigrationService,
  exportManagerService: ExportManagerService,
  importManagerService: ImportManagerService,
  practiceEditorService: PracticeEditorService,
  practiceService: PracticeService,
  practiceManagerService: PracticeManagerService,
  persistedEventService: PersistedEventService,
|};
