// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { LabelComponent } from "ui/label/labelComponents";
import { SelectControlComponent } from "../../ui/selectControl/selectControlComponents.jsx";
import { SelectControlOptionComponent } from "../../ui/selectControl/selectControlOptionComponents.jsx";
import { useEventDispatcher } from "../event/eventHooks";
import { useServices } from "../services/servicesHooks";
import { useIntlDictionary } from "../intl/intlHooks";
import { useThemeMode } from "../theme/themeHooks";
import { normalizeStringToThemeMode } from "../theme/themeUtils";
import { SettingsManagerSectionComponent } from "../settingsManager/settingsManagerSectionComponents.jsx";
import {
  THEME_MODE__AUTO,
  THEME_MODE__DARK,
  THEME_MODE__LIGHT,
} from "../theme/themeConstants";
import { useAsyncProcessController } from "../../utility/asyncProcessController/asyncProcessControllerHooks";

export const AppearanceManagerComponent = (): React.Node => {
  const dispatch = useEventDispatcher();

  const { themeService } = useServices();

  const { appearanceManager: dictionary } = useIntlDictionary();

  const themeMode = useThemeMode();

  const { process, wrapAsync } = useAsyncProcessController();

  const handleThemeModeSelect = React.useCallback(
    (value) => {
      wrapAsync(
        Promise.resolve(normalizeStringToThemeMode(value)).then(
          (nextThemeMode) =>
            themeService.setPersistedThemeMode(nextThemeMode).then(() => {
              dispatch({
                name: "APPEARANCE_MANAGER__THEME_CHANGED",
                nextTheme: nextThemeMode,
              });
            })
        )
      );
    },
    [dispatch, themeService, wrapAsync]
  );

  return (
    <SettingsManagerSectionComponent label="Внешний вид">
      <LabelComponent>
        <GridLayoutComponent
          mainAxis="x"
          primaryAlignment="apart"
          secondaryAlignment="center"
        >
          <TextComponent>{dictionary.themeLabelText}</TextComponent>
          <BoxComponent>
            <SelectControlComponent
              disabled={process.stage.name === "PENDING"}
              selectedValue={themeMode}
              onSelect={handleThemeModeSelect}
            >
              <SelectControlOptionComponent value={THEME_MODE__AUTO}>
                {dictionary.selectOptionAutoLabelText}
              </SelectControlOptionComponent>
              <SelectControlOptionComponent value={THEME_MODE__DARK}>
                {dictionary.selectOptionDarkLabelText}
              </SelectControlOptionComponent>
              <SelectControlOptionComponent value={THEME_MODE__LIGHT}>
                {dictionary.selectOptionLightLabelText}
              </SelectControlOptionComponent>
            </SelectControlComponent>
          </BoxComponent>
        </GridLayoutComponent>
      </LabelComponent>
    </SettingsManagerSectionComponent>
  );
};
