// @flow strict

class DocumentServiceError extends Error {
  name: string = "Document Service Error";
}

export class DocumentServiceScriptInjectionError extends DocumentServiceError {
  name: string = "Failed to inject script";
}
