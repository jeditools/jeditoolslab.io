// @flow strict

import type { Event } from "../event/eventTypes";

export const migrationCompleteReducer = (
  state: boolean,
  event: Event
): boolean => {
  switch (event.name) {
    case "ROOT__MIGRATION_COMPLETE":
      return true;
    default:
      return state;
  }
};
