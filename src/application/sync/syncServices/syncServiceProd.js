// @flow strict

import type { SyncService } from "application/sync/syncTypes";

import { googleAuthService } from "utility/googleAuth/googleAuthServices";
import {
  SyncEnableFailedError,
  SyncEnableCancelledError,
  SyncDisableFailedError,
} from "application/sync/syncUtils";
import { GoogleAuthSignInError } from "utility/googleAuth/googleAuthUtils";
import { createSyncDisabled, createSyncEnabled } from "../syncUtils";

export const syncServiceProd: SyncService = {
  async read() {
    const signedIn = await googleAuthService.isSignedIn();

    if (signedIn) {
      return createSyncEnabled();
    }

    return createSyncDisabled();
  },
  async update(nextValue) {
    if (nextValue.status === "ENABLED") {
      try {
        await googleAuthService.signIn();
      } catch (error) {
        if (error instanceof GoogleAuthSignInError) {
          switch (error.reason) {
            case "CANCELLED":
              throw new SyncEnableCancelledError();
            default:
              throw new SyncEnableFailedError();
          }
        }

        throw error;
      }
    } else {
      try {
        await googleAuthService.signOut();
      } catch (error) {
        throw new SyncDisableFailedError();
      }
    }
  },
};
