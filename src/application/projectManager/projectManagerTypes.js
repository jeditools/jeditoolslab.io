// @flow

import type { ProjectEntry, ProjectId } from "../project/projectTypes";
import type { TaskEntry, TaskId } from "../task/taskTypes";

export type ProjectManagerData = {|
  projectEntry: ProjectEntry,
  taskEntries: TaskEntry[],
  completedTaskIds: TaskId[],
  activeTaskIds: TaskId[],
|};

type DataRecieved = {|
  name: "PROJECT_MANAGER__DATA_RECIEVED",
  data: ProjectManagerData,
|};

type ProjectDeleted = {|
  name: "PROJECT_MANAGER__PROJECT_DELETED",
  projectId: ProjectId,
|};

export type ProjectManagerEvent = DataRecieved | ProjectDeleted;

export type ProjectManagerDictionary = {|
  noActiveTasksText: string,
  newTaskLinkText: string,
  editActionText: string,
  deleteActionText: string,
  deleteConfirmText: string,
  hideCompletedTasksActionText(tasksCount: number): string,
  showCompletedTasksActionText(tasksCount: number): string,
|};

export type ProjectManagerService = {|
  getData(projectId: ProjectId): Promise<ProjectManagerData>,
|};
