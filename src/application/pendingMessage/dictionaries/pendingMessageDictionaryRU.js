// @flow strict

import type { PendingMessageDictionary } from "application/pendingMessage/pendingMessageTypes";

export const pendingMessageDictionaryRU: PendingMessageDictionary = {
  messageText: "Подождите...",
};
