// @flow strict

import type { PracticeService } from "application/practice/practiceTypes";

import { persistedEventDevService } from "application/persistedEvent/persistedEventServices/persistedEventDevServices";
import { createPracticeService } from "application/practice/practiceServiceCreators";

export const practiceServiceDev: PracticeService = createPracticeService(
  persistedEventDevService
);
