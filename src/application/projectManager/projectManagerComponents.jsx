// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import { DelimiterComponent } from "ui/delimiter/delimiterComponents";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { ExpandLessIconComponent, ExpandMoreIconComponent } from "ui/icon";
import { useSyncEnabled, useSyncInProgress } from "application/sync/syncHooks";
import { useEventDispatcher } from "../event/eventHooks";
import { createActivityRoute } from "../activity/activityUtils";
import { useServices } from "../services/servicesHooks";
import { TasksManagerListComponent } from "../tasksManager/tasksManagerComponents/tasksManagerListComponent.jsx";
import { useProject } from "../project/projectHooks/_useProject";
import { useProjectActiveTaskIds } from "../project/projectHooks/_useProjectActiveTaskIds";
import { useProjectCompletedTaskIds } from "../project/projectHooks/_useProjectCompletedTaskIds";
import { useIntlDictionary } from "../intl/intlHooks";
import { useManagedProjectId } from "./projectManagerHooks";
import { ParagraphComponent } from "../../ui/paragraph/paragraphComponents.jsx";
import { InnerLinkControlComponent } from "../../ui/innerLinkControl/innerLinkControlComponents.jsx";
import {
  DropDownMenuComponent,
  DropDownMenuItemComponent,
} from "ui/dropDownMenu/dropDownMenuComponents";
import { useHistory } from "react-router-dom";
import { useSafeAsyncController } from "../../utility/safeAsyncController/safeAsyncControllerHooks";

export const ProjectManagerComponent = (): React.Node => {
  const [shouldMountCompletedTasks, setShouldMountCompletedTasks] =
    React.useState(false);

  const history = useHistory();

  const dispatch = useEventDispatcher();

  const projectId = useManagedProjectId();

  const { wrapAsyncSafely } = useSafeAsyncController();

  const { projectManagerService, projectService } = useServices();

  const project = useProject(projectId);

  const activeTaskIds = useProjectActiveTaskIds(projectId);

  const completedTaskIds = useProjectCompletedTaskIds(projectId);

  const { projectManager: intl } = useIntlDictionary();

  const syncEnabled = useSyncEnabled();

  const syncInProgress = useSyncInProgress();

  const showCompletedTasks = React.useCallback(() => {
    setShouldMountCompletedTasks(true);
  }, []);

  const hideCompletedTasks = React.useCallback(() => {
    setShouldMountCompletedTasks(false);
  }, []);

  const editProject = React.useCallback(() => {
    history.push(createActivityRoute({ name: "PROJECT_EDITING", projectId }));
  }, [history, projectId]);

  const handleDeleteSucceeded = React.useCallback(() => {
    dispatch({
      name: "PROJECT_MANAGER__PROJECT_DELETED",
      projectId,
    });

    history.replace(createActivityRoute({ name: "PROJECTS_MANAGEMENT" }));
  }, [dispatch, history, projectId]);

  const deleteProject = React.useCallback(() => {
    const confirmed = window.confirm(intl.deleteConfirmText);

    if (confirmed) {
      wrapAsyncSafely(
        projectService.deleteProject(projectId).then(handleDeleteSucceeded)
      );
    }
  }, [
    handleDeleteSucceeded,
    wrapAsyncSafely,
    intl.deleteConfirmText,
    projectId,
    projectService,
  ]);

  React.useLayoutEffect(() => {
    const promise = projectManagerService.getData(projectId).then((data) => {
      dispatch({
        name: "PROJECT_MANAGER__DATA_RECIEVED",
        data,
      });
    });

    wrapAsyncSafely(promise);

    // WARNING: implicit syncInProgress dependency
  }, [
    dispatch,
    wrapAsyncSafely,
    syncEnabled,
    projectId,
    projectManagerService,
    syncInProgress,
  ]);

  return (
    <GridLayoutComponent mainAxis="y">
      <GridLayoutComponent mainAxis="y">
        <InlineLayoutComponent>
          <TextComponent size="l">{project.name}</TextComponent>
        </InlineLayoutComponent>
        {project.details && (
          <InlineLayoutComponent>
            <TextComponent>
              <ParagraphComponent text={project.details} />
            </TextComponent>
          </InlineLayoutComponent>
        )}

        <GridLayoutComponent
          mainAxis="x"
          primaryAlignment="apart"
          secondaryAlignment="center"
        >
          <DropDownMenuComponent>
            <DropDownMenuItemComponent
              text={intl.editActionText}
              onSelect={editProject}
            />
            <DropDownMenuItemComponent
              text={intl.deleteActionText}
              onSelect={deleteProject}
            />
          </DropDownMenuComponent>
        </GridLayoutComponent>
      </GridLayoutComponent>
      <DelimiterComponent />
      <GridLayoutComponent mainAxis="y">
        <InlineLayoutComponent>
          <InnerLinkControlComponent
            to={createActivityRoute({ name: "TASK_CREATION", projectId })}
          >
            <TextComponent color="accent">{intl.newTaskLinkText}</TextComponent>
          </InnerLinkControlComponent>
        </InlineLayoutComponent>
        {activeTaskIds.length === 0 && (
          <TextComponent color="secondary">
            {intl.noActiveTasksText}
          </TextComponent>
        )}
        {activeTaskIds.length > 0 && (
          <TasksManagerListComponent taskIds={activeTaskIds} />
        )}
      </GridLayoutComponent>
      {completedTaskIds.length > 0 && <DelimiterComponent />}
      {completedTaskIds.length > 0 && shouldMountCompletedTasks && (
        <ActionControlComponent type="button" onClick={hideCompletedTasks}>
          <BoxComponent>
            <GridLayoutComponent mainAxis="y" primaryAlignment="center">
              <TextComponent>
                {intl.hideCompletedTasksActionText(completedTaskIds.length)}
              </TextComponent>
              <TextComponent>
                <ExpandLessIconComponent />
              </TextComponent>
            </GridLayoutComponent>
          </BoxComponent>
        </ActionControlComponent>
      )}
      {completedTaskIds.length > 0 && !shouldMountCompletedTasks && (
        <ActionControlComponent type="button" onClick={showCompletedTasks}>
          <BoxComponent>
            <GridLayoutComponent mainAxis="y" primaryAlignment="center">
              <TextComponent>
                {intl.showCompletedTasksActionText(completedTaskIds.length)}
              </TextComponent>
              <TextComponent>
                <ExpandMoreIconComponent />
              </TextComponent>
            </GridLayoutComponent>
          </BoxComponent>
        </ActionControlComponent>
      )}
      {completedTaskIds.length > 0 && shouldMountCompletedTasks && (
        <GridLayoutComponent mainAxis="y">
          <TasksManagerListComponent taskIds={completedTaskIds} />
        </GridLayoutComponent>
      )}
    </GridLayoutComponent>
  );
};
