// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { useServices } from "application/services/servicesHooks";
import { useEventDispatcher } from "application/event/eventHooks";
import { SyncEnableCancelledError } from "application/sync/syncUtils";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { CardComponent } from "ui/card/cardComponents";
import { useSyncEnabled } from "application/sync/syncHooks";
import { SettingsManagerSectionComponent } from "../settingsManager/settingsManagerSectionComponents.jsx";
import { useIntlDictionary } from "../intl/intlHooks";
import { createSyncDisabled, createSyncEnabled } from "../sync/syncUtils";
import { useSafeAsyncController } from "../../utility/safeAsyncController/safeAsyncControllerHooks";

export const SyncManagerComponent = (): React.Node => {
  const { syncManager: dictionary } = useIntlDictionary();

  const { syncService } = useServices();

  const { wrapAsyncSafely } = useSafeAsyncController();

  const dispatch = useEventDispatcher();

  const syncEnabled = useSyncEnabled();

  const enable = React.useCallback(() => {
    wrapAsyncSafely(
      syncService
        .update(createSyncEnabled())
        .then(() => {
          dispatch({
            name: "SYNC_MANAGER__SYNC_ENABLED",
          });
        })
        .catch((error) => {
          const cancelled = error instanceof SyncEnableCancelledError;

          if (!cancelled) {
            throw error;
          }
        })
    );
  }, [syncService, dispatch, wrapAsyncSafely]);

  const disable = React.useCallback(() => {
    wrapAsyncSafely(
      syncService.update(createSyncDisabled()).then(() => {
        dispatch({
          name: "SYNC_MANAGER__SYNC_DISABLED",
        });
      })
    );
  }, [syncService, dispatch, wrapAsyncSafely]);

  const descriptionText = syncEnabled
    ? dictionary.enabledDescriptionText
    : dictionary.disabledDescriptionText;

  return (
    <SettingsManagerSectionComponent label={dictionary.sectionLabelText}>
      <GridLayoutComponent mainAxis="y">
        <TextComponent>{descriptionText}</TextComponent>
        <GridLayoutComponent mainAxis="x" primaryAlignment="start">
          {syncEnabled && (
            <ActionControlComponent type="button" onClick={disable}>
              <CardComponent floating>
                <BoxComponent>{dictionary.disableActionLabelText}</BoxComponent>
              </CardComponent>
            </ActionControlComponent>
          )}
          {!syncEnabled && (
            <ActionControlComponent type="button" onClick={enable}>
              <CardComponent floating>
                <BoxComponent>{dictionary.enableActionLabelText}</BoxComponent>
              </CardComponent>
            </ActionControlComponent>
          )}
        </GridLayoutComponent>
      </GridLayoutComponent>
    </SettingsManagerSectionComponent>
  );
};
