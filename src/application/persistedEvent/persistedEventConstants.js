// @flow strict

const BASE_STORAGE_KEY = "eventEntries";

export const PERSISTED_EVENT__STORAGE_KEY: string = `${BASE_STORAGE_KEY}_${
  process.env.NODE_ENV || "unknown"
}`;
