// @flow strict

export const isScriptInjected = (src: string): boolean => {
  const { head } = document;

  if (head) {
    const scripts = head.getElementsByTagName("script");

    return Array.from(scripts).some((script) => {
      return script.src === src;
    });
  } else {
    throw new TypeError();
  }
};

export const getMetaTagElementByName = (
  name: string
): HTMLMetaElement | null => {
  const { head } = document;

  if (head) {
    const elements = head.getElementsByTagName("meta");

    const element = Array.from(elements).find((element) => {
      return element.name === name;
    });

    return element || null;
  } else {
    throw new TypeError();
  }
};
