// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { CardComponent } from "ui/card/cardComponents";
import { FormControlComponent } from "ui/formControl/formControlComponents";
import { useIntlDictionary } from "../intl/intlHooks";
import { useServices } from "../services/servicesHooks";
import { SettingsManagerSectionComponent } from "../settingsManager/settingsManagerSectionComponents.jsx";
import { useSafeAsyncController } from "../../utility/safeAsyncController/safeAsyncControllerHooks";

export const ImportManagerComponent = (): React.Node => {
  const { importManager: dictionary } = useIntlDictionary();

  const { importManagerService } = useServices();

  const { wrapAsyncSafely } = useSafeAsyncController();

  const submitFormData = React.useCallback(() => {
    wrapAsyncSafely(importManagerService.uploadData());
  }, [importManagerService, wrapAsyncSafely]);

  return (
    <FormControlComponent onSubmit={submitFormData}>
      <SettingsManagerSectionComponent label={dictionary.sectionLabelText}>
        <GridLayoutComponent mainAxis="y">
          <TextComponent>{dictionary.descriptionText}</TextComponent>
          <GridLayoutComponent mainAxis="x" primaryAlignment="start">
            <ActionControlComponent type="submit">
              <CardComponent floating>
                <BoxComponent>{dictionary.actionLabelText}</BoxComponent>
              </CardComponent>
            </ActionControlComponent>
          </GridLayoutComponent>
        </GridLayoutComponent>
      </SettingsManagerSectionComponent>
    </FormControlComponent>
  );
};
