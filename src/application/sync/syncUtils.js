// @flow strict

import type { Sync } from "application/sync/syncTypes";

export const createSyncEnabled = (): Sync => ({
  status: "ENABLED",
  inProgress: false,
  timestamp: null,
});

export const createSyncDisabled = (): Sync => ({
  status: "DISABLED",
  inProgress: false,
  timestamp: null,
});

export const createSyncFailed = (): Sync => ({
  status: "FAILED",
  inProgress: false,
  timestamp: null,
});

class SyncError extends Error {
  name: string = "Sync Error";
}

export class SyncEnableFailedError extends SyncError {
  message: string = "Sync enable failed";
}

export class SyncEnableCancelledError extends SyncError {
  message: string = "Sync enable cancelled";
}

export class SyncDisableFailedError extends SyncError {
  message: string = "Sync disable failed";
}
