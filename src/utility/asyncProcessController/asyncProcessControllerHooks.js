// @flow

import { useSafeAsyncController } from "../safeAsyncController/safeAsyncControllerHooks";
import { AsyncProcess } from "@draftup/utility.js";
import React from "react";

type AsyncProcessController<ValueType> = {
  process: AsyncProcess<ValueType>,
  wrapAsync(promise: Promise<ValueType | Error>): void,
};

export function useAsyncProcessController<
  ValueType
>(): AsyncProcessController<ValueType> {
  const [process, setProcess] = React.useState<AsyncProcess<ValueType>>(
    new AsyncProcess()
  );

  const { wrapAsyncSafely } = useSafeAsyncController();

  const counterRef = React.useRef(0);

  const wrapAsync = React.useCallback(
    (promise: Promise<ValueType | Error>) => {
      counterRef.current++;

      const { current: currentCount } = counterRef;

      setProcess(new AsyncProcess().pend());

      wrapAsyncSafely(
        promise.then((value) => {
          if (currentCount === counterRef.current) {
            if (value instanceof Error) {
              setProcess((prevProcess) => {
                return new AsyncProcess(prevProcess).reject(value);
              });
            } else {
              setProcess((prevProcess) => {
                return new AsyncProcess(prevProcess).resolve(value);
              });
            }
          }
        })
      );
    },
    [wrapAsyncSafely]
  );

  return { process, wrapAsync };
}
