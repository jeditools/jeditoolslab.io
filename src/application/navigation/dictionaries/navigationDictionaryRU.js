// @flow strict

import type { NavigationDictionary } from "application/navigation/navigationTypes";

export const navigationDictionaryRU: NavigationDictionary = {
  projectsManagementLinkText: "Проекты",
  practicesManagementLinkText: "Практики",
  settingsManagementLinkText: "Настройки",
};
