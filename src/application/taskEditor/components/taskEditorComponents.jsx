// @flow strict

import type { TaskId } from "../../task/taskTypes";

import * as React from "react";

import { useServices } from "../../services/servicesHooks";
import { useEventDispatcher } from "../../event/eventHooks";
import { TaskEditorFormComponent } from "./taskEditorFormComponents.jsx";
import { TaskEditorFieldsetComponent } from "./taskEditorFieldsetComponents.jsx";
import { TaskEditorFooterComponent } from "./taskEditorFooterComponents.jsx";
import { useEditedTaskId } from "../taskEditorHooks";
import { FormControlComponent } from "../../../ui/formControl/formControlComponents.jsx";
import { useHistory } from "react-router-dom";
import {
  createTaskEditorFormData,
  createTaskEditorFormDataConstructor,
  validateTaskEditorFormData,
} from "../../taskEditorForm/taskEditorFormUtils";
import { createActivityRoute } from "../../activity/activityUtils";
import {
  createForm,
  createFormConstructor,
} from "../../../utility/form/formUtils";
import { useManagedProjectId } from "../../projectManager/projectManagerHooks";
import { useState } from "../../state/stateHooks";
import { useAsyncProcessController } from "../../../utility/asyncProcessController/asyncProcessControllerHooks";

export const TaskEditorComponent = (): React.Node => {
  const { process, wrapAsync } = useAsyncProcessController();

  const managedProjectId = useManagedProjectId();

  const editedTaskId = useEditedTaskId();

  const { taskEditorService } = useServices();

  const { taskByIdCache } = useState();

  const dispatch = useEventDispatcher();

  const [form, setForm] = React.useState(
    createForm(createTaskEditorFormData(managedProjectId))
  );

  React.useLayoutEffect(() => {
    if (editedTaskId) {
      const task = taskByIdCache.readValue(editedTaskId);

      if (task) {
        setForm((prev) =>
          createForm(
            createTaskEditorFormDataConstructor(prev.data)
              .changeDescription(task.description)
              .return()
          )
        );
      }
    }
  }, [editedTaskId, taskByIdCache]);

  const history = useHistory();

  const validate = React.useCallback(() => {
    const violations = validateTaskEditorFormData(form.data);

    const valid = violations.length === 0;

    if (!valid) {
      setForm((prev) => {
        return createFormConstructor(prev).toRejected(violations).return();
      });
    }

    return valid;
  }, [form.data]);

  const handleDataChanged = React.useCallback((nextData) => {
    setForm((prev) =>
      createFormConstructor(prev).changeData(nextData).return()
    );
  }, []);

  const handleSubmitSucceed = React.useCallback(
    (taskId: TaskId) => {
      dispatch({
        name: "TASK_EDITOR__TASK_SUBMITTED",
        taskId,
      });

      const { projectId } = form.data;

      if (editedTaskId) {
        history.replace(
          createActivityRoute({
            name: "TASK_MANAGEMENT",
            taskId: editedTaskId,
            projectId,
          })
        );
      } else {
        history.replace(
          createActivityRoute({ name: "PROJECT_MANAGEMENT", projectId })
        );
      }
    },
    [dispatch, editedTaskId, form.data, history]
  );

  const submitFormData = React.useCallback(() => {
    const promise = taskEditorService
      .submitFormData(editedTaskId, form.data)
      .then(handleSubmitSucceed);

    wrapAsync(promise);
  }, [
    editedTaskId,
    form.data,
    handleSubmitSucceed,
    taskEditorService,
    wrapAsync,
  ]);

  const submit = React.useCallback(() => {
    const valid = validate();

    if (valid) {
      submitFormData();
    }
  }, [submitFormData, validate]);

  React.useEffect(() => {
    if (form.status === "REJECTED") {
      validate();
    }
  }, [form.status, validate]);

  const violations = form.status === "REJECTED" ? form.violations : [];

  const processIsPending = process.stage.name === "PENDING";

  return (
    <FormControlComponent onSubmit={submit}>
      <TaskEditorFormComponent
        fieldset={
          <TaskEditorFieldsetComponent
            disabled={processIsPending}
            data={form.data}
            violations={violations}
            onDataChanged={handleDataChanged}
          />
        }
        footer={<TaskEditorFooterComponent disabled={processIsPending} />}
      />
    </FormControlComponent>
  );
};
