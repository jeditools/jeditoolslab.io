// @flow strict

import type { TaskService } from "../taskTypes";

import { createTaskService } from "../taskServiceCreators";
import { persistedEventDevService } from "application/persistedEvent/persistedEventServices/persistedEventDevServices";

export const taskServiceDev: TaskService = createTaskService(
  persistedEventDevService
);
