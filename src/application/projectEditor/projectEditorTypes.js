// @flow
import type { Form } from "utility/form/formTypes";
import type { ProjectFormViolation } from "application/projectForm/projectFormTypes";
import type { Project, ProjectId } from "../project/projectTypes";

export type ProjectEditorDictionary = {|
  nameLabelText: string,
  nameRequiredHintText: string,
  nameTakenHintText: string,
  namePlaceholderText: string,
  detailsLabelText: string,
  detailsPlaceholderText: string,
  submitActionText: string,
|};

export type ProjectEditorForm = Form<Project, ProjectFormViolation>;

type FormSubmitted = {|
  name: "PROJECT_EDITOR__PROJECT_SUBMITTED",
  projectId: ProjectId,
|};

export type ProjectEditorEvent = FormSubmitted;

export type ProjectEditorService = {|
  submit(projectId: null | ProjectId, project: Project): Promise<ProjectId>,
|};
