// @flow

import type {
  PracticeEntry,
  PracticeId,
} from "application/practice/practiceTypes";

export type PracticeManagerData = {|
  practiceEntry: PracticeEntry,
|};

type DataRecieved = {|
  name: "PRACTICE_MANAGER__DATA_RECIEVED",
  data: PracticeManagerData,
|};

type PracticeCompleted = {|
  name: "PRACTICE_MANAGER__PRACTICE_COMPLETED",
  practiceId: PracticeId,
|};

type PracticeDeleted = {|
  name: "PRACTICE_MANAGER__PRACTICE_DELETED",
  practiceId: PracticeId,
|};

export type PracticeManagerEvent =
  | DataRecieved
  | PracticeCompleted
  | PracticeDeleted;

export type PracticeManagerDictionary = {|
  editLinkText: string,
  initialCompletionText: string,
  nextCompletionText(nextCompletionTimestamp: number): string,
  completeActionText: string,
  editActionText: string,
  deleteActionText: string,
  deleteConfirmText: string,
|};

export type PracticeManagerService = {|
  getData(practiceId: PracticeId): Promise<PracticeManagerData>,
|};
