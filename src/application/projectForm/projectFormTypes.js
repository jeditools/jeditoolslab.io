// @flow strict

type ProjectFormViolation__NameRequired = {
  name: "NAME_REQUIRED",
};

type ProjectFormViolation__NameTaken = {
  name: "NAME_TAKEN",
};

export type ProjectFormViolation =
  | ProjectFormViolation__NameRequired
  | ProjectFormViolation__NameTaken;
