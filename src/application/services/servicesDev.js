// @flow strict

import type { Services } from "application/services/servicesTypes";

import { syncServiceDev } from "application/sync/syncServices/syncServiceDev";
import { migrationServiceDev } from "application/migration/migrationServices/migrationServicesDev";
import { persistedEventDevService } from "application/persistedEvent/persistedEventServices/persistedEventDevServices";
import { documentService } from "utility/document/documentServices";
import { projectManagerServiceDev } from "../projectManager/services/projectManagerServiceDev";
import { projectEditorServiceDev } from "../projectEditor/services/projectEditorServiceDev";
import { repositoryServiceDev } from "../repository/services/repositoryServiceDev";
import { exportManagerServiceDev } from "../exportManager/services/exportManagerServiceDev";
import { importManagerServiceDev } from "../importManager/services/importManagerServiceDev";
import { practiceEditorServiceDev } from "../practiceEditor/services/practiceEditorServiceDev";
import { practiceServiceDev } from "../practice/services/practiceServiceDev";
import { practiceManagerServiceDev } from "../practiceManager/services/practiceManagerServiceDev";
import { taskServiceDev } from "../task/services/taskServiceDev";
import { taskEditorServiceDev } from "../taskEditor/services/taskEditorServiceDev";
import { themeServiceDev } from "../theme/services/themeServiceDev";
import { updateServiceDev } from "../update/services/updateServiceDev";
import { projectServiceDev } from "../project/services/projectServiceDev";

export const servicesDev: Services = {
  syncService: syncServiceDev,
  documentService: documentService,
  taskService: taskServiceDev,
  taskEditorService: taskEditorServiceDev,
  themeService: themeServiceDev,
  updateService: updateServiceDev,
  projectService: projectServiceDev,
  projectManagerService: projectManagerServiceDev,
  projectEditorService: projectEditorServiceDev,
  repositoryService: repositoryServiceDev,
  migrationService: migrationServiceDev,
  exportManagerService: exportManagerServiceDev,
  importManagerService: importManagerServiceDev,
  practiceEditorService: practiceEditorServiceDev,
  practiceService: practiceServiceDev,
  practiceManagerService: practiceManagerServiceDev,
  persistedEventService: persistedEventDevService,
};
