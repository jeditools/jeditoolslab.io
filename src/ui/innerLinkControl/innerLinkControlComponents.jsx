// @flow strict

import * as React from "react";
import { Link } from "react-router-dom";

import "./innerLinkControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  children: React.Node,
  to: string,
  replace?: boolean,
|};

export const InnerLinkControlComponent = (props: Props): React.Node => {
  const { children, to, replace } = props;

  const className = createClassName("innerLinkControl")();

  return (
    <Link to={to} replace={replace} className={className}>
      {children}
    </Link>
  );
};
