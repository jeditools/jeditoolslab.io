// @flow strict

import type { Event } from "../../event/eventTypes";

export const visibleReducer = (state: boolean, event: Event): boolean => {
  switch (event.name) {
    case "ROOT__DOCUMENT_VISIBILITY_CHANGED": {
      return event.visible;
    }
    default:
      return state;
  }
};
