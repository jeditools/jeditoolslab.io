// @flow

import type { TaskId, Task } from "application/task/taskTypes";

type DataRecieved = {|
  name: "TASK_MANAGER__DATA_RECIEVED",
  taskId: TaskId,
  task: Task,
|};

type TaskCancelled = {|
  name: "TASK_MANAGER__TASK_DELETED",
  taskId: TaskId,
|};

type TaskCompleted = {|
  name: "TASK_MANAGER__TASK_COMPLETED",
  taskId: TaskId,
|};

export type TaskManagerEvent = TaskCompleted | TaskCancelled | DataRecieved;

export type TaskManagerDictionary = {|
  editActionText: string,
  deleteActionText: string,
  deleteConfirmText: string,
  completeActionText: string,
  completedText: string,
|};
