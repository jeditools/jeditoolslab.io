// @flow strict

import type {
  SelectControl,
  SelectControlOption,
} from "utility/selectControl/selectControlTypes";

export function createSelectControl<ValueType>(
  selectControl: SelectControl<ValueType>
): SelectControl<ValueType> {
  return selectControl;
}

export function createSelectControlOption<ValueType>(
  selectControlOption: SelectControlOption<ValueType>
): SelectControlOption<ValueType> {
  return selectControlOption;
}
