// @flow

import type { ProjectId } from "../project/projectTypes";

export type Task = {|
  creationTimestamp: number,
  description: string,
  completionTimestamp: null | number,
  projectId: ProjectId,
|};

export type TaskConstructor = {|
  return(): Task,
  changeDescription(nextValue: string): TaskConstructor,
  changeProjectId(nextValue: string): TaskConstructor,
  complete(timestamp: number): TaskConstructor,
|};

export type TaskId = string;

export type TaskEntry = [TaskId, Task];

export type TaskService = {|
  deleteTask(taskId: TaskId): Promise<void>,
  completeTask(taskId: TaskId): Promise<void>,
  addTask(task: Task): Promise<TaskId>,
  editTask(taskId: TaskId, task: Task): Promise<TaskId>,
  getTask(taskId: TaskId): Promise<Task>,
  getTaskEntries(taskIds: TaskId[]): Promise<TaskEntry[]>,
  getAllTaskEntries(): Promise<TaskEntry[]>,
|};

export type TaskEntriesLense = {|
  selectActiveIds(): TaskId[],
  selectCompletedIds(): TaskId[],
  selectActiveEntries(): TaskEntry[],
  selectCompletedEntries(): TaskEntry[],
|};

export type TaskManagementActivityName = "TASK_MANAGEMENT";

export type TaskManagementActivity = {|
  name: TaskManagementActivityName,
  projectId: ProjectId,
  taskId: TaskId,
|};

export type TaskCreationActivityName = "TASK_CREATION";

export type TaskCreationActivity = {|
  name: TaskCreationActivityName,
  projectId: ProjectId,
|};

export type TaskEditingActivityName = "TASK_EDITING";

export type TaskEditingActivity = {|
  name: TaskEditingActivityName,
  projectId: ProjectId,
  taskId: TaskId,
|};
