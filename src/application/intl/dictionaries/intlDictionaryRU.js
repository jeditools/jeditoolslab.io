// @flow strict

import type { IntlDictionary } from "../intlTypes";

import { activityControlsDictionaryRU } from "../../activityControls/dictionaries/activityControlsDictionaryRU";
import { appearanceManagerDictionaryRU } from "../../appearanceManager/dictionaries/appearanceManagerDictionaryRU";
import { errorMessageDictionaryRU } from "../../errorMessage/dictionaries/errorMessageDictionaryRU";
import { navigationDictionaryRU } from "../../navigation/dictionaries/navigationDictionaryRU";
import { pendingMessageDictionaryRU } from "../../pendingMessage/dictionaries/pendingMessageDictionaryRU";
import { taskEditorDictionaryRU } from "../../taskEditor/dictionaries/taskEditorDictionaryRU";
import { taskManagerDictionaryRU } from "../../taskManager/dictionaries/taskManagerDictionaryRU";
import { tasksManagerDictionaryRU } from "../../tasksManager/dictionaries/tasksManagerDictionaryRU";
import { syncManagerDictRU } from "../../syncManager/dictionaries/syncManagerDictionaryRU";
import { errorScreenDictionaryRU } from "../../errorScreen/dictionaries/errorScreenDictionaryRU";
import { updateManagerDictRU } from "../../updateManager/dictionaries/updateManagerDictionaryRU";
import { projectEditorDictionaryRU } from "../../projectEditor/dictionaries/projectEditorDictionaryRU";
import { projectsManagerDictionaryRU } from "../../projectsManager/dictionaries/projectsManagerDictionaryRU";
import { projectManagerDictionaryRU } from "../../projectManager/dictionaries/projectManagerDictionaryRU";
import { indicatorDictionaryRu } from "../../indicator/dictionaries/indicatorDictionaryRu";
import { exportManagerDictRU } from "../../exportManager/dictionaries/exportManagerDictionaryRU";
import { importManagerDictRU } from "../../importManager/dictionaries/importManagerDictionaryRU";
import { practicesManagerDictionaryRU } from "../../practicesManager/dictionaries/practicesManagerDictionaryRU";
import { practiceEditorDictionaryRU } from "../../practiceEditor/dictionaries/practiceEditorDictionaryRU";
import { practiceManagerDictionaryRU } from "../../practiceManager/dictionaries/practiceManagerDictionaryRU";

export const intlDictionaryRU: IntlDictionary = {
  taskEditor: taskEditorDictionaryRU,
  taskManager: taskManagerDictionaryRU,
  activityControls: activityControlsDictionaryRU,
  navigation: navigationDictionaryRU,
  appearanceManager: appearanceManagerDictionaryRU,
  errorMessage: errorMessageDictionaryRU,
  pendingMessage: pendingMessageDictionaryRU,
  tasksManager: tasksManagerDictionaryRU,
  syncManager: syncManagerDictRU,
  errorScreen: errorScreenDictionaryRU,
  updateManager: updateManagerDictRU,
  projectEditor: projectEditorDictionaryRU,
  projectsManager: projectsManagerDictionaryRU,
  projectManager: projectManagerDictionaryRU,
  indicator: indicatorDictionaryRu,
  exportManager: exportManagerDictRU,
  importManager: importManagerDictRU,
  practicesManager: practicesManagerDictionaryRU,
  practiceEditor: practiceEditorDictionaryRU,
  practiceManager: practiceManagerDictionaryRU,
};
