// @flow strict

import * as React from "react";

import "./actionControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type ButtonActionProps = {|
  type: "button",
  children: React.Node,
  disabled?: boolean,
  onClick(): void,
|};

type SubmitActionProps = {|
  type: "submit",
  children: React.Node,
  disabled?: boolean,
|};

type Props = ButtonActionProps | SubmitActionProps;

export const ActionControlComponent = (props: Props): React.Node => {
  const handleClick = React.useCallback(() => {
    if (props.type === "button") {
      props.onClick();
    }
  }, [props]);

  const className = createClassName("action-control")();

  return (
    <button
      type={props.type}
      disabled={props.disabled}
      className={className}
      onClick={handleClick}
    >
      {props.children}
    </button>
  );
};
