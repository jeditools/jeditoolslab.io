// @flow strict

import * as React from "react";

import "./fileInputControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  disabled?: boolean,
  autoFocus?: boolean,
  onFilesChange(nextFiles: FileList): void,
|};

export const FileInputControlComponent = (props: Props): React.Node => {
  const { disabled, autoFocus, onFilesChange } = props;

  const handleChange = React.useCallback(
    (event: Event) => {
      const { target } = event;

      if (target instanceof HTMLInputElement) {
        onFilesChange(target.files);
      }
    },
    [onFilesChange]
  );

  const classNameConstructor = createClassName("file-input-control");

  const inputClassName = classNameConstructor("input");

  return (
    <input
      type="file"
      disabled={disabled}
      autoFocus={autoFocus}
      className={inputClassName}
      onChange={handleChange}
    />
  );
};
