// @flow strict

import * as React from "react";

import { createActivityRoute } from "application/activity/activityUtils";
import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { ArrowForwardIconComponent } from "ui/icon";
import {
  gearEmoji,
  booksEmoji,
  checkMarkButtonEmoji,
} from "ui/emoji/emojiConstants";
import { useIntlDictionary } from "../intl/intlHooks";
import { InnerLinkControlComponent } from "../../ui/innerLinkControl/innerLinkControlComponents.jsx";

type ItemProps = {|
  route: string,
  label: string,
|};

const NavigationItemComponent = ({ route, label }: ItemProps) => (
  <InnerLinkControlComponent to={route}>
    <GridLayoutComponent
      mainAxis="x"
      primaryAlignment="apart"
      secondaryAlignment="center"
    >
      <TextComponent>{label}</TextComponent>
      <BoxComponent>
        <TextComponent>
          <ArrowForwardIconComponent />
        </TextComponent>
      </BoxComponent>
    </GridLayoutComponent>
  </InnerLinkControlComponent>
);

export const NavigationComponent = (): React.Node => {
  const { navigation } = useIntlDictionary();

  return (
    <GridLayoutComponent mainAxis="y">
      <NavigationItemComponent
        route={createActivityRoute({ name: "PRACTICES_MANAGEMENT" })}
        label={`${checkMarkButtonEmoji} ${navigation.practicesManagementLinkText}`}
      />
      <NavigationItemComponent
        route={createActivityRoute({ name: "PROJECTS_MANAGEMENT" })}
        label={`${booksEmoji} ${navigation.projectsManagementLinkText}`}
      />
      <NavigationItemComponent
        route={createActivityRoute({ name: "SETTINGS_MANAGEMENT" })}
        label={`${gearEmoji} ${navigation.settingsManagementLinkText}`}
      />
    </GridLayoutComponent>
  );
};
