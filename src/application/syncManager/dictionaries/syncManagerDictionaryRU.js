// @flow strict

import type { SyncManagerDictionary } from "application/syncManager/syncManagerTypes";

export const syncManagerDictRU: SyncManagerDictionary = {
  sectionLabelText: "Синхронизация",
  inputLabelText: "Использовать синхронизацию",
  enabledDescriptionText:
    "Синхронизация включена. Спиосок задач доступен на всех подключенных устройствах.",
  disabledDescriptionText:
    "Включите синхронизацию, чтобы просматривать список задач на других устройствах.",
  disableActionLabelText: "Выключить",
  enableActionLabelText: "Включить",
};
