// @flow strict

import type { ProjectManagerDictionary } from "application/projectManager/projectManagerTypes";

export const projectManagerDictionaryRU: ProjectManagerDictionary = {
  noActiveTasksText: "Нет активных задач",
  newTaskLinkText: "Новая задача",
  editActionText: "Редактировать",
  deleteActionText: "Удалить",
  deleteConfirmText: "Вы уверены, что хотите удалить проект?",
  hideCompletedTasksActionText(tasksCount) {
    return `скрыть завершённые (${tasksCount})`;
  },
  showCompletedTasksActionText(tasksCount) {
    return `показать завершённые (${tasksCount})`;
  },
};
