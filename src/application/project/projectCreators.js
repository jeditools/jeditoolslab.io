// @flow strict

import type {
  Project,
  ProjectConstructor,
  ProjectNormalizer,
} from "./projectTypes";

export const createProject = (creationTimestamp: number): Project => ({
  name: "",
  details: "",
  creationTimestamp,
});

export const createProjectConstructor = (
  project: Project
): ProjectConstructor => ({
  return() {
    return project;
  },
  changeName(nextValue) {
    return createProjectConstructor({
      ...project,
      name: nextValue,
    });
  },
  changeDetails(nextDescription) {
    return createProjectConstructor({
      ...project,
      details: nextDescription,
    });
  },
  changeCreationTimestamp(nextCreationTimestamp) {
    return createProjectConstructor({
      ...project,
      creationTimestamp: nextCreationTimestamp,
    });
  },
});

export const createProjectNormalizer = (
  project: Project
): ProjectNormalizer => ({
  toPersistedProject() {
    return {
      name: project.name,
      details: project.details,
    };
  },
});
