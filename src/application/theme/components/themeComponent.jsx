// @flow strict

import type { ThemeMode } from "application/theme/themeTypes";

import * as React from "react";

import "./themeComponent.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  children: React.Node,
  mode: ThemeMode,
|};

export const ThemeComponent = (props: Props): React.Node => {
  const { children, mode } = props;

  const className = createClassName("theme")({ mode });

  return <div className={className}>{children}</div>;
};
