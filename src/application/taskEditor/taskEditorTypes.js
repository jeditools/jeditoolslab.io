// @flow strict

import type { TaskEntry } from "application/task/taskTypes";
import type { TaskId } from "application/task/taskTypes";
import type { TaskEditorFormData } from "application/taskEditorForm/taskEditorFormTypes";
import type { ProjectEntry } from "../project/projectTypes";

export type TaskEditorData = {|
  editedTaskEntry: null | TaskEntry,
  projectEntries: ProjectEntry[],
|};

export type TaskEditorDictionary = {|
  requiredFieldHintText: string,
  descriptionLabelText: string,
  descriptionPlaceholderText: string,
  projectIdLabelText: string,
  submitActionText: string,
|};

type FormSubmitted = {|
  name: "TASK_EDITOR__TASK_SUBMITTED",
  taskId: TaskId,
|};

export type TaskEditorEvent = FormSubmitted;

export type TaskEditorService = {|
  getData(editedTaskId: null | TaskId): Promise<TaskEditorData>,
  submitFormData(
    editedTaskId: null | TaskId,
    formData: TaskEditorFormData
  ): Promise<TaskId>,
|};
