// @flow strict

import type { ExportManagerService } from "application/exportManager/exportManagerTypes";

import { devServicesAsyncTimeoutNormal } from "../../services/servicesUtils";

export const exportManagerServiceDev: ExportManagerService = {
  async downloadData() {
    await devServicesAsyncTimeoutNormal.start();
  },
};
