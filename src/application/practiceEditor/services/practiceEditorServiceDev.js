// @flow strict

import type { PracticeEditorService } from "application/practiceEditor/practiceEditorTypes";

import { devServicesAsyncTimeoutNormal } from "../../services/servicesUtils";
import { practiceServiceDev } from "../../practice/services/practiceServiceDev";
import { PracticeWithNameAlreadyExistsError } from "application/practice/practiceErrors";

export const practiceEditorServiceDev: PracticeEditorService = {
  async submit(editedPracticeId, practice, params) {
    await devServicesAsyncTimeoutNormal.start();

    const practiceEntries = await practiceServiceDev.getAllPracticeEntries();

    const practiceEntryWithSameName = practiceEntries.find(
      ([, currentPractice]) => currentPractice.name === practice.name
    );

    if (
      practiceEntryWithSameName &&
      practiceEntryWithSameName[0] !== editedPracticeId
    ) {
      throw new PracticeWithNameAlreadyExistsError(practice.name);
    }

    if (editedPracticeId) {
      await practiceServiceDev.putPractice(editedPracticeId, practice);

      return editedPracticeId;
    } else {
      return practiceServiceDev.addPractice(practice);
    }
  },
};
