// @flow strict

import type { GoogleDriveService } from "utility/googleDrive/googleDriveTypes";

import { googleAuthService } from "utility/googleAuth/googleAuthServices";
import { googleAPIService } from "utility/googleAPI/googleAPIServices";
import { GoogleDriveFileNotFoundError } from "utility/googleDrive/googleDriveUtils";
import { GoogleAPIRequestError } from "../googleAPI/googleAPIUtils";

const handleRequestSafe = async (thenable: any): Promise<any> => {
  try {
    return await googleAPIService.handleRequest(thenable);
  } catch (error) {
    if (error instanceof GoogleAPIRequestError) {
      await googleAuthService.refreshAccessToken();

      return googleAPIService.handleRequest(thenable);
    }

    throw error;
  }
};

const getFiles = async () => {
  const googleAPI = await googleAPIService.getAPI();

  const resp = await handleRequestSafe(
    googleAPI.client.drive.files.list({
      spaces: "appDataFolder",
      fields: "files(id, name)",
      pageSize: 100,
      orderBy: "createdTime",
    })
  );

  return resp.result.files;
};

export const googleDriveService: GoogleDriveService = {
  async getFileId(name) {
    const files = await getFiles();

    const file = files.find((file) => file.name === name);

    if (file) {
      return file.id;
    }

    throw new GoogleDriveFileNotFoundError({ name });
  },
  async createFile(name) {
    const googleAPI = await googleAPIService.getAPI();

    const response = await handleRequestSafe(
      googleAPI.client.drive.files.create({
        resource: {
          name,
          mimeType: "text/plain",
          parents: ["appDataFolder"],
        },
        fields: "id",
      })
    );

    const { id } = response.result;

    return {
      id,
      name,
    };
  },
  async writeFile(id, content) {
    const googleAPI = await googleAPIService.getAPI();

    await handleRequestSafe(
      googleAPI.client.request({
        path: `/upload/drive/v3/files/${id}`,
        method: "PATCH",
        params: { uploadType: "media" },
        body: content,
      })
    );
  },
  async readFile(id) {
    const googleAPI = await googleAPIService.getAPI();

    const resp = await handleRequestSafe(
      googleAPI.client.drive.files.get({
        fileId: id,
        alt: "media",
      })
    );

    return resp.body;
  },
  async deleteFile(id) {
    const googleAPI = await googleAPIService.getAPI();

    await handleRequestSafe(
      googleAPI.client.drive.files.delete({
        fileId: id,
      })
    );
  },
};
