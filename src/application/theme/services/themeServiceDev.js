// @flow strict

import type { ThemeService } from "../themeTypes";

import { DEFAULT_THEME_MODE } from "../themeConstants";

let persistedThemeMode = DEFAULT_THEME_MODE;

export const themeServiceDev: ThemeService = {
  async getPersistedThemeMode() {
    return persistedThemeMode;
  },
  async setPersistedThemeMode(nextThemeMode) {
    persistedThemeMode = nextThemeMode;
  },
};
