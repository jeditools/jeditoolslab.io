// @flow strict

import type { Reducer } from "../../../utility/reducer/reducerTypes";
import type { Event } from "../../event/eventTypes";
import type { Practice } from "../../practice/practiceTypes";

import { createReducer } from "../../../utility/reducer/reducerUtils";

import { DictionaryCache } from "@draftup/utility.js";

export const practiceByIdCacheReducer: Reducer<
  DictionaryCache<string, Practice>,
  Event
> = createReducer((state, event) => {
  switch (event.name) {
    case "PRACTICES_MANAGER__DATA_RECIEVED": {
      const { practiceEntries } = event.data;

      return practiceEntries.reduce(
        (acc, [id, item]) => acc.writeValue(id, item),
        new DictionaryCache(state)
      );
    }
    case "PRACTICE_MANAGER__DATA_RECIEVED": {
      const { practiceEntry } = event.data;

      return new DictionaryCache(state).writeValue(
        practiceEntry[0],
        practiceEntry[1]
      );
    }
    case "PRACTICE_MANAGER__PRACTICE_DELETED": {
      return new DictionaryCache(state).deleteEntry(event.practiceId);
    }
    default:
      return state;
  }
});
