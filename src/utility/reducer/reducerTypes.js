// @flow strict

export type Reducer<StateType, EventType> = (
  state: StateType,
  event: EventType
) => StateType;

type ReductionDiff<StateType> = {|
  prevState: StateType,
  nextState: StateType,
|};

export type ReducerCallback<StateType, EventType> = (
  diff: ReductionDiff<StateType>,
  event: EventType
) => void;
