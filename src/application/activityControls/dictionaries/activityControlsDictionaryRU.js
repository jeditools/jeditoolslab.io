// @flow strict

import type { ActivityControlsDictionary } from "application/activityControls/activityControlsTypes";

export const activityControlsDictionaryRU: ActivityControlsDictionary = {
  projectsManagementScreenTitle: "Проекты",
  projectManagementScreenTitle: "Проект",
  projectCreationScreenTitle: "Новый проект",
  taskManagementScreenTitle: "Задача",
  taskEditingScreenTitle: "Задача",
  taskCreationScreenTitle: "Новая задача",
  practicesManagementScreenTitle: "Практики",
  practiceManagementScreenTitle: "Практика",
  practiceCreationScreenTitle: "Новая практика",
  navigationScreenTitle: "Инструменты джедая",
  settingsManagementScreenTitle: "Настройки",
};
