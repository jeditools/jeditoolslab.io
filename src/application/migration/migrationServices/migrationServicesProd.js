// @flow strict

import type { MigrationService } from "application/migration/migrationTypes";

import { createMigrationService } from "application/migration/migrationUtils";
import { persistedEventProdService } from "application/persistedEvent/persistedEventServices/persistedEventProdServices";

export const migrationServiceProd: MigrationService = createMigrationService({
  persistedEventService: persistedEventProdService,
});
