// @flow strict

import type { ThemeMode } from "./themeTypes";

export const THEME_MODE__AUTO: ThemeMode = "auto";
export const THEME_MODE__LIGHT: ThemeMode = "light";
export const THEME_MODE__DARK: ThemeMode = "dark";

export const DEFAULT_THEME_MODE = THEME_MODE__AUTO;

export const DOCUMENT_LIGHT_THEME_COLOR = "whitesmoke";

export const DOCUMENT_DARK_THEME_COLOR = "#282c35";
