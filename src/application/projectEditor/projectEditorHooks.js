// @flow strict

import type { ProjectId } from "../project/projectTypes";

import { useRouteMatch } from "react-router-dom";

import { createActivityRoutePattern } from "application/activity/activityUtils";

export const useEditedProjectId = (): null | ProjectId => {
  const match = useRouteMatch(createActivityRoutePattern("PROJECT_EDITING"));

  if (!match) {
    return null;
  }

  const {
    params: { projectId },
  } = match;

  if (typeof projectId === "string") {
    return projectId;
  }

  return null;
};
