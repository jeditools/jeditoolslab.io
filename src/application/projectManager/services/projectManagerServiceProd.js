// @flow strict

import type { ProjectEntry } from "../../project/projectTypes";
import type { ProjectManagerService } from "application/projectManager/projectManagerTypes";

import { projectServiceProd } from "../../project/services/projectServiceProd";
import { taskServiceProd } from "../../task/services/taskServiceProd";
import { createTaskEntriesLense } from "../../task/taskCreators";

export const projectManagerServiceProd: ProjectManagerService = {
  async getData(projectId) {
    const project = await projectServiceProd.getProject(projectId);

    const projectEntry: ProjectEntry = [projectId, project];

    const taskEntries = await taskServiceProd.getAllTaskEntries();

    const projectTaskEntries = taskEntries.filter(
      ([taskId, task]) => task.projectId === projectId
    );

    const activeTaskIds =
      createTaskEntriesLense(projectTaskEntries).selectActiveIds();

    const completedTaskIds =
      createTaskEntriesLense(projectTaskEntries).selectCompletedIds();

    return {
      projectEntry,
      taskEntries,
      activeTaskIds,
      completedTaskIds,
    };
  },
};
