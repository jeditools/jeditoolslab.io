// @flow strict

export class GoogleAuthError extends Error {
  name: string = "Google Auth Error";
}

type SignInErrorReason = "CANCELLED" | "UNKNOWN";

export class GoogleAuthSignInError extends GoogleAuthError {
  constructor(reason: SignInErrorReason) {
    super();

    this.reason = reason;
  }

  message: string = "Failed to sign in";

  reason: SignInErrorReason;
}

export class GoogleDriveFileNotFoundError extends GoogleAuthError {
  message: string = "File not found";
}
