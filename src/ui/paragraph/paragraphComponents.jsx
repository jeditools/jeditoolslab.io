// @flow strict

import { ExternalLinkControlComponent } from "../externalLinkControl/externalLinkControlComponents.jsx";
import { createParagraphBlocksFromText } from "./paragraphUtils";

import * as React from "react";
import { TextComponent } from "ui/text/textComponents";
import { OpenInNewIconComponent } from "ui/icon/index.js";

type Props = {|
  text: string,
  preview?: boolean,
|};

export const ParagraphComponent = (props: Props): React.Node => {
  const { text, preview } = props;

  const blocks = createParagraphBlocksFromText(text);

  return (
    <TextComponent>
      {blocks.map((block, index) => {
        switch (block.type) {
          case "URL": {
            if (preview) {
              return (
                <TextComponent key={block.id}>
                  {block.url.hostname} <OpenInNewIconComponent />
                </TextComponent>
              );
            }

            return (
              <ExternalLinkControlComponent key={block.id} to={block.url.href}>
                <TextComponent color="secondary">
                  {block.url.hostname} <OpenInNewIconComponent />
                </TextComponent>
              </ExternalLinkControlComponent>
            );
          }
          case "TEXT":
            return <TextComponent key={block.id}>{block.text}</TextComponent>;
          default:
            return null;
        }
      })}
    </TextComponent>
  );
};
