// @flow strict

import type { Task, TaskId } from "./taskTypes";

import React from "react";

import { createTaskEntriesLense, createTaskFallback } from "./taskCreators";
import { useState } from "../state/stateHooks";

export const useActiveTaskIdsList = (): TaskId[] => {
  const { taskByIdCache } = useState();

  return React.useMemo(() => {
    return createTaskEntriesLense(
      taskByIdCache.readEntries()
    ).selectActiveIds();
  }, [taskByIdCache]);
};

export const useCompletedTaskIdsList = (): TaskId[] => {
  const { taskByIdCache } = useState();

  return React.useMemo(() => {
    return createTaskEntriesLense(
      taskByIdCache.readEntries()
    ).selectCompletedIds();
  }, [taskByIdCache]);
};

export const useTask = (taskId: TaskId): Task => {
  const { taskByIdCache } = useState();

  return React.useMemo(() => {
    const task = taskByIdCache.readValue(taskId);

    return task || createTaskFallback();
  }, [taskByIdCache, taskId]);
};
