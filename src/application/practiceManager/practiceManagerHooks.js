// @flow strict

import type { PracticeId } from "../practice/practiceTypes";

import { useRouteMatch } from "react-router-dom";

import { createActivityRoutePattern } from "application/activity/activityUtils";

export const useManagedPracticeId = (): PracticeId => {
  const {
    params: { practiceId },
  } = useRouteMatch(createActivityRoutePattern("PRACTICE_MANAGEMENT"));

  if (typeof practiceId === "string") {
    return practiceId;
  }

  throw TypeError();
};
