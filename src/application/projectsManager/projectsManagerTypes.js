// @flow strict

import type { Project, ProjectId } from "../project/projectTypes";
import type { RepositoryData } from "application/repository/repositoryTypes";

export type ProjectsManagerDictionary = {|
  noProjectsText: string,
  newProjectLinkText: string,
  latestCompletionText(timestamp: number): string,
|};

type DataRecieved = {|
  name: "PROJECTS_MANAGER__DATA_RECIEVED",
  data: RepositoryData,
|};

export type ProjectsManagerEvent = DataRecieved;

export type ProjectManagerInitialData = {|
  activeIds: ProjectId[],
  completedIds: ProjectId[],
  projectEntries: [ProjectId, Project][],
|};
