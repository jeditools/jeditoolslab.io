// @flow strict

type FormStatusCreated = "CREATED";

type FormStatusRejected = "REJECTED";

type FormStatusAccepted = "ACCEPTED";

export type FormStatus =
  | FormStatusCreated
  | FormStatusRejected
  | FormStatusAccepted;

type FormCreated<DataType> = {
  status: FormStatusCreated,
  data: DataType,
};

type FormRejected<DataType, ViolationType> = {
  status: FormStatusRejected,
  data: DataType,
  violations: ViolationType[],
};

type FormAccepted<DataType> = {
  status: FormStatusAccepted,
  data: DataType,
};

export type Form<DataType, ValidityType> =
  | FormCreated<DataType>
  | FormRejected<DataType, ValidityType>
  | FormAccepted<DataType>;

export type FormConstructor<DataType, ViolationType> = {
  return(): Form<DataType, ViolationType>,
  changeData(nextData: DataType): FormConstructor<DataType, ViolationType>,
  toRejected(
    violations: ViolationType[]
  ): FormConstructor<DataType, ViolationType>,
  toAccepted(): FormConstructor<DataType, ViolationType>,
};
