// @flow strict

import type { RepositoryService } from "application/repository/repositoryTypes";

import { practiceServiceDev } from "../../practice/services/practiceServiceDev";
import { projectServiceDev } from "../../project/services/projectServiceDev";
import { taskServiceDev } from "../../task/services/taskServiceDev";

export const repositoryServiceDev: RepositoryService = {
  async getData(query, params) {
    const {
      projectIds,
      taskIds,
      practiceIds,
      allProjects,
      allTasks,
      allPractices,
    } = query;

    let projectEntriesPromise = Promise.resolve([]);

    let taskEntriesPromise = Promise.resolve([]);

    let practiceEntriesPromise = Promise.resolve([]);

    if (allProjects) {
      projectEntriesPromise = projectServiceDev.getAllProjectEntries();
    }

    if (projectIds && projectIds.length > 0) {
      projectEntriesPromise = projectServiceDev.getProjectEntries(projectIds);
    }

    if (allTasks) {
      taskEntriesPromise = taskServiceDev.getAllTaskEntries();
    }

    if (taskIds && taskIds.length > 0) {
      taskEntriesPromise = taskServiceDev.getTaskEntries(taskIds);
    }

    if (allPractices) {
      practiceEntriesPromise = practiceServiceDev.getAllPracticeEntries();
    }

    if (practiceIds && practiceIds.length > 0) {
      practiceEntriesPromise =
        practiceServiceDev.getPracticeEntries(practiceIds);
    }

    const [projectEntries, taskEntries, practiceEntries] = await Promise.all([
      projectEntriesPromise,
      taskEntriesPromise,
      practiceEntriesPromise,
    ]);

    return { projectEntries, taskEntries, practiceEntries };
  },
};
