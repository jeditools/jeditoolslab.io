// @flow strict

import type { ActivityControlsDictionary } from "application/activityControls/activityControlsTypes";

import { useIntlDictionary } from "../intl/intlHooks";

export const useActivityControlsDictionary = (): ActivityControlsDictionary => {
  const { activityControls } = useIntlDictionary();

  return activityControls;
};
