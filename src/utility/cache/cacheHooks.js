// @flow strict

import type { Cache } from "./cacheTypes";

import React from "react";

export function useCacheValue<ValueType>(cache: Cache<ValueType>): ValueType {
  const [value, setValue] = React.useState(cache.get());

  if (value instanceof Error) {
    throw value;
  }

  if (value instanceof Promise) {
    throw value;
  }

  React.useEffect(() => {
    return cache.listen(() => {
      const nextValue = cache.get();

      if (!(nextValue instanceof Promise)) {
        setValue(nextValue);
      }
    });
  }, [cache]);

  return value;
}
