// @flow strict

import type { PracticeEditorDictionary } from "../practiceEditorTypes";

export const practiceEditorDictionaryRU: PracticeEditorDictionary = {
  nameLabelText: "Название",
  nameRequiredHintText: "Поле обязательно для заполнения",
  namePlaceholderText: "Название практики",
  detailsLabelText: "Подробности",
  detailsPlaceholderText: "Подробности практики",
  regularityInDaysLabelText: "Регулярность (в днях)",
  regularityInDaysRequiredHintText: "Поле обязательно для заполнения",
  regularityInDaysPlaceholderText: "Регулярность практики",
  nameTakenHintText: "Прaктика с таким названием уже существует",
  submitActionText: "Сохранить",
};
