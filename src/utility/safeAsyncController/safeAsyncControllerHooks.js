// @flow strict

import React from "react";

type SafeAsyncWrapper = {
  wrapAsyncSafely(promise: Promise<void>): void,
};

export function useSafeAsyncController(): SafeAsyncWrapper {
  const [error, setError] = React.useState<null | Error>(null);

  if (error !== null) {
    throw error;
  }

  const wrapAsyncSafely = React.useCallback((promise) => {
    promise.catch(setError);
  }, []);

  return { wrapAsyncSafely };
}
