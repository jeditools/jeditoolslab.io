// @flow strict

import * as React from "react";
import { useHistory } from "react-router-dom";

import { TextComponent } from "ui/text/textComponents";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { useSyncEnabled } from "application/sync/syncHooks";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import { CardComponent } from "ui/card/cardComponents";
import { DoneIconComponent } from "ui/icon";
import { useEventDispatcher } from "../event/eventHooks";
import { useManagedTaskId } from "./taskManagerHooks";
import { useServices } from "../services/servicesHooks";
import { useTask } from "../task/taskHooks";
import { createActivityRoute } from "../activity/activityUtils";
import { useIntlDictionary } from "../intl/intlHooks";
import { useManagedProjectId } from "../projectManager/projectManagerHooks";
import { ParagraphComponent } from "../../ui/paragraph/paragraphComponents.jsx";
import {
  DropDownMenuComponent,
  DropDownMenuItemComponent,
} from "ui/dropDownMenu/dropDownMenuComponents";
import { useAsyncProcessController } from "../../utility/asyncProcessController/asyncProcessControllerHooks";

export const TaskManagerComponent = (): React.Node => {
  const dispatch = useEventDispatcher();

  const taskId = useManagedTaskId();

  const projectId = useManagedProjectId();

  const { wrapAsync, process } = useAsyncProcessController();

  const { taskService } = useServices();

  const task = useTask(taskId);

  const history = useHistory();

  const { taskManager: dictionary } = useIntlDictionary();

  const syncEnabled = useSyncEnabled();

  const taskIsCompleted = task.completionTimestamp !== null;

  React.useLayoutEffect(() => {
    const promise = taskService.getTask(taskId).then((task) => {
      dispatch({
        name: "TASK_MANAGER__DATA_RECIEVED",
        taskId,
        task,
      });
    });

    wrapAsync(promise);
  }, [dispatch, wrapAsync, syncEnabled, taskId, taskService]);

  const goToProjectManagement = React.useCallback(() => {
    history.replace(
      createActivityRoute({ name: "PROJECT_MANAGEMENT", projectId })
    );
  }, [history, projectId]);

  const confirmCancelation = React.useCallback(() => {
    return window.confirm(dictionary.deleteConfirmText);
  }, [dictionary.deleteConfirmText]);

  const handleSuccessfulDeletion = React.useCallback(() => {
    dispatch({
      name: "TASK_MANAGER__TASK_DELETED",
      taskId,
    });

    goToProjectManagement();
  }, [dispatch, goToProjectManagement, taskId]);

  const deleteTask = React.useCallback(() => {
    const confirmed = confirmCancelation();

    if (confirmed) {
      wrapAsync(taskService.deleteTask(taskId).then(handleSuccessfulDeletion));
    }
  }, [
    confirmCancelation,
    handleSuccessfulDeletion,
    wrapAsync,
    taskId,
    taskService,
  ]);

  const handleSuccessfulCompletion = React.useCallback(() => {
    dispatch({
      name: "TASK_MANAGER__TASK_COMPLETED",
      taskId,
    });

    goToProjectManagement();
  }, [dispatch, goToProjectManagement, taskId]);

  const completeTask = React.useCallback(() => {
    wrapAsync(
      taskService.completeTask(taskId).then(handleSuccessfulCompletion)
    );
  }, [handleSuccessfulCompletion, wrapAsync, taskId, taskService]);

  const editTask = React.useCallback(() => {
    history.push(
      createActivityRoute({
        name: "TASK_EDITING",
        taskId,
        projectId,
      })
    );
  }, [history, projectId, taskId]);

  return (
    <GridLayoutComponent mainAxis="y">
      <GridLayoutComponent mainAxis="y" secondaryAlignment="start">
        <TextComponent>
          <ParagraphComponent text={task.description} />
        </TextComponent>
      </GridLayoutComponent>
      <GridLayoutComponent
        mainAxis="x"
        primaryAlignment="apart"
        secondaryAlignment="center"
      >
        <DropDownMenuComponent>
          {!taskIsCompleted && (
            <DropDownMenuItemComponent
              text={dictionary.editActionText}
              onSelect={editTask}
            />
          )}
          <DropDownMenuItemComponent
            text={dictionary.deleteActionText}
            onSelect={deleteTask}
          />
        </DropDownMenuComponent>
        {!taskIsCompleted ? (
          <ActionControlComponent
            disabled={process.stage.name === "PENDING"}
            type="button"
            onClick={completeTask}
          >
            <CardComponent floating>
              <BoxComponent>{dictionary.completeActionText}</BoxComponent>
            </CardComponent>
          </ActionControlComponent>
        ) : (
          <TextComponent color="secondary">
            <InlineLayoutComponent>
              <DoneIconComponent /> {dictionary.completedText}
            </InlineLayoutComponent>
          </TextComponent>
        )}
      </GridLayoutComponent>
    </GridLayoutComponent>
  );
};
