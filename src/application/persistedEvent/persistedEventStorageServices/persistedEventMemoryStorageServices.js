// @flow strict

import type {
  PersistedEventStorageService,
  PersistedEvent,
  PersistedEventId,
} from "application/persistedEvent/persistedEventTypes";

let persistedEventEntries: [PersistedEventId, PersistedEvent][] = [];

export const persistedEventMemoryStorageService: PersistedEventStorageService =
  {
    async write(nextPrsistedEventEntries) {
      persistedEventEntries = nextPrsistedEventEntries;
    },
    async read() {
      return persistedEventEntries;
    },
  };
