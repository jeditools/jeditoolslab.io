// @flow strict

import type { PersistedEventService } from "application/persistedEvent/persistedEventTypes";
import type { ProjectService } from "./projectTypes";

import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createPersistedEventEntriesProjectLense } from "application/persistedEvent/persistedEventUtils/persistedEventProjectLenses";
import { createProjectNormalizer } from "./projectCreators";
import { ProjectNotFoundError } from "./projectConstants";

export const createProjectService = (
  persistedEventService: PersistedEventService
): ProjectService => ({
  async addProject(project, params) {
    const projectId = createUniqueId();

    await persistedEventService.addEvents([
      {
        name: "ProjectCreated",
        timestamp: Date.now(),
        projectId,
        project: createProjectNormalizer(project).toPersistedProject(),
      },
    ]);

    return projectId;
  },
  async getProject(projectId, params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    const project = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProject(projectId);

    if (!project) {
      throw new ProjectNotFoundError(projectId);
    }

    return project;
  },
  async getProjectEntries(projectIds, params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    return createPersistedEventEntriesProjectLense(persistedEventEntries)
      .selectProjectEntries()
      .filter(([projectId]) => projectIds.includes(projectId));
  },
  async getAllProjectEntries(params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    return createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProjectEntries();
  },
  async deleteProject(projectId, params) {
    await persistedEventService.addEvents([
      {
        name: "ProjectDeleted",
        timestamp: Date.now(),
        projectId,
      },
    ]);
  },

  async putProject(projectId, project, params) {
    await persistedEventService.addEvents([
      {
        name: "ProjectUpdated",
        timestamp: Date.now(),
        projectId,
        project: createProjectNormalizer(project).toPersistedProject(),
      },
    ]);
  },
});
