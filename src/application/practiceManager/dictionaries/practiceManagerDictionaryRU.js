// @flow strict

import type { PracticeManagerDictionary } from "application/practiceManager/practiceManagerTypes";

import {
  formatDistanceToNow,
  isPast,
  isToday,
  isTomorrow,
  isYesterday,
} from "date-fns";
import { ru } from "date-fns/locale";

export const practiceManagerDictionaryRU: PracticeManagerDictionary = {
  editLinkText: "Редактировать",
  initialCompletionText: "Не выполнялась",
  nextCompletionText(nextCompletionTimestamp) {
    const date = new Date(nextCompletionTimestamp);

    if (isYesterday(date)) {
      return "Нужно было выполнить вчера";
    }

    if (isToday(date)) {
      return "Нужно выполнить сегодня";
    }

    if (isTomorrow(date)) {
      return "Нужно будет выполнить завтра";
    }

    const dateText = formatDistanceToNow(date, {
      locale: ru,
      addSuffix: true,
    });

    if (isPast(date)) {
      return `Нужно было выполнить ${dateText}`;
    }

    return `Нужно будет выполнить ${dateText}`;
  },
  completeActionText: "Выполнить",
  editActionText: "Редактировать",
  deleteActionText: "Удалить",
  deleteConfirmText: "Вы уверены, что хотите удалить практику?",
};
