// @flow strict

import type { PersistedEventService } from "application/persistedEvent/persistedEventTypes";

import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createEntriesConstructor } from "utility/entry/entryUtils";
import { persistedEventMemoryStorageService } from "application/persistedEvent/persistedEventStorageServices/persistedEventMemoryStorageServices";
import { devServicesAsyncTimeoutLong } from "../../services/servicesUtils";

export const persistedEventDevService: PersistedEventService = {
  async addEvents(events) {
    const persistedEventEntries =
      await persistedEventMemoryStorageService.read();

    const nextPersistedEventEntries = events
      .reduce((acc, event) => {
        const eventId = createUniqueId();

        return acc.setEntry(eventId, event);
      }, createEntriesConstructor(persistedEventEntries))
      .return();

    await persistedEventMemoryStorageService.write(nextPersistedEventEntries);
  },
  async getEventEntries() {
    return persistedEventMemoryStorageService.read();
  },
  async setEventEntries(nextEntries) {
    return persistedEventMemoryStorageService.write(nextEntries);
  },
  async sync() {
    await devServicesAsyncTimeoutLong.start();
  },
};
