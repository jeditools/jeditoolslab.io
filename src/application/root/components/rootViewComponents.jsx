// @flow strict

import type { ThemeMode } from "../../theme/themeTypes";

import * as React from "react";

import { ScreenLayoutComponent } from "ui/screenLayout/screenLayoutComponents";
import { useServices } from "application/services/servicesHooks";
import { ErrorBoundaryComponent } from "../../errorBoundary/errorBoundaryComponents.jsx";
import { ErrorScreenComponent } from "../../errorScreen/errorScreenComponents.jsx";
import { useThemeMode } from "../../theme/themeHooks";
import {
  DOCUMENT_DARK_THEME_COLOR,
  DOCUMENT_LIGHT_THEME_COLOR,
} from "../../theme/themeConstants";
import { ThemeComponent } from "../../theme/components/themeComponent.jsx";

type Props = {|
  children: React.Node,
|};

export const RootViewComponent = (props: Props): React.Node => {
  const { children } = props;

  const themeName: ThemeMode = useThemeMode();

  const { documentService } = useServices();

  React.useEffect(() => {
    if (themeName === "light") {
      documentService.setThemeColor(DOCUMENT_LIGHT_THEME_COLOR);
    }

    if (themeName === "dark") {
      documentService.setThemeColor(DOCUMENT_DARK_THEME_COLOR);
    }
  }, [documentService, themeName]);

  return (
    <ThemeComponent mode={themeName}>
      <ScreenLayoutComponent>
        <ErrorBoundaryComponent
          fallbackView={ErrorScreenComponent}
          errorConstructorsToCatch={[Error]}
        >
          {children}
        </ErrorBoundaryComponent>
      </ScreenLayoutComponent>
    </ThemeComponent>
  );
};
