// @flow strict

import type { UpdateService } from "application/update/updateTypes";

import { serviceWorkerService } from "utility/serviceWorker/serviceWorkerServices";

let waitingWorker: null | ServiceWorker = null;

export const updateServiceProd: UpdateService = {
  async subscribe(callback) {
    const handleSWUppdate = (registration: ServiceWorkerRegistration) => {
      if (registration.waiting) {
        waitingWorker = registration.waiting;

        callback("DOWNLOADED");
      }
    };

    serviceWorkerService.register({ onUpdate: handleSWUppdate });
  },
  async applyUpdate() {
    if (waitingWorker) {
      waitingWorker.postMessage({ type: "SKIP_WAITING" });

      window.location.reload(true);
    }
  },
};
