// @flow

type Props = {
  className?: string,
};

declare export var ReactComponent: (props: Props) => React$Node;
