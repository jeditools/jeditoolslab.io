// @flow strict

import type { EventDispatcher } from "application/event/eventTypes";

import * as React from "react";

export const EventDispatcherContext: React.Context<EventDispatcher> =
  React.createContext((event) => {});
