// @flow strict

import type { Practice } from "../../practice/practiceTypes";
import type { PracticeFormViolation } from "../../practiceForm/practiceFormTypes";

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { TextControlComponent } from "ui/textControl/textControlComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { LabelComponent } from "ui/label/labelComponents";
import { NumberInputControlComponent } from "ui/numberInputControl/numberInputControlComponents";
import { useIntlDictionary } from "../../intl/intlHooks";
import { createPracticeConstructor } from "../../practice/practiceCreators";

type Props = {
  disabled: boolean,
  data: Practice,
  violations: PracticeFormViolation[],
  onDataChanged(nextData: Practice): void,
};

export const PracticeEditorFieldsetComponent = (props: Props): React.Node => {
  const { data, violations, disabled, onDataChanged } = props;

  const { practiceEditor: dictionary } = useIntlDictionary();

  const changePracticeName = React.useCallback(
    (nextValue: string) => {
      onDataChanged(createPracticeConstructor(data).setName(nextValue).get());
    },
    [data, onDataChanged]
  );

  const changePracticeDetails = React.useCallback(
    (nextValue: string) => {
      onDataChanged(
        createPracticeConstructor(data).setDetails(nextValue).get()
      );
    },
    [data, onDataChanged]
  );

  const changeRegularityInDays = React.useCallback(
    (nextValue: number) => {
      onDataChanged(
        createPracticeConstructor(data).setRegularityInDays(nextValue).get()
      );
    },
    [data, onDataChanged]
  );

  const hasNameRequiredHint = violations.some(
    (violation) => violation.name === "NAME_REQUIRED"
  );

  const hasRegularityInDaysRequiredHint = violations.some(
    (violation) => violation.name === "REGULARITY_IN_DAYS_REQUIRED"
  );

  const hasNameTakenHint = violations.some(
    (violation) => violation.name === "NAME_TAKEN"
  );

  return (
    <GridLayoutComponent mainAxis="y">
      <LabelComponent>
        <GridLayoutComponent mainAxis="y">
          <TextComponent>{dictionary.nameLabelText}</TextComponent>
          <TextControlComponent
            placeholder={dictionary.namePlaceholderText}
            value={data.name}
            disabled={disabled}
            autoFocus
            onValueChange={changePracticeName}
          />
          {hasNameRequiredHint && (
            <TextComponent size="s" color="critical">
              {dictionary.nameRequiredHintText}
            </TextComponent>
          )}
          {hasNameTakenHint && (
            <TextComponent size="s" color="critical">
              {dictionary.nameTakenHintText}
            </TextComponent>
          )}
        </GridLayoutComponent>
      </LabelComponent>
      <LabelComponent>
        <GridLayoutComponent mainAxis="y">
          <TextComponent>{dictionary.regularityInDaysLabelText}</TextComponent>
          <NumberInputControlComponent
            min={0}
            placeholder={dictionary.regularityInDaysPlaceholderText}
            value={data.regularityInDays}
            disabled={disabled}
            onValueChange={changeRegularityInDays}
          />
          {hasRegularityInDaysRequiredHint && (
            <TextComponent size="s" color="critical">
              {dictionary.regularityInDaysRequiredHintText}
            </TextComponent>
          )}
        </GridLayoutComponent>
      </LabelComponent>
      <LabelComponent>
        <GridLayoutComponent mainAxis="y">
          <TextComponent>{dictionary.detailsLabelText}</TextComponent>
          <TextControlComponent
            placeholder={dictionary.detailsPlaceholderText}
            value={data.details}
            disabled={disabled}
            multiline
            onValueChange={changePracticeDetails}
          />
        </GridLayoutComponent>
      </LabelComponent>
    </GridLayoutComponent>
  );
};
