// @flow strict

export type AsyncCache<ValueType> = {
  read(): Promise<ValueType>,
  invalidate(): void,
};
