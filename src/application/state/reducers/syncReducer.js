// @flow strict

import type { Event } from "../../event/eventTypes";
import type { Sync } from "application/sync/syncTypes";

import {
  createSyncDisabled,
  createSyncEnabled,
  createSyncFailed,
} from "../../sync/syncUtils";

export const syncReducer = (state: null | Sync, event: Event): null | Sync => {
  switch (event.name) {
    case "ROOT__SYNC_RECIEVED":
      return event.sync;
    case "SYNC_MANAGER__SYNC_ENABLED":
      return createSyncEnabled();
    case "SYNC_MANAGER__SYNC_DISABLED":
      return createSyncDisabled();
    case "CONNECTION__STATUS_CHANGED":
      if (event.online) {
        return null;
      }

      return createSyncDisabled();
    case "PRACTICE_EDITOR__PRACTICE_SUBMITTED":
    case "PRACTICE_MANAGER__PRACTICE_DELETED":
    case "PROJECT_EDITOR__PROJECT_SUBMITTED":
    case "PROJECT_MANAGER__PROJECT_DELETED":
    case "TASK_EDITOR__TASK_SUBMITTED":
    case "TASK_MANAGER__TASK_DELETED":
    case "TASK_MANAGER__TASK_COMPLETED":
    case "PRACTICE_MANAGER__PRACTICE_COMPLETED":
      if (state) {
        return {
          ...state,
          timestamp: null,
        };
      }

      return state;
    case "SYNC__STARTED":
      if (state?.status === "ENABLED") {
        return {
          ...state,
          inProgress: true,
        };
      }

      return state;
    case "SYNC__COMPLETED":
      if (state?.status === "ENABLED") {
        return {
          ...createSyncEnabled(),
          timestamp: Date.now(),
        };
      }

      return state;
    case "SYNC__FAILED":
      if (state?.status === "ENABLED") {
        return {
          ...createSyncFailed(),
          timestamp: Date.now(),
        };
      }

      return state;
    default:
      return state;
  }
};
