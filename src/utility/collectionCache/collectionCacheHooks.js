// @flow strict

import type { CollectionCache } from "./collectionCacheTypes";

import React from "react";

export function useCollectionCacheValue<IdType, ValueType>(
  cache: CollectionCache<IdType, ValueType>,
  id: IdType
): ValueType {
  const [value, setValue] = React.useState(cache.get(id));

  if (value instanceof Error) {
    throw value;
  }

  if (value instanceof Promise) {
    throw value;
  }

  React.useEffect(() => {
    return cache.listen((changedId) => {
      if (changedId === id) {
        const nextValue = cache.get(id);

        if (!(nextValue instanceof Promise)) {
          setValue(nextValue);
        }
      }
    });
  }, [cache, id]);

  return value;
}

export function useCollectionCacheEntries<IdType, ValueType>(
  cache: CollectionCache<IdType, ValueType>,
  ids: IdType[]
): [IdType, ValueType][] {
  const selectRawEntries = React.useCallback(() => {
    return ids.map((id) => [id, cache.get(id)]);
  }, [cache, ids]);

  const [rawEntries, setRawEntries] = React.useState(selectRawEntries);

  const entries = React.useMemo(() => {
    const entries: [IdType, ValueType][] = [];

    for (const [id, value] of rawEntries) {
      if (value instanceof Error) {
        throw value;
      }

      if (value instanceof Promise) {
        throw value;
      }

      entries.push([id, value]);
    }

    return entries;
  }, [rawEntries]);

  React.useEffect(() => {
    const listeners = ids.map((id) => {
      return cache.listen((changedValueId) => {
        if (changedValueId === id) {
          const nextValue = cache.get(id);

          if (!(nextValue instanceof Promise)) {
            setRawEntries(selectRawEntries);
          }
        }
      });
    });

    return () => {
      listeners.forEach((cancel) => cancel());
    };
  }, [cache, ids, selectRawEntries]);

  return entries;
}
