// @flow strict

import type {
  ActivityName,
  Activity,
} from "application/activity/activityTypes";
import type { Match } from "react-router-dom";

import { UnreachableCodeError } from "utility/unreachableCodeError/unreachableCodeErrorUtils";
import { createTypedString } from "utility/typedString/typedStringUtils";

export const createActivity = (activity: Activity): Activity => activity;

export const createActivityRoutePattern = (name: ActivityName): string => {
  switch (name) {
    case "PROJECTS_MANAGEMENT":
      return "/projects";
    case "PROJECT_CREATION":
      return "/project/create";
    case "PROJECT_EDITING":
      return "/project/:projectId/edit";
    case "PROJECT_MANAGEMENT":
      return "/project/:projectId";
    case "TASK_CREATION":
      return "/project/:projectId/task/create";
    case "TASK_EDITING":
      return "/project/:projectId/task/:taskId/edit";
    case "TASK_MANAGEMENT":
      return "/project/:projectId/task/:taskId";
    case "PRACTICES_MANAGEMENT":
      return "/practices";
    case "PRACTICE_CREATION":
      return "/practice/create";
    case "PRACTICE_EDITING":
      return "/practice/:practiceId/edit";
    case "PRACTICE_MANAGEMENT":
      return "/practice/:practiceId";
    case "NAVIGATION":
      return "/navigation";
    case "SETTINGS_MANAGEMENT":
      return "/settings";
    default:
      throw new UnreachableCodeError(name);
  }
};

export const createActivityRoute = (activity: Activity): string => {
  switch (activity.name) {
    case "PROJECTS_MANAGEMENT":
      return "/projects";
    case "PROJECT_CREATION":
      return "/project/create";
    case "PROJECT_MANAGEMENT":
      return `/project/${activity.projectId}`;
    case "PROJECT_EDITING":
      return `/project/${activity.projectId}/edit`;
    case "TASK_CREATION":
      return `/project/${activity.projectId}/task/create`;
    case "TASK_MANAGEMENT":
      return `/project/${activity.projectId}/task/${activity.taskId}`;
    case "TASK_EDITING":
      return `/project/${activity.projectId}/task/${activity.taskId}/edit`;
    case "PRACTICES_MANAGEMENT":
      return `/practices`;
    case "PRACTICE_CREATION":
      return "/practice/create";
    case "PRACTICE_MANAGEMENT":
      return `/practice/${activity.practiceId}`;
    case "PRACTICE_EDITING":
      return `/practice/${activity.practiceId}/edit`;
    case "NAVIGATION":
      return "/navigation";
    case "SETTINGS_MANAGEMENT":
      return "/settings";
    default:
      throw new UnreachableCodeError(activity.name);
  }
};

export const createActivityFromMatch = (match: Match): null | Activity => {
  switch (match.path) {
    case createActivityRoutePattern("PROJECTS_MANAGEMENT"): {
      return createActivity({
        name: "PROJECTS_MANAGEMENT",
      });
    }
    case createActivityRoutePattern("PROJECT_CREATION"): {
      return createActivity({
        name: "PROJECT_CREATION",
      });
    }
    case createActivityRoutePattern("PROJECT_EDITING"): {
      const {
        params: { projectId },
      } = match;

      return createActivity({
        name: "PROJECT_EDITING",
        projectId: createTypedString(projectId),
      });
    }
    case createActivityRoutePattern("PROJECT_MANAGEMENT"): {
      const {
        params: { projectId },
      } = match;

      return createActivity({
        name: "PROJECT_MANAGEMENT",
        projectId: createTypedString(projectId),
      });
    }
    case createActivityRoutePattern("TASK_CREATION"): {
      const {
        params: { projectId },
      } = match;

      return createActivity({
        name: "TASK_CREATION",
        projectId: createTypedString(projectId),
      });
    }
    case createActivityRoutePattern("TASK_EDITING"): {
      const {
        params: { taskId, projectId },
      } = match;

      return createActivity({
        name: "TASK_EDITING",
        projectId: createTypedString(projectId),
        taskId: createTypedString(taskId),
      });
    }
    case createActivityRoutePattern("TASK_MANAGEMENT"): {
      const {
        params: { taskId, projectId },
      } = match;

      return createActivity({
        name: "TASK_MANAGEMENT",
        projectId: createTypedString(projectId),
        taskId: createTypedString(taskId),
      });
    }
    case createActivityRoutePattern("PRACTICES_MANAGEMENT"): {
      return createActivity({
        name: "PRACTICES_MANAGEMENT",
      });
    }
    case createActivityRoutePattern("PRACTICE_CREATION"): {
      return createActivity({
        name: "PRACTICE_CREATION",
      });
    }
    case createActivityRoutePattern("PRACTICE_EDITING"): {
      const {
        params: { practiceId },
      } = match;

      return createActivity({
        name: "PRACTICE_EDITING",
        practiceId: createTypedString(practiceId),
      });
    }
    case createActivityRoutePattern("PRACTICE_MANAGEMENT"): {
      const {
        params: { practiceId },
      } = match;

      return createActivity({
        name: "PRACTICE_MANAGEMENT",
        practiceId: createTypedString(practiceId),
      });
    }
    case createActivityRoutePattern("NAVIGATION"): {
      return createActivity({
        name: "NAVIGATION",
      });
    }
    case createActivityRoutePattern("SETTINGS_MANAGEMENT"): {
      return createActivity({
        name: "SETTINGS_MANAGEMENT",
      });
    }
    default:
      return null;
  }
};
