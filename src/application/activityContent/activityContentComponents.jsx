// @flow strict

import * as React from "react";
import { Switch, Route } from "react-router-dom";

import { createActivityRoutePattern } from "application/activity/activityUtils";
import {
  SyncEnableFailedError,
  SyncDisableFailedError,
} from "application/sync/syncUtils";
import { NavigationComponent } from "../navigation/navigationComponents.jsx";
import { ErrorBoundaryComponent } from "../errorBoundary/errorBoundaryComponents.jsx";
import { ErrorMessageComponent } from "../errorMessage/errorMessageComponents.jsx";
import { TaskNotFoundError } from "../task/taskConstants";
import { TaskManagerComponent } from "../taskManager/taskManagerComponents.jsx";
import { ProjectsManagerComponent } from "../projectsManager/components/projectsManagerComponents.jsx";
import { ProjectNotFoundError } from "../project/projectConstants";
import { ProjectEditorComponent } from "../projectEditor/components/projectEditorComponents.jsx";
import { ProjectManagerComponent } from "../projectManager/projectManagerComponents.jsx";
import { PracticesManagerComponent } from "../practicesManager/components/practicesManagerComponents.jsx";
import { PracticeNotFoundError } from "../practice/practiceErrors";
import { PracticeEditorComponent } from "../practiceEditor/components/practiceEditorComponents.jsx";
import { PracticeManagerComponent } from "../practiceManager/practiceManagerComponents.jsx";
import { SettingsManagerComponent } from "../settingsManager/settingsManagerComponents.jsx";
import { TaskEditorComponent } from "../taskEditor/components/taskEditorComponents.jsx";

export const ActivityContentComponent = (): React.Node => {
  return (
    <Switch>
      <Route path={createActivityRoutePattern("NAVIGATION")}>
        <NavigationComponent />
      </Route>
      <Route
        path={[
          createActivityRoutePattern("TASK_CREATION"),
          createActivityRoutePattern("TASK_EDITING"),
        ]}
      >
        <ErrorBoundaryComponent
          fallbackView={ErrorMessageComponent}
          errorConstructorsToCatch={[TaskNotFoundError]}
        >
          <TaskEditorComponent />
        </ErrorBoundaryComponent>
      </Route>
      <Route path={createActivityRoutePattern("TASK_MANAGEMENT")}>
        <ErrorBoundaryComponent
          fallbackView={ErrorMessageComponent}
          errorConstructorsToCatch={[TaskNotFoundError]}
        >
          <TaskManagerComponent />
        </ErrorBoundaryComponent>
      </Route>
      <Route path={createActivityRoutePattern("PROJECTS_MANAGEMENT")}>
        <ProjectsManagerComponent />
      </Route>
      <Route
        path={[
          createActivityRoutePattern("PROJECT_CREATION"),
          createActivityRoutePattern("PROJECT_EDITING"),
        ]}
      >
        <ErrorBoundaryComponent
          fallbackView={ErrorMessageComponent}
          errorConstructorsToCatch={[ProjectNotFoundError]}
        >
          <ProjectEditorComponent />
        </ErrorBoundaryComponent>
      </Route>
      <Route path={createActivityRoutePattern("PROJECT_MANAGEMENT")}>
        <ErrorBoundaryComponent
          fallbackView={ErrorMessageComponent}
          errorConstructorsToCatch={[ProjectNotFoundError]}
        >
          <ProjectManagerComponent />
        </ErrorBoundaryComponent>
      </Route>
      <Route path={createActivityRoutePattern("PRACTICES_MANAGEMENT")}>
        <PracticesManagerComponent />
      </Route>
      <Route
        path={[
          createActivityRoutePattern("PRACTICE_CREATION"),
          createActivityRoutePattern("PRACTICE_EDITING"),
        ]}
      >
        <ErrorBoundaryComponent
          fallbackView={ErrorMessageComponent}
          errorConstructorsToCatch={[PracticeNotFoundError]}
        >
          <PracticeEditorComponent />
        </ErrorBoundaryComponent>
      </Route>
      <Route path={createActivityRoutePattern("PRACTICE_MANAGEMENT")}>
        <ErrorBoundaryComponent
          fallbackView={ErrorMessageComponent}
          errorConstructorsToCatch={[PracticeNotFoundError]}
        >
          <PracticeManagerComponent />
        </ErrorBoundaryComponent>
      </Route>
      <Route path={createActivityRoutePattern("SETTINGS_MANAGEMENT")}>
        <ErrorBoundaryComponent
          fallbackView={ErrorMessageComponent}
          errorConstructorsToCatch={[
            SyncEnableFailedError,
            SyncDisableFailedError,
          ]}
        >
          <SettingsManagerComponent />
        </ErrorBoundaryComponent>
      </Route>
    </Switch>
  );
};
