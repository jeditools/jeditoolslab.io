// @flow

export const checkMarkEmoji = "✔️️";

export const checkMarkButtonEmoji = "✅️️";

export const checkBoxEmoji = "☑️";

export const gearEmoji = "⚙️";

export const openBookEmoji = "️📖";

export const memoEmoji = "📝";

export const clipboardEmoji = "📋";

export const booksEmoji = "📚";
