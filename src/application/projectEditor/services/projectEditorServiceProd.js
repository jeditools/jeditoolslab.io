// @flow strict

import type { ProjectEditorService } from "../projectEditorTypes";

import { ProjectWithNameAlreadyExistsError } from "../../project/projectConstants";
import { projectServiceProd } from "../../project/services/projectServiceProd";

export const projectEditorServiceProd: ProjectEditorService = {
  async submit(editedProjectId, project, params) {
    const projectEntries = await projectServiceProd.getAllProjectEntries();

    const projectEntryWithSameName = projectEntries.find(
      ([, currentProject]) => currentProject.name === project.name
    );

    if (
      projectEntryWithSameName &&
      projectEntryWithSameName[0] !== editedProjectId
    ) {
      throw new ProjectWithNameAlreadyExistsError(project.name);
    }

    if (editedProjectId) {
      await projectServiceProd.putProject(editedProjectId, project);

      return editedProjectId;
    } else {
      return projectServiceProd.addProject(project);
    }
  },
};
