// @flow strict

import type { ThemeMode } from "./themeTypes";

import { useState } from "../state/stateHooks";

export const useThemeMode = (): ThemeMode => {
  const { themeMode } = useState();

  return themeMode;
};
