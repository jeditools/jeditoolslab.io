// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";

type Props = {|
  children: React.Node,
  label: string,
|};

export const SettingsManagerSectionComponent = (props: Props): React.Node => {
  const { children, label } = props;

  return (
    <GridLayoutComponent mainAxis="y">
      <TextComponent size="l">{label}</TextComponent>
      {children}
    </GridLayoutComponent>
  );
};
