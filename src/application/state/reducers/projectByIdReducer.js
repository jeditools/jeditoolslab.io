// @flow strict

import type { Reducer } from "../../../utility/reducer/reducerTypes";
import type { Event } from "../../event/eventTypes";
import type { Project } from "../../project/projectTypes";

import { DictionaryCache } from "@draftup/utility.js";
import { createReducer } from "utility/reducer/reducerUtils";

export const projectByIdCacheReducer: Reducer<
  DictionaryCache<string, Project>,
  Event
> = createReducer((state, event) => {
  switch (event.name) {
    case "PROJECTS_MANAGER__DATA_RECIEVED": {
      const { projectEntries } = event.data;

      return projectEntries.reduce(
        (acc, [id, item]) => acc.writeValue(id, item),
        new DictionaryCache(state)
      );
    }
    case "PROJECT_MANAGER__DATA_RECIEVED": {
      const { projectEntry } = event.data;

      const [id, project] = projectEntry;

      return new DictionaryCache(state).writeValue(id, project);
    }
    case "PROJECT_MANAGER__PROJECT_DELETED": {
      return new DictionaryCache(state).deleteEntry(event.projectId);
    }
    default:
      return state;
  }
});
