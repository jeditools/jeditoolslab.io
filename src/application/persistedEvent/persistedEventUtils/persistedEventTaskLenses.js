// @flow strict

import type {
  PersistedEventEntries,
  PersistedEventEntriesTaskLense,
} from "application/persistedEvent/persistedEventTypes";

import { createTask, createTaskConstructor } from "../../task/taskCreators";
import { PersistedEventError } from "./persistedEventErrors";

import {
  createEntriesConstructor,
  createEntriesLense,
} from "utility/entry/entryUtils";

export const createPersistedEventEntriesTaskLense = (
  persistedEventEntries: PersistedEventEntries
): PersistedEventEntriesTaskLense => ({
  selectTaskRelatedEventEntries(taskId) {
    return persistedEventEntries.filter(([id, event]) => {
      switch (event.name) {
        case "TaskCreated":
        case "TaskUpdated":
        case "TaskCompleted":
        case "TaskDeleted": {
          if (!taskId || event.taskId === taskId) {
            return true;
          }

          return false;
        }
        default:
          return false;
      }
    });
  },
  selectTaskEntries(taskIds) {
    const tasksRelatedEventEntries = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskRelatedEventEntries();

    const deletedTaskIds = [];

    const taskEntries = tasksRelatedEventEntries
      .reduce((acc, [id, event]) => {
        switch (event.name) {
          case "TaskCreated": {
            const { taskId: persistedTaskId, task: persistedTask } = event;

            if (!deletedTaskIds.includes(persistedTaskId)) {
              const task = createTask(persistedTask.projectId, event.timestamp);

              const taskConstructor = createTaskConstructor(task)
                .changeDescription(persistedTask.description)
                .changeProjectId(persistedTask.projectId);

              return acc.setEntry(persistedTaskId, taskConstructor.return());
            }

            return acc;
          }
          case "TaskUpdated": {
            const { taskId: persistedTaskId, task: persistedTask } = event;

            if (!deletedTaskIds.includes(persistedTaskId)) {
              const task = createEntriesLense(acc.return()).selectItem(
                persistedTaskId
              );

              if (task === null) {
                throw new PersistedEventError("Missing task creation event.");
              }

              const taskConstructor = createTaskConstructor(task)
                .changeDescription(persistedTask.description)
                .changeProjectId(persistedTask.projectId);

              return acc.setEntry(persistedTaskId, taskConstructor.return());
            }

            return acc;
          }
          case "TaskCompleted": {
            const { taskId: persistedTaskId } = event;

            if (!deletedTaskIds.includes(persistedTaskId)) {
              const prevTask = createEntriesLense(acc.return()).selectItem(
                event.taskId
              );

              // TODO: исправляет ошибку повторного завершения задачи в результате слияния двух версий истории,
              // скорее всего проблему нужно решать чисткой повторящюхся событий.
              if (prevTask && prevTask.completionTimestamp === null) {
                const taskConstructor = createTaskConstructor(
                  prevTask
                ).complete(event.timestamp);

                return acc.setEntry(event.taskId, taskConstructor.return());
              }
            }

            return acc;
          }
          case "TaskDeleted": {
            const { taskId: persistedTaskId } = event;

            if (!deletedTaskIds.includes(persistedTaskId)) {
              deletedTaskIds.push(persistedTaskId);

              return acc.deleteEntry(event.taskId);
            }

            return acc;
          }
          default:
            return acc;
        }
      }, createEntriesConstructor([]))
      .return();

    if (taskIds) {
      return taskEntries.filter(([taskId]) => taskIds.includes(taskId));
    }

    return taskEntries;
  },
  selectProjectRelatedTaskIds(projectId) {
    return persistedEventEntries.reduce((acc, [id, event]) => {
      if (event.name === "TaskCreated" && event.task.projectId === projectId) {
        acc.push(event.taskId);
      }

      if (event.name === "TaskUpdated") {
        if (event.task.projectId === projectId) {
          if (!acc.includes(event.taskId)) {
            acc.push(event.taskId);
          }
        } else {
          if (acc.includes(event.taskId)) {
            return acc.filter((currentId) => currentId !== event.taskId);
          }
        }
      }

      return acc;
    }, []);
  },
  selectTask(taskId) {
    const taskEntries = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskEntries();

    for (let [id, task] of taskEntries) {
      if (id === taskId) {
        return task;
      }
    }

    return null;
  },
});
