// @flow strict

import type { Activity } from "./activityTypes";
import type { Event } from "../event/eventTypes";

export const currentActivityReducer = (
  state: Activity,
  event: Event
): Activity => {
  switch (event.name) {
    case "ROOT__NEXT_ACTIVITY_RECIEVED":
      return event.nextActivity;
    default:
      return state;
  }
};
