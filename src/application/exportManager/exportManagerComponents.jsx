// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { CardComponent } from "ui/card/cardComponents";
import { useIntlDictionary } from "../intl/intlHooks";
import { useServices } from "../services/servicesHooks";
import { SettingsManagerSectionComponent } from "../settingsManager/settingsManagerSectionComponents.jsx";
import { useSafeAsyncController } from "../../utility/safeAsyncController/safeAsyncControllerHooks";

export const ExportManagerComponent = (): React.Node => {
  const { exportManager: dictionary } = useIntlDictionary();

  const { exportManagerService } = useServices();

  const { wrapAsyncSafely } = useSafeAsyncController();

  const downloadData = React.useCallback(() => {
    wrapAsyncSafely(exportManagerService.downloadData());
  }, [exportManagerService, wrapAsyncSafely]);

  return (
    <SettingsManagerSectionComponent label={dictionary.sectionLabelText}>
      <GridLayoutComponent mainAxis="y">
        <TextComponent>{dictionary.descriptionText}</TextComponent>
        <GridLayoutComponent mainAxis="x" primaryAlignment="start">
          <ActionControlComponent type="button" onClick={downloadData}>
            <CardComponent floating>
              <BoxComponent>{dictionary.actionLabelText}</BoxComponent>
            </CardComponent>
          </ActionControlComponent>
        </GridLayoutComponent>
      </GridLayoutComponent>
    </SettingsManagerSectionComponent>
  );
};
