// @flow strict

import type { Services } from "application/services/servicesTypes";

import * as React from "react";

const services: Services = (() => {
  if (process.env.NODE_ENV === "production") {
    return require("./servicesProd").servicesProd;
  }

  if (process.env.REACT_APP__SERVICES_TYPE === "production") {
    return require("./servicesProd").servicesProd;
  }

  return require("./servicesDev").servicesDev;
})();

export const ServicesContext: React.Context<Services> =
  React.createContext(services);
