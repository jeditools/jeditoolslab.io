// @flow strict

// Project events

type PersistedProjectId = string;

export type PersistedProject = {|
  name: string,
  details: string,
|};

type ProjectCreated = {
  name: "ProjectCreated",
  timestamp: number,
  projectId: PersistedProjectId,
  project: PersistedProject,
};

type ProjectUpdated = {
  name: "ProjectUpdated",
  timestamp: number,
  projectId: PersistedProjectId,
  project: PersistedProject,
};

type ProjectDeleted = {
  name: "ProjectDeleted",
  timestamp: number,
  projectId: PersistedProjectId,
};

type ProjectEventV1 = ProjectCreated | ProjectUpdated | ProjectDeleted;

// Task events

type PersistedTaskId = string;

type PersistedTask = {|
  description: string,
  projectId: string,
|};

type TaskCreated = {|
  name: "TaskCreated",
  timestamp: number,
  taskId: PersistedTaskId,
  task: PersistedTask,
|};

type TaskUpdated = {|
  name: "TaskUpdated",
  timestamp: number,
  taskId: PersistedTaskId,
  task: PersistedTask,
|};

type TaskCompleted = {|
  name: "TaskCompleted",
  timestamp: number,
  taskId: PersistedTaskId,
|};

type TaskDeleted = {|
  name: "TaskDeleted",
  timestamp: number,
  taskId: PersistedTaskId,
|};

type TaskEventV1 = TaskCreated | TaskUpdated | TaskCompleted | TaskDeleted;

// Practice events

type PersistedPracticeId = string;

type PersistedPractice = {|
  name: string,
  details: string,
  regularityInDays: number,
|};

type PracticeCreated = {|
  name: "PracticeCreated",
  timestamp: number,
  practiceId: PersistedPracticeId,
  practice: PersistedPractice,
|};

type PracticeUpdated = {|
  name: "PracticeUpdated",
  timestamp: number,
  practiceId: PersistedPracticeId,
  practice: PersistedPractice,
|};

type PracticeCompleted = {|
  name: "PracticeCompleted",
  timestamp: number,
  practiceId: PersistedPracticeId,
|};

type PracticeDeleted = {|
  name: "PracticeDeleted",
  timestamp: number,
  practiceId: PersistedPracticeId,
|};

type PracticeEventV1 =
  | PracticeCreated
  | PracticeUpdated
  | PracticeCompleted
  | PracticeDeleted;

// Event union

export type PersistedEventIdV1 = string;

export type PersistedEventV1 = ProjectEventV1 | TaskEventV1 | PracticeEventV1;
