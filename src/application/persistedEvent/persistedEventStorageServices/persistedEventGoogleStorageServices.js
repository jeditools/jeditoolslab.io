// @flow strict

import type { PersistedEventStorageService } from "application/persistedEvent/persistedEventTypes";

import { createAsyncCache } from "utility/asyncCache/asyncCacheUtils";
import { PERSISTED_EVENT__STORAGE_KEY } from "application/persistedEvent/persistedEventConstants";
import { googleDriveService } from "utility/googleDrive/googleDriveServices";
import { GoogleDriveFileNotFoundError } from "utility/googleDrive/googleDriveUtils";

const fileIdCache = createAsyncCache((): Promise<number> => {
  const getFileId = async () => {
    try {
      const id = await googleDriveService.getFileId(
        PERSISTED_EVENT__STORAGE_KEY
      );

      return id;
    } catch (error) {
      if (error instanceof GoogleDriveFileNotFoundError) {
        await googleDriveService.createFile(PERSISTED_EVENT__STORAGE_KEY);

        return getFileId();
      } else {
        throw error;
      }
    }
  };

  return getFileId();
});

export const persistedEventGoogleStorageService: PersistedEventStorageService =
  {
    async write(persistedEventEntries) {
      const id = await fileIdCache.read();

      await googleDriveService.writeFile(
        id,
        JSON.stringify(persistedEventEntries)
      );
    },
    async read() {
      const id = await fileIdCache.read();

      const content = await googleDriveService.readFile(id);

      if (!content) {
        await persistedEventGoogleStorageService.write([]);

        return persistedEventGoogleStorageService.read();
      } else {
        return JSON.parse(content);
      }
    },
  };
