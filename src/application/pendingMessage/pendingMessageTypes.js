// @flow strict

export type PendingMessageDictionary = {|
  messageText: string,
|};
