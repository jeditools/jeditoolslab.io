// @flow strict

import * as React from "react";
import { useHistory } from "react-router-dom";

import { TextComponent } from "ui/text/textComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import { useSyncEnabled } from "application/sync/syncHooks";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { CardComponent } from "ui/card/cardComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { WarningAmberIconComponent } from "ui/icon";
import { useEventDispatcher } from "../event/eventHooks";
import { usePractice } from "../practice/practiceHooks";
import { createPracticeLense } from "../practice/practiceLenses";
import { createActivityRoute } from "../activity/activityUtils";
import { useServices } from "../services/servicesHooks";
import { useIntlDictionary } from "../intl/intlHooks";
import { useManagedPracticeId } from "./practiceManagerHooks";
import { ParagraphComponent } from "../../ui/paragraph/paragraphComponents.jsx";
import {
  DropDownMenuComponent,
  DropDownMenuItemComponent,
} from "ui/dropDownMenu/dropDownMenuComponents";
import { useAsyncProcessController } from "../../utility/asyncProcessController/asyncProcessControllerHooks";

export const PracticeManagerComponent = (): React.Node => {
  const dispatch = useEventDispatcher();

  const practiceId = useManagedPracticeId();

  const { process, wrapAsync } = useAsyncProcessController();

  const history = useHistory();

  const { practiceManagerService, practiceService } = useServices();

  const practice = usePractice(practiceId);

  const { practiceManager: dictionary } = useIntlDictionary();

  const practiceLense = React.useMemo(() => {
    return createPracticeLense(practice);
  }, [practice]);

  const syncEnabled = useSyncEnabled();

  const goToPracticesManagement = React.useCallback(() => {
    history.replace(createActivityRoute({ name: "PRACTICES_MANAGEMENT" }));
  }, [history]);

  const handleSuccessfulCompletion = React.useCallback(() => {
    dispatch({
      name: "PRACTICE_MANAGER__PRACTICE_COMPLETED",
      practiceId,
    });

    goToPracticesManagement();
  }, [dispatch, goToPracticesManagement, practiceId]);

  const completePractice = React.useCallback(() => {
    wrapAsync(
      practiceService
        .completePractice(practiceId)
        .then(handleSuccessfulCompletion)
    );
  }, [handleSuccessfulCompletion, wrapAsync, practiceId, practiceService]);

  const editPractice = React.useCallback(() => {
    const route = createActivityRoute({ name: "PRACTICE_EDITING", practiceId });

    history.push(route);
  }, [history, practiceId]);

  const handleDeleteSucceeded = React.useCallback(() => {
    dispatch({
      name: "PRACTICE_MANAGER__PRACTICE_DELETED",
      practiceId: practiceId,
    });

    history.replace(createActivityRoute({ name: "PRACTICES_MANAGEMENT" }));
  }, [dispatch, practiceId, history]);

  const deletePractice = React.useCallback(() => {
    const confirmed = window.confirm(dictionary.deleteConfirmText);

    if (confirmed) {
      wrapAsync(
        practiceService.deletePractice(practiceId).then(handleDeleteSucceeded)
      );
    }
  }, [
    dictionary.deleteConfirmText,
    handleDeleteSucceeded,
    wrapAsync,
    practiceId,
    practiceService,
  ]);

  React.useLayoutEffect(() => {
    const promise = practiceManagerService.getData(practiceId).then((data) => {
      dispatch({
        name: "PRACTICE_MANAGER__DATA_RECIEVED",
        data,
      });
    });

    wrapAsync(promise);
  }, [dispatch, syncEnabled, practiceId, practiceManagerService, wrapAsync]);

  const nextCompletionText = React.useMemo(() => {
    const date = createPracticeLense(practice).selectNextCompletionDate();

    if (date) {
      return dictionary.nextCompletionText(date.getTime());
    }

    return dictionary.initialCompletionText;
  }, [dictionary, practice]);

  const nextCompletionIcon = React.useMemo(() => {
    if (practiceLense.shouldBeAlreadyCompleted()) {
      return <WarningAmberIconComponent />;
    }

    return null;
  }, [practiceLense]);

  const nextCompletionTextColor = React.useMemo(() => {
    if (practiceLense.shouldBeAlreadyCompleted()) {
      return "critical";
    }

    return "secondary";
  }, [practiceLense]);

  return (
    <GridLayoutComponent mainAxis="y">
      <GridLayoutComponent mainAxis="y">
        <InlineLayoutComponent>
          <TextComponent size="l">{practice.name}</TextComponent>
        </InlineLayoutComponent>
        {practice.details && (
          <InlineLayoutComponent>
            <TextComponent>
              <ParagraphComponent text={practice.details} />
            </TextComponent>
          </InlineLayoutComponent>
        )}
        <TextComponent color={nextCompletionTextColor} size="s">
          <InlineLayoutComponent>
            {nextCompletionIcon ? (
              <>
                {nextCompletionIcon} {nextCompletionText}
              </>
            ) : (
              nextCompletionText
            )}
          </InlineLayoutComponent>
        </TextComponent>
      </GridLayoutComponent>
      <GridLayoutComponent
        mainAxis="x"
        primaryAlignment="apart"
        secondaryAlignment="center"
      >
        <DropDownMenuComponent>
          <DropDownMenuItemComponent
            text={dictionary.editActionText}
            onSelect={editPractice}
          />
          <DropDownMenuItemComponent
            text={dictionary.deleteActionText}
            onSelect={deletePractice}
          />
        </DropDownMenuComponent>
        <ActionControlComponent
          disabled={process.stage.name === "PENDING"}
          type="button"
          onClick={completePractice}
        >
          <CardComponent floating>
            <BoxComponent>{dictionary.completeActionText}</BoxComponent>
          </CardComponent>
        </ActionControlComponent>
      </GridLayoutComponent>
    </GridLayoutComponent>
  );
};
