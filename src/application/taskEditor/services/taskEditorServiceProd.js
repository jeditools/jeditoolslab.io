// @flow strict

import type { TaskEditorService } from "../taskEditorTypes";

import { normalizeTaskEditorFormData } from "application/taskEditorForm/taskEditorFormUtils";
import { taskServiceProd } from "../../task/services/taskServiceProd";
import { projectServiceProd } from "../../project/services/projectServiceProd";

export const taskEditorServiceProd: TaskEditorService = {
  async getData(editedTaskId) {
    const projectEntries = await projectServiceProd.getAllProjectEntries();

    let editedTaskEntry = null;

    if (editedTaskId) {
      const editedTask = await taskServiceProd.getTask(editedTaskId);

      editedTaskEntry = [editedTaskId, editedTask];
    }

    return { projectEntries, editedTaskEntry };
  },
  async submitFormData(editedTaskId, formData) {
    const task = normalizeTaskEditorFormData(formData).toTask();

    let taskId;

    if (editedTaskId) {
      taskId = await taskServiceProd.editTask(editedTaskId, task);
    } else {
      taskId = await taskServiceProd.addTask(task);
    }

    return taskId;
  },
};
