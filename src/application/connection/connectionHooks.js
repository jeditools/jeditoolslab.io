// @flow strict

import React from "react";

import { useEventDispatcher } from "application/event/eventHooks";
import { useServices } from "application/services/servicesHooks";

export const useConnectionProcess = () => {
  const dispatch = useEventDispatcher();

  const { documentService } = useServices();

  React.useEffect(() => {
    return documentService.listenToConnectionStatusChange((online) => {
      dispatch({
        name: "CONNECTION__STATUS_CHANGED",
        online,
      });
    });
  }, [dispatch, documentService]);
};
