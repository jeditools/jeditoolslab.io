// @flow strict

import * as React from "react";

import "./gridLayoutStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type GridLayoutAxis = "x" | "y";

type GridLayoutAlignment =
  | "start"
  | "center"
  | "end"
  | "stretch"
  | "spread"
  | "apart";

type GridLayoutGap = "auto" | "off";

type Props = {|
  mainAxis: GridLayoutAxis,
  primaryAlignment?: GridLayoutAlignment,
  secondaryAlignment?: GridLayoutAlignment,
  gap?: GridLayoutGap,
  children: React.Node,
|};

export const GridLayoutComponent = (props: Props): React.Node => {
  const {
    children,
    mainAxis,
    primaryAlignment = "stretch",
    secondaryAlignment = "stretch",
    gap = "auto",
  } = props;

  const blockClassName = createClassName("grid-layout");

  const wrapperClassName = blockClassName("wrapper");

  const slotClassName = blockClassName("slot", {
    mainAxis,
    primaryAlignment,
    secondaryAlignment,
    gap,
  });

  return (
    <div className={wrapperClassName}>
      <div className={slotClassName}>{children}</div>
    </div>
  );
};
