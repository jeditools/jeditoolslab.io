// @flow strict

import type { TaskId } from "../task/taskTypes";

import { useRouteMatch } from "react-router-dom";

import { createActivityRoutePattern } from "application/activity/activityUtils";

export const useManagedTaskId = (): TaskId => {
  const {
    params: { taskId },
  } = useRouteMatch(createActivityRoutePattern("TASK_MANAGEMENT"));

  if (typeof taskId === "string") {
    return taskId;
  }

  throw TypeError();
};
