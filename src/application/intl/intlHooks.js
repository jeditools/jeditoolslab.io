// @flow strict

import type { IntlDictionary } from "./intlTypes";

import { useState } from "../state/stateHooks";

export const useIntlDictionary = (): IntlDictionary => {
  const { intlDictionary } = useState();

  return intlDictionary;
};
