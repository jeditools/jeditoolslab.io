// @flow strict

import type { ProjectId } from "../project/projectTypes";

import { useRouteMatch } from "react-router-dom";

import { createActivityRoutePattern } from "application/activity/activityUtils";

export const useManagedProjectId = (): ProjectId => {
  const {
    params: { projectId },
  } = useRouteMatch(createActivityRoutePattern("PROJECT_MANAGEMENT"));

  if (typeof projectId === "string") {
    return projectId;
  }

  throw TypeError();
};
