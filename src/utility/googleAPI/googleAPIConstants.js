// @flow strict

export const GOOGLE_API__SCRIPT_URL = "https://apis.google.com/js/api.js";

// Ваш ключ API
export const GOOGLE_API__KEY = process.env.REACT_APP__GOOGLE_API__KEY;

// Ваш идентификатор клиента
export const GOOGLE_API__CLIENT_ID =
  process.env.REACT_APP__GOOGLE_API__CLIENT_ID;

export const GOOGLE_API__DISCOVERY_DOCS = [
  // Указание, что мы хотим использовать Google Drive API v3
  "https://www.googleapis.com/discovery/v1/apis/drive/v3/rest",
];

// Запрос доступа к application data folder
export const GOOGLE_API__SCOPE =
  "https://www.googleapis.com/auth/drive.appfolder";
