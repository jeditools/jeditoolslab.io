// @flow

import type { ProjectId } from "../project/projectTypes";
import type {
  Task,
  TaskConstructor,
  TaskEntriesLense,
  TaskEntry,
} from "./taskTypes";

import { IllegalTaskModification } from "./taskConstants";

export const createTask = (
  projectId: ProjectId,
  creationTimestamp: number
): Task => ({
  creationTimestamp,
  description: "",
  completionTimestamp: null,
  projectId,
});

export const createTaskConstructor = (task: Task): TaskConstructor => ({
  return() {
    return task;
  },
  changeDescription(nextValue) {
    if (task.completionTimestamp !== null) {
      throw new IllegalTaskModification();
    }

    return createTaskConstructor({
      ...task,
      description: nextValue,
    });
  },
  changeProjectId(nextValue) {
    if (task.completionTimestamp !== null) {
      throw new IllegalTaskModification();
    }

    return createTaskConstructor({
      ...task,
      projectId: nextValue,
    });
  },
  complete(timestamp) {
    if (task.completionTimestamp !== null) {
      throw new IllegalTaskModification();
    }

    return createTaskConstructor({
      ...task,
      completionTimestamp: timestamp,
    });
  },
});

export const createTaskEntriesLense = (
  taskEntries: TaskEntry[]
): TaskEntriesLense => ({
  selectCompletedIds() {
    return taskEntries
      .filter(([id, task]) => {
        return task.completionTimestamp !== null;
      })
      .sort(([id1, task1], [id2, task2]) => {
        if (task1.completionTimestamp && task2.completionTimestamp) {
          return task2.completionTimestamp - task1.completionTimestamp;
        }

        return 0;
      })
      .map(([id]) => id);
  },
  selectActiveIds() {
    return taskEntries
      .filter(([id, task]) => {
        return task.completionTimestamp === null;
      })
      .map(([id]) => id);
  },
  selectCompletedEntries() {
    return taskEntries.filter(([id, task]) => {
      return task.completionTimestamp !== null;
    });
  },
  selectActiveEntries() {
    return taskEntries.filter(([id, task]) => {
      return task.completionTimestamp === null;
    });
  },
});

export const createTaskFallback = (): Task => ({
  creationTimestamp: Date.now(),
  description: "description",
  completionTimestamp: null,
  projectId: "projectId",
});
