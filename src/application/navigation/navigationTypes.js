// @flow strict

export type NavigationDictionary = {|
  projectsManagementLinkText: string,
  practicesManagementLinkText: string,
  settingsManagementLinkText: string,
|};

export type NavigationActivityName = "NAVIGATION";

export type NavigationActivity = {|
  name: NavigationActivityName,
|};
