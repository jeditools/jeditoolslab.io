// @flow

export type ParagraphBlock =
  | {
      id: string,
      type: "TEXT",
      text: string,
    }
  | {
      id: string,
      type: "URL",
      url: URL,
    };
