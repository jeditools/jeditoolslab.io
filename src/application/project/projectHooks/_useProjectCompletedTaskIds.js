// @flow strict

import type { TaskId } from "../../task/taskTypes";
import type { ProjectId } from "../projectTypes";

import React from "react";

import { useState } from "../../state/stateHooks";
import { createTaskEntriesLense } from "../../task/taskCreators";

export const useProjectCompletedTaskIds = (projectId: ProjectId): TaskId[] => {
  const { taskByIdCache } = useState();

  return React.useMemo(() => {
    const projectTaskEntries = taskByIdCache
      .readEntries()
      .filter(([taskId, task]) => task.projectId === projectId);

    return createTaskEntriesLense(projectTaskEntries).selectCompletedIds();
  }, [projectId, taskByIdCache]);
};
