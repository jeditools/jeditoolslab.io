// @flow strict

import type { ProjectEditorDictionary } from "../projectEditorTypes";

export const projectEditorDictionaryRU: ProjectEditorDictionary = {
  nameLabelText: "Название",
  nameRequiredHintText: "Поле обязательно для заполнения",
  nameTakenHintText: "Проект с таким названием уже существует",
  namePlaceholderText: "Название проекта",
  detailsLabelText: "Подробности",
  detailsPlaceholderText: "Подробности проекта",
  submitActionText: "Сохранить",
};
