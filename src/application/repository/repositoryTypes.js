// @flow strict

import type {
  PracticeEntry,
  PracticeId,
} from "application/practice/practiceTypes";
import type { TaskEntry, TaskId } from "../task/taskTypes";
import type { ProjectEntry, ProjectId } from "../project/projectTypes";

export type RepositoryQuery = {|
  projectIds?: ProjectId[],
  taskIds?: TaskId[],
  practiceIds?: PracticeId[],
  allProjects?: boolean,
  allTasks?: boolean,
  allPractices?: boolean,
|};

export type RepositoryData = {|
  projectEntries: ProjectEntry[],
  taskEntries: TaskEntry[],
  practiceEntries: PracticeEntry[],
|};

export type RepositoryService = {|
  getData(query: RepositoryQuery): Promise<RepositoryData>,
|};
