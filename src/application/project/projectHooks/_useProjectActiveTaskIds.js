// @flow strict

import type { TaskId } from "../../task/taskTypes";
import type { ProjectId } from "../projectTypes";

import { useState } from "../../state/stateHooks";
import { createTaskEntriesLense } from "../../task/taskCreators";

import React from "react";

export const useProjectActiveTaskIds = (projectId: ProjectId): TaskId[] => {
  const { taskByIdCache } = useState();

  return React.useMemo(() => {
    const projectTaskEntries = taskByIdCache
      .readEntries()
      .filter(([taskId, task]) => task.projectId === projectId)
      .sort(([, task1], [, task2]) => {
        return task1.creationTimestamp - task2.creationTimestamp;
      });

    return createTaskEntriesLense(projectTaskEntries).selectActiveIds();
  }, [projectId, taskByIdCache]);
};
