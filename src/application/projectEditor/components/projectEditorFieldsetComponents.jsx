// @flow strict

import type { Project } from "../../project/projectTypes";
import type { ProjectFormViolation } from "../../projectForm/projectFormTypes";

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { TextControlComponent } from "ui/textControl/textControlComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { LabelComponent } from "ui/label/labelComponents";
import { useIntlDictionary } from "../../intl/intlHooks";
import { createProjectConstructor } from "../../project/projectCreators";

type Props = {
  disabled: boolean,
  data: Project,
  violations: ProjectFormViolation[],
  onDataChange(nextData: Project): void,
};

export const ProjectEditorFieldsetComponent = (props: Props): React.Node => {
  const { disabled, data, violations, onDataChange } = props;

  const { projectEditor: dictionary } = useIntlDictionary();

  const changeProjectName = React.useCallback(
    (nextValue: string) => {
      const nextData = createProjectConstructor(data)
        .changeName(nextValue)
        .return();

      onDataChange(nextData);
    },
    [data, onDataChange]
  );

  const changeProjectDescription = React.useCallback(
    (nextValue: string) => {
      const nextData = createProjectConstructor(data)
        .changeDetails(nextValue)
        .return();

      onDataChange(nextData);
    },
    [data, onDataChange]
  );

  const hasNameRequiredHint = violations.some(
    (violation) => violation.name === "NAME_REQUIRED"
  );

  const hasNameTakenHint = violations.some(
    (violation) => violation.name === "NAME_TAKEN"
  );

  return (
    <GridLayoutComponent mainAxis="y">
      <LabelComponent>
        <GridLayoutComponent mainAxis="y">
          <TextComponent>{dictionary.nameLabelText}</TextComponent>
          <TextControlComponent
            placeholder={dictionary.namePlaceholderText}
            value={data.name}
            disabled={disabled}
            autoFocus
            onValueChange={changeProjectName}
          />
          {hasNameRequiredHint && (
            <TextComponent size="s" color="critical">
              {dictionary.nameRequiredHintText}
            </TextComponent>
          )}
          {hasNameTakenHint && (
            <TextComponent size="s" color="critical">
              {dictionary.nameTakenHintText}
            </TextComponent>
          )}
        </GridLayoutComponent>
      </LabelComponent>
      <LabelComponent>
        <GridLayoutComponent mainAxis="y">
          <TextComponent>{dictionary.detailsLabelText}</TextComponent>
          <TextControlComponent
            placeholder={dictionary.detailsPlaceholderText}
            value={data.details}
            disabled={disabled}
            multiline
            onValueChange={changeProjectDescription}
          />
        </GridLayoutComponent>
      </LabelComponent>
    </GridLayoutComponent>
  );
};
