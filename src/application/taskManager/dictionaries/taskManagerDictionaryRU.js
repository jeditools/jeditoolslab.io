// @flow strict

import type { TaskManagerDictionary } from "application/taskManager/taskManagerTypes";

export const taskManagerDictionaryRU: TaskManagerDictionary = {
  completeActionText: "Выполнить",
  deleteActionText: "Удалить",
  deleteConfirmText: "Вы уверены, что хотите удалить задачу?",
  editActionText: "Редактировать",
  completedText: "выполнена",
};
