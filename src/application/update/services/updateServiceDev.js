// @flow strict

import type {
  UpdateService,
  UpdateStatus,
} from "application/update/updateTypes";

let status: UpdateStatus = "DOWNLOADED";

const callbacks = new Set();

const recall = () => {
  callbacks.forEach((callback) => {
    callback(status);
  });
};

export const updateServiceDev: UpdateService = {
  async subscribe(callback) {
    callbacks.add(callback);

    callback(status);
  },
  async applyUpdate() {
    status = "UNKNOWN";

    recall();
  },
};
