// @flow strict

import type {
  Practice,
  PracticeConstructor,
} from "application/practice/practiceTypes";

export const createPractice = (): Practice => ({
  name: "",
  details: "",
  regularityInDays: 0,
  latestCompletionTimestamp: null,
});

export const createPracticeConstructor = (
  practice: Practice
): PracticeConstructor => ({
  get() {
    return practice;
  },
  setName(nextValue) {
    return createPracticeConstructor({
      ...practice,
      name: nextValue,
    });
  },
  setDetails(nextDescription) {
    return createPracticeConstructor({
      ...practice,
      details: nextDescription,
    });
  },
  setRegularityInDays(nextValue) {
    return createPracticeConstructor({
      ...practice,
      regularityInDays: nextValue,
    });
  },
  setLatestCompletionTimestamp(nextTimestamp) {
    return createPracticeConstructor({
      ...practice,
      latestCompletionTimestamp: nextTimestamp,
    });
  },
});
