// @flow strict

import type { UpdateStatus } from "application/update/updateTypes";

export const createUpdateStatus = (status: UpdateStatus): UpdateStatus =>
  status;
