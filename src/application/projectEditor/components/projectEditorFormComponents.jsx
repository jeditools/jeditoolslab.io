// @flow strict

import * as React from "react";

import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";

type Props = {|
  fieldset: React.Node,
  footer: React.Node,
|};

export const ProjectEditorFormComponent = (props: Props): React.Node => {
  const { fieldset, footer } = props;

  return (
    <GridLayoutComponent mainAxis="y">
      {fieldset}
      {footer}
    </GridLayoutComponent>
  );
};
