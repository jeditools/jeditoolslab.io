// @flow strict

export type FileService = {
  write(name: string, content: string): Promise<void>,
  browse(): Promise<FileList>,
  readAsText(file: File): Promise<string>,
};
