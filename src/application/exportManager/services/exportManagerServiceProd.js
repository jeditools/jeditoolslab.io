// @flow strict

import type { ExportManagerService } from "application/exportManager/exportManagerTypes";

import { persistedEventProdService } from "application/persistedEvent/persistedEventServices/persistedEventProdServices";
import { fileService } from "utility/file/fileServices";

export const exportManagerServiceProd: ExportManagerService = {
  async downloadData() {
    const eventEntries = await persistedEventProdService.getEventEntries();

    const fileData = JSON.stringify(eventEntries);

    await fileService.write(`jeditools_data_${Date.now()}.json`, fileData);
  },
};
