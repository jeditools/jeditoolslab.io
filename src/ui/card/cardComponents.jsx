// @flow strict

import * as React from "react";

import "./cardStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  children: React.Node,
  floating?: boolean,
|};

export const CardComponent = (props: Props): React.Node => {
  const { children, floating } = props;

  const className = createClassName("card")({ floating });

  return <div className={className}>{children}</div>;
};
