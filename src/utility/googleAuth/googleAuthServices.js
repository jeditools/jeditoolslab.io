// @flow strict

import type { GoogleAuthSignInErrorObject } from "utility/googleAuth/googleAuthTypes";

import {
  GoogleAuthSignInError,
  GoogleAuthError,
} from "utility/googleAuth/googleAuthUtils";
import { googleAPIService } from "utility/googleAPI/googleAPIServices";

const getAuthInstance = async (): Promise<any> => {
  const gapi = await googleAPIService.getAPI();

  const authInstance = gapi.auth2.getAuthInstance();

  if (authInstance) {
    return authInstance;
  } else {
    throw new GoogleAuthError("Failed to get Google API Auth Instance");
  }
};

export const googleAuthService = {
  async isSignedIn(): Promise<boolean> {
    // WARNING: do not use destructuring
    const authInstance = await getAuthInstance();

    return authInstance.isSignedIn.get();
  },

  async signIn(): Promise<void> {
    // WARNING: do not use destructuring
    const authInstance = await getAuthInstance();

    try {
      await authInstance.signIn();
    } catch (exception) {
      const { error: name }: GoogleAuthSignInErrorObject = exception;

      switch (name) {
        case "popup_closed_by_user":
        case "access_denied":
          throw new GoogleAuthSignInError("CANCELLED");
        default:
          throw new GoogleAuthSignInError("UNKNOWN");
      }
    }
  },

  async signOut(): Promise<void> {
    // WARNING: do not use destructuring
    const authInstance = await getAuthInstance();

    await authInstance.signOut();
  },

  async refreshAccessToken(): Promise<void> {
    // WARNING: do not use destructuring
    const authInstance = await getAuthInstance();

    await authInstance.currentUser.get().reloadAuthResponse();
  },
};
