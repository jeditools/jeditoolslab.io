// @flow strict

import * as React from "react";

type Props = {|
  children: React.Node,
  onSubmit(): void,
|};

export const FormControlComponent = (props: Props): React.Node => {
  const { children, onSubmit } = props;

  const handleSubmit = React.useCallback(
    (event: Event) => {
      event.preventDefault();

      onSubmit();
    },
    [onSubmit]
  );

  return <form onSubmit={handleSubmit}>{children}</form>;
};
