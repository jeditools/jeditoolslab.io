// @flow strict

import * as React from "react";

import "./animationStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type AnimationName = "endlessRotation";

type Props = {|
  name: AnimationName,
  children: React.Node,
|};

export const AnimationComponent = (props: Props): React.Node => {
  const { children, name } = props;

  const className = createClassName("animation")({ name });

  return <div className={className}>{children}</div>;
};
