// @flow strict

import type { DictionaryCache } from "@draftup/utility.js";

import type { Sync } from "application/sync/syncTypes";
import type { Activity } from "../activity/activityTypes";
import type { Practice } from "../practice/practiceTypes";
import type { UpdateStatus } from "../update/updateTypes";
import type { ThemeMode } from "../theme/themeTypes";
import type { Task } from "../task/taskTypes";
import type { IntlDictionary } from "../intl/intlTypes";
import type { Project } from "../project/projectTypes";

export type State = {|
  migrationComplete: boolean,
  visible: boolean,
  sync: null | Sync,
  online: boolean,
  updateStatus: UpdateStatus,
  themeMode: ThemeMode,
  currentActivity: Activity,
  intlDictionary: IntlDictionary,
  taskByIdCache: DictionaryCache<string, Task>,
  projectByIdCache: DictionaryCache<string, Project>,
  practiceByIdCache: DictionaryCache<string, Practice>,
|};
