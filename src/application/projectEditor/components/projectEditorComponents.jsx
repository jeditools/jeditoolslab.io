// @flow strict

import type { Project, ProjectId } from "../../project/projectTypes";
import type { ProjectEditorForm } from "../projectEditorTypes";

import * as React from "react";
import { useHistory } from "react-router-dom";

import { useEventDispatcher } from "../../event/eventHooks";
import { ProjectEditorFormComponent } from "./projectEditorFormComponents.jsx";
import { ProjectEditorFieldsetComponent } from "./projectEditorFieldsetComponents.jsx";
import { ProjectEditorFooterComponent } from "./projectEditorFooterComponents.jsx";
import { useServices } from "../../services/servicesHooks";
import { useEditedProjectId } from "../projectEditorHooks";
import { FormControlComponent } from "ui/formControl/formControlComponents";
import { checkProjectForViolations } from "../../projectForm/projectFormUtils";
import { createActivityRoute } from "../../activity/activityUtils";
import { ProjectWithNameAlreadyExistsError } from "../../project/projectConstants";
import {
  createForm,
  createFormConstructor,
} from "../../../utility/form/formUtils";
import { createProject } from "../../project/projectCreators";
import { useState } from "../../state/stateHooks";
import { useAsyncProcessController } from "../../../utility/asyncProcessController/asyncProcessControllerHooks";

export const ProjectEditorComponent = (): React.Node => {
  const { process, wrapAsync } = useAsyncProcessController();

  const editedProjectId = useEditedProjectId();

  const { projectEditorService } = useServices();

  const dispatch = useEventDispatcher();

  const [form, setForm] = React.useState<ProjectEditorForm>(
    createForm(createProject(Date.now()))
  );

  const { projectByIdCache } = useState();

  React.useLayoutEffect(() => {
    if (editedProjectId) {
      const project = projectByIdCache.readValue(editedProjectId);

      if (project) {
        setForm(createForm(project));
      }
    }
  }, [editedProjectId, projectByIdCache]);

  const history = useHistory();

  const validate = React.useCallback(() => {
    const validity = checkProjectForViolations(form.data);

    const valid = validity.length === 0;

    if (!valid) {
      setForm((prev) => {
        return createFormConstructor(prev).toRejected(validity).return();
      });
    }

    return valid;
  }, [form.data]);

  const handleDataChange = React.useCallback((nextData: Project) => {
    setForm((prev) => {
      return createFormConstructor(prev).changeData(nextData).return();
    });
  }, []);

  const handleSubmitSucceed = React.useCallback(
    (projectId: ProjectId) => {
      dispatch({
        name: "PROJECT_EDITOR__PROJECT_SUBMITTED",
        projectId,
      });

      history.replace(
        createActivityRoute({ name: "PROJECT_MANAGEMENT", projectId })
      );
    },
    [dispatch, history]
  );

  const submit = React.useCallback(() => {
    const valid = validate();

    if (valid) {
      const promise = projectEditorService
        .submit(editedProjectId, form.data)
        .then(handleSubmitSucceed)
        .catch((error) => {
          if (error instanceof ProjectWithNameAlreadyExistsError) {
            setForm((prev) => {
              return createFormConstructor(prev)
                .toRejected([
                  {
                    name: "NAME_TAKEN",
                  },
                ])
                .return();
            });
          } else {
            throw error;
          }
        });

      wrapAsync(promise);
    }
  }, [
    editedProjectId,
    form.data,
    handleSubmitSucceed,
    projectEditorService,
    validate,
    wrapAsync,
  ]);

  React.useEffect(() => {
    if (form.status === "REJECTED") {
      validate();
    }
  }, [form.status, validate]);

  const violations = form.status === "REJECTED" ? form.violations : [];

  const processIsPending = process.stage.name === "PENDING";

  return (
    <FormControlComponent onSubmit={submit}>
      <ProjectEditorFormComponent
        fieldset={
          <ProjectEditorFieldsetComponent
            disabled={processIsPending}
            data={form.data}
            violations={violations}
            onDataChange={handleDataChange}
          />
        }
        footer={<ProjectEditorFooterComponent disabled={processIsPending} />}
      />
    </FormControlComponent>
  );
};
