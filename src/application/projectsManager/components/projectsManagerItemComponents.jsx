// @flow strict

import type { ProjectId } from "../../project/projectTypes";

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { ArrowForwardIconComponent, AlarmOnIconComponent } from "ui/icon";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import { createActivityRoute } from "../../activity/activityUtils";
import { useProject } from "../../project/projectHooks/_useProject";
import { useProjectActiveTaskIds } from "../../project/projectHooks/_useProjectActiveTaskIds";
import { useIntlDictionary } from "../../intl/intlHooks";
import { InnerLinkControlComponent } from "../../../ui/innerLinkControl/innerLinkControlComponents.jsx";

type Props = {|
  projectId: ProjectId,
  latestCompletionTimestamp: null | number,
|};

export const ProjectsManagerItemComponent = (props: Props): React.Node => {
  const { projectId, latestCompletionTimestamp } = props;

  const { projectsManager: dictionary } = useIntlDictionary();

  const project = useProject(projectId);

  const activeTasks = useProjectActiveTaskIds(projectId);

  const active = activeTasks.length > 0;

  const projectManagementRoute = createActivityRoute({
    name: "PROJECT_MANAGEMENT",
    projectId,
  });

  return (
    <InnerLinkControlComponent to={projectManagementRoute}>
      <BoxComponent padding="vertical">
        <GridLayoutComponent mainAxis="x" primaryAlignment="apart">
          <TextComponent color={!active ? "secondary" : undefined}>
            {project.name}
          </TextComponent>
          <BoxComponent padding="horizontal">
            <TextComponent>
              <ArrowForwardIconComponent />
            </TextComponent>
          </BoxComponent>
        </GridLayoutComponent>
        {latestCompletionTimestamp !== null && (
          <TextComponent color="secondary" size="s">
            <InlineLayoutComponent>
              <AlarmOnIconComponent />{" "}
              {dictionary.latestCompletionText(latestCompletionTimestamp)}
            </InlineLayoutComponent>
          </TextComponent>
        )}
      </BoxComponent>
    </InnerLinkControlComponent>
  );
};
