// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { useSyncEnabled, useSyncInProgress } from "application/sync/syncHooks";
import { createActivityRoute } from "application/activity/activityUtils";
import { InlineLayoutComponent } from "ui/inlineLayout/inlineLayoutComponents";
import {
  useClosestPracticeToCompleteIds,
  useFarestPracticeToCompleteIds,
} from "application/practice/practiceHooks";
import { DelimiterComponent } from "ui/delimiter/delimiterComponents";
import { ActionControlComponent } from "ui/actionControl/actionControlComponents";
import { BoxComponent } from "ui/box/boxComponents";
import { ExpandLessIconComponent, ExpandMoreIconComponent } from "ui/icon";
import { useEventDispatcher } from "../../event/eventHooks";
import { PracticesManagerItemComponent } from "./practicesManagerItemComponents.jsx";
import { useServices } from "../../services/servicesHooks";
import { useIntlDictionary } from "../../intl/intlHooks";
import { InnerLinkControlComponent } from "../../../ui/innerLinkControl/innerLinkControlComponents.jsx";
import { useAsyncProcessController } from "../../../utility/asyncProcessController/asyncProcessControllerHooks";

export const PracticesManagerComponent = (): React.Node => {
  const dispatch = useEventDispatcher();

  const { wrapAsync } = useAsyncProcessController();

  const [shouldMountFarestPractices, setShouldMountFarestPractices] =
    React.useState(false);

  const hideFarestPractices = React.useCallback(() => {
    setShouldMountFarestPractices(false);
  }, []);

  const showFarestPractices = React.useCallback(() => {
    setShouldMountFarestPractices(true);
  }, []);

  const { repositoryService } = useServices();

  const syncEnabled = useSyncEnabled();

  const syncInProgress = useSyncInProgress();

  React.useLayoutEffect(() => {
    const promise = repositoryService
      .getData({ allPractices: true })
      .then((data) => {
        dispatch({
          name: "PRACTICES_MANAGER__DATA_RECIEVED",
          data,
        });
      });

    wrapAsync(promise);

    // WARNING: implicit syncInProgress dependency
  }, [dispatch, wrapAsync, repositoryService, syncEnabled, syncInProgress]);

  const { practicesManager: intl } = useIntlDictionary();

  const closestPracticeIds = useClosestPracticeToCompleteIds();

  const farestPracticeIds = useFarestPracticeToCompleteIds();

  const hasNoPracticesMessage =
    closestPracticeIds.length === 0 && farestPracticeIds.length === 0;

  const hasNoClosestPracticesMessage =
    closestPracticeIds.length === 0 && farestPracticeIds.length > 0;

  return (
    <GridLayoutComponent mainAxis="y">
      <InlineLayoutComponent>
        <InnerLinkControlComponent
          to={createActivityRoute({ name: "PRACTICE_CREATION" })}
        >
          <TextComponent color="accent">
            {intl.newPracticeLinkText}
          </TextComponent>
        </InnerLinkControlComponent>
      </InlineLayoutComponent>
      {hasNoPracticesMessage && (
        <GridLayoutComponent mainAxis="y">
          <TextComponent color="secondary">
            {intl.noPracticesText}
          </TextComponent>
        </GridLayoutComponent>
      )}
      {hasNoClosestPracticesMessage && (
        <GridLayoutComponent mainAxis="y">
          <TextComponent color="secondary">
            {intl.noClosestPracticesText}
          </TextComponent>
        </GridLayoutComponent>
      )}
      {closestPracticeIds.length > 0 && (
        <GridLayoutComponent mainAxis="y">
          {closestPracticeIds.map((practiceId) => (
            <PracticesManagerItemComponent
              key={practiceId}
              practiceId={practiceId}
            />
          ))}
        </GridLayoutComponent>
      )}
      {farestPracticeIds.length > 0 && <DelimiterComponent />}
      {farestPracticeIds.length > 0 && shouldMountFarestPractices && (
        <ActionControlComponent type="button" onClick={hideFarestPractices}>
          <BoxComponent>
            <GridLayoutComponent mainAxis="y" primaryAlignment="center">
              <TextComponent>
                {intl.hideFarestPracticesActionText}
              </TextComponent>
              <TextComponent>
                <ExpandLessIconComponent />
              </TextComponent>
            </GridLayoutComponent>
          </BoxComponent>
        </ActionControlComponent>
      )}
      {farestPracticeIds.length > 0 && !shouldMountFarestPractices && (
        <ActionControlComponent type="button" onClick={showFarestPractices}>
          <BoxComponent>
            <GridLayoutComponent mainAxis="y" primaryAlignment="center">
              <TextComponent>
                {intl.showFarestPracticesActionText}
              </TextComponent>
              <TextComponent>
                <ExpandMoreIconComponent />
              </TextComponent>
            </GridLayoutComponent>
          </BoxComponent>
        </ActionControlComponent>
      )}
      {farestPracticeIds.length > 0 && shouldMountFarestPractices && (
        <GridLayoutComponent mainAxis="y">
          {farestPracticeIds.map((practiceId) => (
            <PracticesManagerItemComponent
              key={practiceId}
              practiceId={practiceId}
            />
          ))}
        </GridLayoutComponent>
      )}
    </GridLayoutComponent>
  );
};
