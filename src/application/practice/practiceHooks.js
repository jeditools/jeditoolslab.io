// @flow strict

import type { PracticeId } from "./practiceTypes";
import type { Practice } from "./practiceTypes";

import React from "react";

import { createEntriesLense } from "utility/entry/entryUtils";
import { createPractice } from "./practiceCreators";
import { createPracticeLense } from "./practiceLenses";
import { useState } from "../state/stateHooks";

export const usePractice = (practiceId: PracticeId): Practice => {
  const { practiceByIdCache } = useState();

  return React.useMemo(() => {
    const practice = createEntriesLense(
      practiceByIdCache.readEntries()
    ).selectItem(practiceId);

    return practice || createPractice();
  }, [practiceByIdCache, practiceId]);
};
const sortPracticeEntries = ([, practice1], [, practice2]) => {
  const nextCompletionDate1 =
    createPracticeLense(practice1).selectNextCompletionDate();

  const nextCompletionDate2 =
    createPracticeLense(practice2).selectNextCompletionDate();

  if (nextCompletionDate1 === null) {
    return -1;
  }

  if (nextCompletionDate2 === null) {
    return 1;
  }

  return nextCompletionDate1 - nextCompletionDate2;
};

export const useClosestPracticeToCompleteIds = (): PracticeId[] => {
  const { practiceByIdCache } = useState();

  return React.useMemo(() => {
    const sortedPracticeEntries = practiceByIdCache
      .readEntries()
      .filter(([, practice]) => {
        const lense = createPracticeLense(practice);

        if (!lense.selectNextCompletionDate()) {
          return true;
        }

        return (
          lense.shouldBeAlreadyCompleted() || lense.shouldBeCompletedToday()
        );
      })
      .sort(sortPracticeEntries);

    return createEntriesLense(sortedPracticeEntries).selectAllIds();
  }, [practiceByIdCache]);
};

export const useFarestPracticeToCompleteIds = (): PracticeId[] => {
  const { practiceByIdCache } = useState();

  return React.useMemo(() => {
    const sortedPracticeEntries = practiceByIdCache
      .readEntries()
      .filter(([, practice]) => {
        const lense = createPracticeLense(practice);

        if (!lense.selectNextCompletionDate()) {
          return false;
        }

        return lense.shouldNotYetBeCompleted();
      })
      .sort(sortPracticeEntries);

    return createEntriesLense(sortedPracticeEntries).selectAllIds();
  }, [practiceByIdCache]);
};
