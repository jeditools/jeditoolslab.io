// @flow strict

type ConnectionStatusChanged = {|
  name: "CONNECTION__STATUS_CHANGED",
  online: boolean,
|};

export type ConnectionEvent = ConnectionStatusChanged;
