// @flow strict

import type { Event } from "../../event/eventTypes";
import type { UpdateStatus } from "../../update/updateTypes";

export const updateStatusReducer = (
  state: UpdateStatus,
  event: Event
): UpdateStatus => {
  switch (event.name) {
    case "ROOT__UPDATE_STATUS_RECIEVED":
      return event.updateStatus;
    default:
      return state;
  }
};
