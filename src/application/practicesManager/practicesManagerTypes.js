// @flow strict

import type { RepositoryData } from "../repository/repositoryTypes";

export type PracticesManagerDictionary = {|
  noPracticesText: string,
  noClosestPracticesText: string,
  newPracticeLinkText: string,
  hideFarestPracticesActionText: string,
  showFarestPracticesActionText: string,
  nextCompletionText(timestamp: number): string,
  initialCompletionText: string,
|};

type DataRecieved = {|
  name: "PRACTICES_MANAGER__DATA_RECIEVED",
  data: RepositoryData,
|};

export type PracticesManagerEvent = DataRecieved;
