// @flow strict

export type ActivityControlsDictionary = {|
  projectsManagementScreenTitle: string,
  projectManagementScreenTitle: string,
  projectCreationScreenTitle: string,
  taskManagementScreenTitle: string,
  taskEditingScreenTitle: string,
  taskCreationScreenTitle: string,
  practicesManagementScreenTitle: string,
  practiceManagementScreenTitle: string,
  practiceCreationScreenTitle: string,
  navigationScreenTitle: string,
  settingsManagementScreenTitle: string,
|};
