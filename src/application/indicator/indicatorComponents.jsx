// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { TooltipComponent } from "ui/tooltip/tooltipComponents";
import { AnimationComponent } from "ui/animation/animationComponents";
import {
  NewReleasesIconComponent,
  CloudOffIconComponent,
  CachedIconComponent,
  MobileDataOffIconComponent,
  WarningAmberIconComponent,
} from "ui/icon";
import { useSyncDisabled, useSyncInProgress } from "application/sync/syncHooks";
import { useState } from "../state/stateHooks";
import { useIntlDictionary } from "../intl/intlHooks";
import { createUpdateStatus } from "../update/updateUtils";
import { useSyncFailed } from "../sync/syncHooks";

export const IndicatorComponent = (): React.Node => {
  const { updateStatus, online } = useState();

  const { indicator: ownDictionary } = useIntlDictionary();

  const syncInProgress = useSyncInProgress();

  const syncDisabled = useSyncDisabled();

  const syncFailed = useSyncFailed();

  if (!online) {
    return (
      <TextComponent>
        <TooltipComponent text={ownDictionary.offlineTooltipText}>
          <MobileDataOffIconComponent />
        </TooltipComponent>
      </TextComponent>
    );
  }

  if (syncInProgress) {
    return (
      <TextComponent>
        <TooltipComponent text={ownDictionary.syncInProcessTooltipText}>
          <AnimationComponent name="endlessRotation">
            <CachedIconComponent />
          </AnimationComponent>
        </TooltipComponent>
      </TextComponent>
    );
  }

  if (updateStatus === createUpdateStatus("DOWNLOADED")) {
    return (
      <TextComponent>
        <TooltipComponent text={ownDictionary.updateAvailableTooltipText}>
          <NewReleasesIconComponent />
        </TooltipComponent>
      </TextComponent>
    );
  }

  if (syncFailed) {
    return (
      <TextComponent>
        <TooltipComponent text={ownDictionary.syncFailedTooltipText}>
          <WarningAmberIconComponent />
        </TooltipComponent>
      </TextComponent>
    );
  }

  if (syncDisabled) {
    return (
      <TextComponent>
        <TooltipComponent text={ownDictionary.syncDisabledTooltipText}>
          <CloudOffIconComponent />
        </TooltipComponent>
      </TextComponent>
    );
  }

  return null;
};
