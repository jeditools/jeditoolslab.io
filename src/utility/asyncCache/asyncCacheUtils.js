// @flow strict

import type { AsyncCache } from "utility/asyncCache/asyncCacheTypes";

export function createAsyncCache<ValueType>(
  method: () => Promise<ValueType>
): AsyncCache<ValueType> {
  let cachedPromise = null;

  const asyncCache = {
    read() {
      if (cachedPromise) {
        return cachedPromise;
      }

      cachedPromise = method();

      return asyncCache.read();
    },
    invalidate() {
      cachedPromise = null;
    },
  };

  return asyncCache;
}
