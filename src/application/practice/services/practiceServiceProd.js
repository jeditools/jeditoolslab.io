// @flow strict

import type { PracticeService } from "application/practice/practiceTypes";

import { createPracticeService } from "application/practice/practiceServiceCreators";
import { persistedEventProdService } from "application/persistedEvent/persistedEventServices/persistedEventProdServices";

export const practiceServiceProd: PracticeService = createPracticeService(
  persistedEventProdService
);
