// @flow strict

import * as React from "react";

import type { State } from "application/state/stateTypes";

import { createState } from "application/state/stateUtils";

export const StateContext: React.Context<State> = React.createContext(
  createState()
);
