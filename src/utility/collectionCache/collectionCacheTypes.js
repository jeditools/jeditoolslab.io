// @flow strict

export type CollectionCache<IdType, ValueType> = {|
  get(id: IdType): ValueType | Promise<void> | Error,
  invalidate(id: IdType): void,
  listen(listener: (id: IdType) => void): () => void,
|};
