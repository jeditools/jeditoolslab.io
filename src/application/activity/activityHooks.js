// @flow strict

import type { Activity } from "application/activity/activityTypes";

import React from "react";
import { useRouteMatch } from "react-router-dom";

import { useState } from "../state/stateHooks";
import {
  createActivityRoutePattern,
  createActivityFromMatch,
} from "application/activity/activityUtils";

export const useCurrentActivity = (): Activity => {
  const { currentActivity } = useState();

  return currentActivity;
};

export const useNextActivity = (): null | Activity => {
  const prevMatchRef = React.useRef(null);
  const prevActivityRef = React.useRef(null);

  const match = useRouteMatch([
    createActivityRoutePattern("TASK_CREATION"), // Must go before TASK_MANAGEMENT
    createActivityRoutePattern("TASK_EDITING"), // Must go before TASK_MANAGEMENT
    createActivityRoutePattern("TASK_MANAGEMENT"),
    createActivityRoutePattern("PROJECTS_MANAGEMENT"),
    createActivityRoutePattern("PROJECT_CREATION"), // Must go before PROJECT_MANAGEMENT
    createActivityRoutePattern("PROJECT_EDITING"), // Must go before PROJECT_MANAGEMENT
    createActivityRoutePattern("PROJECT_MANAGEMENT"),
    createActivityRoutePattern("PRACTICES_MANAGEMENT"),
    createActivityRoutePattern("PRACTICE_CREATION"), // Must go before PRACTICE_MANAGEMENT
    createActivityRoutePattern("PRACTICE_EDITING"), // Must go before PRACTICE_MANAGEMENT
    createActivityRoutePattern("PRACTICE_MANAGEMENT"),
    createActivityRoutePattern("NAVIGATION"),
    createActivityRoutePattern("SETTINGS_MANAGEMENT"),
  ]);

  const nextActivity = React.useMemo(() => {
    if (match) {
      const { current: prevMatch } = prevMatchRef;

      if (prevMatch && prevMatch.url === match.url) {
        return prevActivityRef.current;
      }

      return createActivityFromMatch(match);
    }

    return null;
  }, [match]);

  prevMatchRef.current = match;
  prevActivityRef.current = nextActivity;

  return nextActivity;
};
