// @flow strict

import type { Task, TaskId } from "application/task/taskTypes";

export type TasksManagerDictionary = {|
  noActiveTasksText: string,
  createTaskText: string,
  completedText: string,
|};

export type TaskManagerInitialData = {|
  activeIds: TaskId[],
  completedIds: TaskId[],
  taskEntries: [TaskId, Task][],
|};
