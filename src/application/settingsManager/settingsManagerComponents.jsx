// @flow strict

import * as React from "react";

import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { DelimiterComponent } from "ui/delimiter/delimiterComponents";
import { useState } from "../state/stateHooks";
import { AppearanceManagerComponent } from "../appearanceManager/appearanceManagerComponents.jsx";
import { ExportManagerComponent } from "../exportManager/exportManagerComponents.jsx";
import { ImportManagerComponent } from "../importManager/importManagerComponents.jsx";
import { createUpdateStatus } from "../update/updateUtils";
import { UpdateManagerComponent } from "../updateManager/updateManagerComponents.jsx";
import { SyncManagerComponent } from "../syncManager/syncManagerComponents.jsx";

export const SettingsManagerComponent = (): React.Node => {
  const { updateStatus, online } = useState();

  const updateDownloaded = updateStatus === createUpdateStatus("DOWNLOADED");

  return (
    <GridLayoutComponent mainAxis="y">
      {updateDownloaded && (
        <>
          <UpdateManagerComponent />
          <DelimiterComponent />
        </>
      )}
      <AppearanceManagerComponent />
      <DelimiterComponent />
      {online && (
        <>
          <SyncManagerComponent />
          <DelimiterComponent />
        </>
      )}
      <ExportManagerComponent />
      <DelimiterComponent />
      <ImportManagerComponent />
    </GridLayoutComponent>
  );
};
