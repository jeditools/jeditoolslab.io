// @flow strict

import { formatDistanceToNow, isTomorrow, isYesterday } from "date-fns";
import { ru } from "date-fns/locale";

import type { PracticesManagerDictionary } from "application/practicesManager/practicesManagerTypes";

export const practicesManagerDictionaryRU: PracticesManagerDictionary = {
  noPracticesText: "Нет практик",
  noClosestPracticesText: "Все актуальные практики выполнены",
  newPracticeLinkText: "Новая практика",
  hideFarestPracticesActionText: "скрыть неактуальные",
  showFarestPracticesActionText: "показать неактуальные",
  nextCompletionText(timestamp) {
    const date = new Date(timestamp);

    if (isYesterday(date)) {
      return "вчера";
    }

    if (isTomorrow(date)) {
      return "завтра";
    }

    return formatDistanceToNow(new Date(timestamp), {
      locale: ru,
      addSuffix: true,
    });
  },
  initialCompletionText: "не выполнялась",
};
