// @flow strict

export type ErrorMessageDictionary = {|
  generalErrorMessage: string,
  taskNotFoundErrorMessage: string,
  projectNotFoundErrorMessage: string,
  practiceNotFoundErrorMessage: string,
|};
