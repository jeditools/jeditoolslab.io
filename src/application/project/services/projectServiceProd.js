// @flow strict

import type { ProjectService } from "../projectTypes";

import { createProjectService } from "../projectServiceCreators";
import { persistedEventProdService } from "application/persistedEvent/persistedEventServices/persistedEventProdServices";

export const projectServiceProd: ProjectService = createProjectService(
  persistedEventProdService
);
