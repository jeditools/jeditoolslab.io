// @flow strict

import type { IndicatorDictionary } from "application/indicator/indicatorTypes";

export const indicatorDictionaryRu: IndicatorDictionary = {
  updateAvailableTooltipText: "доступно обновление",
  syncFailedTooltipText: "синхронизация не удалась",
  syncDisabledTooltipText: "синхронизация отключена",
  syncInProcessTooltipText: "идёт синхронизация данных",
  offlineTooltipText: "нет соединения",
};
