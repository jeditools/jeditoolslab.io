// @flow strict

import type {
  PersistedEventEntries,
  PersistedEvent,
} from "application/persistedEvent/persistedEventTypes";

import { createEntriesConstructor } from "utility/entry/entryUtils";
import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createPersistedEventEntriesPracticeLense } from "application/persistedEvent/persistedEventUtils/persistedEventPracticeLenses";

// Project 1

const project1Id = createUniqueId();

const project1Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: 1,
  projectId: project1Id,
  project: {
    name: "Project1",
    details: "",
  },
};

const project1Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: 2,
  projectId: project1Id,
  project: {
    name: "Project1 Updated",
    details: "Project1 Details",
  },
};

const project1Deleted: PersistedEvent = {
  name: "ProjectDeleted",
  timestamp: 3,
  projectId: project1Id,
};

// Project 2

const project2Id = createUniqueId();

const project2Created: PersistedEvent = {
  name: "ProjectCreated",
  timestamp: 4,
  projectId: project2Id,
  project: {
    name: "Project2",
    details: "",
  },
};

const project2Updated: PersistedEvent = {
  name: "ProjectUpdated",
  timestamp: 5,
  projectId: project2Id,
  project: {
    name: "Project2 Updated",
    details: "Project2 Details",
  },
};

const project2Deleted: PersistedEvent = {
  name: "ProjectDeleted",
  timestamp: 6,
  projectId: project2Id,
};

// Practice 1

const practice1Id = createUniqueId();

const practice1Created: PersistedEvent = {
  name: "PracticeCreated",
  timestamp: 7,
  practiceId: practice1Id,
  practice: {
    name: "Practice1",
    details: "",
    regularityInDays: 1,
  },
};

const practice1Updated: PersistedEvent = {
  name: "PracticeUpdated",
  timestamp: 8,
  practiceId: practice1Id,
  practice: {
    name: "Practice1 Updated",
    details: "Practice1 Details",
    regularityInDays: 2,
  },
};

const practice1Completed: PersistedEvent = {
  name: "PracticeCompleted",
  timestamp: 9,
  practiceId: practice1Id,
};

const practice1CompletedOnceAgain: PersistedEvent = {
  name: "PracticeCompleted",
  timestamp: 10,
  practiceId: practice1Id,
};

const practice1Deleted: PersistedEvent = {
  name: "PracticeDeleted",
  timestamp: 11,
  practiceId: practice1Id,
};

// Practice 2

const practice2Id = createUniqueId();

const practice2Created: PersistedEvent = {
  name: "PracticeCreated",
  timestamp: 12,
  practiceId: practice2Id,
  practice: {
    name: "Practice2",
    details: "",
    regularityInDays: 4,
  },
};

const practice2Updated: PersistedEvent = {
  name: "PracticeUpdated",
  timestamp: 13,
  practiceId: practice2Id,
  practice: {
    name: "Practice2 Updated",
    details: "Practice2 Details",
    regularityInDays: 4,
  },
};

const practice2UpdatedOnceAgain: PersistedEvent = {
  name: "PracticeUpdated",
  timestamp: 14,
  practiceId: practice2Id,
  practice: {
    name: "Practice2 Updated",
    details: "Practice2 Details",
    regularityInDays: 7,
  },
};

const practice2Completed: PersistedEvent = {
  name: "PracticeCompleted",
  timestamp: 15,
  practiceId: practice2Id,
};

const practice2Deleted: PersistedEvent = {
  name: "PracticeDeleted",
  timestamp: 16,
  practiceId: practice2Id,
};

describe("selectPracticeRelatedEventEntries", () => {
  test("should select all project related entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), project2Deleted)
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice1Updated)
        .setEntry(createUniqueId(), practice1Deleted)
        .return();

    const practiceRelatedEventEntries =
      createPersistedEventEntriesPracticeLense(
        persistedEventEntries
      ).selectPracticeRelatedEventEntries();

    expect(practiceRelatedEventEntries.map(([id, event]) => event)).toEqual([
      practice1Created,
      practice1Updated,
      practice1Deleted,
    ]);
  });
  test("should select project 2 related entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), project1Created)
        .setEntry(createUniqueId(), project1Updated)
        .setEntry(createUniqueId(), project1Deleted)
        .setEntry(createUniqueId(), project2Created)
        .setEntry(createUniqueId(), project2Updated)
        .setEntry(createUniqueId(), project2Deleted)
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice1Updated)
        .setEntry(createUniqueId(), practice1Deleted)
        .setEntry(createUniqueId(), practice2Created)
        .setEntry(createUniqueId(), practice2Updated)
        .setEntry(createUniqueId(), practice2Deleted)
        .return();

    const practiceRelatedEventEntries =
      createPersistedEventEntriesPracticeLense(
        persistedEventEntries
      ).selectPracticeRelatedEventEntries(practice2Id);

    expect(practiceRelatedEventEntries.map(([id, event]) => event)).toEqual([
      practice2Created,
      practice2Updated,
      practice2Deleted,
    ]);
  });
});

describe("selectPractice", () => {
  test("should select created practice", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice2Created)
        .return();

    const practice = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPractice(practice1Id);

    expect(practice).toEqual({
      name: "Practice1",
      details: "",
      latestCompletionTimestamp: null,
      regularityInDays: 1,
    });
  });

  test("should select updated practice", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice2Created)
        .setEntry(createUniqueId(), practice1Updated)
        .return();

    const practice = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPractice(practice1Id);

    expect(practice).toEqual({
      name: "Practice1 Updated",
      details: "Practice1 Details",
      latestCompletionTimestamp: null,
      regularityInDays: 2,
    });
  });

  test("should select updated practice with latest updates", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice2Created)
        .setEntry(createUniqueId(), practice2Updated)
        .setEntry(createUniqueId(), practice2UpdatedOnceAgain)
        .return();

    const practice = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPractice(practice2Id);

    expect(practice).toEqual({
      name: "Practice2 Updated",
      details: "Practice2 Details",
      latestCompletionTimestamp: null,
      regularityInDays: 7,
    });
  });

  test("should select completed practice", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice2Created)
        .setEntry(createUniqueId(), practice1Updated)
        .setEntry(createUniqueId(), practice2Completed)
        .return();

    const practice = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPractice(practice2Id);

    expect(practice).toEqual({
      name: "Practice2",
      details: "",
      latestCompletionTimestamp: practice2Completed.timestamp,
      regularityInDays: 4,
    });
  });

  test("should select completed practice with latest updates", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice2Created)
        .setEntry(createUniqueId(), practice1Updated)
        .setEntry(createUniqueId(), practice1Completed)
        .setEntry(createUniqueId(), practice1CompletedOnceAgain)
        .return();

    const practice = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPractice(practice1Id);

    expect(practice).toEqual({
      name: "Practice1 Updated",
      details: "Practice1 Details",
      latestCompletionTimestamp: practice1CompletedOnceAgain.timestamp,
      regularityInDays: 2,
    });
  });

  test("should not select deleted practice", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice1Updated)
        .setEntry(createUniqueId(), practice1Deleted)
        // Artifact events
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice1Updated)
        .return();

    const practice = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPractice(practice1Id);

    expect(practice).toEqual(null);
  });
});

describe("selectPracticeEntries", () => {
  test("should select all practice entries", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice2Created)
        .setEntry(createUniqueId(), practice1Updated)
        .return();

    const practiceEntries = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPracticeEntries();

    expect(practiceEntries.map(([id, practice]) => practice)).toEqual([
      {
        name: "Practice1 Updated",
        details: "Practice1 Details",
        latestCompletionTimestamp: null,
        regularityInDays: 2,
      },
      {
        name: "Practice2",
        details: "",
        latestCompletionTimestamp: null,
        regularityInDays: 4,
      },
    ]);
  });
  test("should select all practice entries except deleted ones", () => {
    const persistedEventEntries: PersistedEventEntries =
      createEntriesConstructor([])
        .setEntry(createUniqueId(), practice1Created)
        .setEntry(createUniqueId(), practice2Created)
        .setEntry(createUniqueId(), practice1Updated)
        .setEntry(createUniqueId(), practice1Deleted)
        // Artifact events
        .setEntry(createUniqueId(), practice1Created)
        .return();

    const practiceEntries = createPersistedEventEntriesPracticeLense(
      persistedEventEntries
    ).selectPracticeEntries();

    expect(practiceEntries.map(([id, practice]) => practice)).toEqual([
      {
        name: "Practice2",
        details: "",
        latestCompletionTimestamp: null,
        regularityInDays: 4,
      },
    ]);
  });
});
