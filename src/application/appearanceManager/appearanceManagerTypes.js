// @flow strict

import type { ThemeMode } from "../theme/themeTypes";

type ThemeChanged = {|
  name: "APPEARANCE_MANAGER__THEME_CHANGED",
  nextTheme: ThemeMode,
|};

export type AppearanceManagerEvent = ThemeChanged;

export type AppearanceManagerDictionary = {|
  themeLabelText: string,
  selectOptionAutoLabelText: string,
  selectOptionLightLabelText: string,
  selectOptionDarkLabelText: string,
|};
