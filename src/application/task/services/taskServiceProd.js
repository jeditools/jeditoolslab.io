// @flow strict

import type { TaskService } from "../taskTypes";

import { createTaskService } from "../taskServiceCreators";
import { persistedEventProdService } from "application/persistedEvent/persistedEventServices/persistedEventProdServices";

export const taskServiceProd: TaskService = createTaskService(
  persistedEventProdService
);
