// @flow strict

import type { PracticeId } from "../../practice/practiceTypes";
import type { PracticeEditorForm } from "../practiceEditorTypes";

import * as React from "react";

import { useEventDispatcher } from "../../event/eventHooks";
import { PracticeEditorFieldsetComponent } from "./practiceEditorFieldsetComponents.jsx";
import { PracticeEditorFooterComponent } from "./practiceEditorFooterComponents.jsx";
import { useServices } from "../../services/servicesHooks";
import { useEditedPracticeId } from "../practiceEditorHooks";
import { createActivityRoute } from "../../activity/activityUtils";
import { useHistory } from "react-router-dom";
import { PracticeWithNameAlreadyExistsError } from "../../practice/practiceErrors";
import { checkPracticeForViolations } from "../../practiceForm/practiceFormUtils";
import { FormControlComponent } from "../../../ui/formControl/formControlComponents.jsx";
import { PracticeEditorLayoutComponent } from "./practiceEditorLayoutComponents.jsx";
import {
  createForm,
  createFormConstructor,
} from "../../../utility/form/formUtils";
import { createPractice } from "../../practice/practiceCreators";
import { useState } from "../../state/stateHooks";
import { useAsyncProcessController } from "../../../utility/asyncProcessController/asyncProcessControllerHooks";

export const PracticeEditorComponent = (): React.Node => {
  const { process, wrapAsync } = useAsyncProcessController();

  const editedPracticeId = useEditedPracticeId();

  const { practiceEditorService } = useServices();

  const dispatch = useEventDispatcher();

  const { practiceByIdCache } = useState();

  const [form, setForm] = React.useState<PracticeEditorForm>(
    createForm(createPractice())
  );

  const history = useHistory();

  React.useLayoutEffect(() => {
    if (editedPracticeId) {
      const practice = practiceByIdCache.readValue(editedPracticeId);

      if (practice) {
        setForm(createForm(practice));
      }
    }
  }, [editedPracticeId, practiceByIdCache]);

  const validate = React.useCallback(() => {
    const validity = checkPracticeForViolations(form.data);

    const valid = validity.length === 0;

    if (!valid) {
      setForm((prev) => {
        return createFormConstructor(prev).toRejected(validity).return();
      });
    }

    return valid;
  }, [form.data]);

  const handleSubmitSucceed = React.useCallback(
    (practiceId: PracticeId) => {
      dispatch({
        name: "PRACTICE_EDITOR__PRACTICE_SUBMITTED",
        practiceId,
      });

      history.replace(
        createActivityRoute({ name: "PRACTICE_MANAGEMENT", practiceId })
      );
    },
    [dispatch, history]
  );

  const handleSubmitFailed = React.useCallback((error) => {
    if (error instanceof PracticeWithNameAlreadyExistsError) {
      setForm((prev) => {
        return createFormConstructor(prev)
          .toRejected([
            {
              name: "NAME_TAKEN",
            },
          ])
          .return();
      });
    } else {
      throw error;
    }
  }, []);

  const handleDataChange = React.useCallback((nextData) => {
    setForm((prev) => {
      return createFormConstructor(prev).changeData(nextData).return();
    });
  }, []);

  const submit = React.useCallback(() => {
    const valid = validate();

    if (valid) {
      const promise = practiceEditorService
        .submit(editedPracticeId, form.data)
        .then(handleSubmitSucceed)
        .catch(handleSubmitFailed);

      wrapAsync(promise);
    }
  }, [
    editedPracticeId,
    form.data,
    handleSubmitFailed,
    handleSubmitSucceed,
    practiceEditorService,
    validate,
    wrapAsync,
  ]);

  React.useEffect(() => {
    if (form.status === "REJECTED") {
      validate();
    }
  }, [form.status, validate]);

  const violations = form.status === "REJECTED" ? form.violations : [];

  const processIsPending = process.stage.name === "PENDING";

  return (
    <FormControlComponent onSubmit={submit}>
      <PracticeEditorLayoutComponent
        fieldset={
          <PracticeEditorFieldsetComponent
            disabled={processIsPending}
            data={form.data}
            violations={violations}
            onDataChanged={handleDataChange}
          />
        }
        footer={<PracticeEditorFooterComponent disabled={processIsPending} />}
      />
    </FormControlComponent>
  );
};
