// @flow strict

import { createAsyncCache } from "utility/asyncCache/asyncCacheUtils";

const createAsyncResourceMock = () => {
  let value = 0;

  return () => {
    return Promise.resolve(++value);
  };
};

describe("createAsyncCache", () => {
  test("should return correct value on a single read", () => {
    const cache = createAsyncCache(createAsyncResourceMock());

    return cache.read().then((value) => expect(value).toEqual(1));
  });
  test("should return correct value on a second read", () => {
    const cache = createAsyncCache(createAsyncResourceMock());

    return cache
      .read()
      .then(() => cache.read())
      .then((value) => expect(value).toEqual(1));
  });
  test("should return correct value on a second read after invalidation", () => {
    const cache = createAsyncCache(createAsyncResourceMock());

    return cache
      .read()
      .then(() => cache.invalidate())
      .then(() => cache.read())
      .then((value) => expect(value).toEqual(2));
  });
  test("should return correct values for concurrent reads", () => {
    const cache = createAsyncCache(createAsyncResourceMock());

    return Promise.all([cache.read(), cache.read()]).then((value) =>
      expect(value).toEqual([1, 1])
    );
  });
});
