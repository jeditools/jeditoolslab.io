// @flow strict

import type { TasksManagerDictionary } from "application/tasksManager/tasksManagerTypes";

export const tasksManagerDictionaryRU: TasksManagerDictionary = {
  noActiveTasksText: "Нет активных задач",
  createTaskText: "Создать задачу",
  completedText: "выполнена",
};
