// @flow strict

type PracticeFormViolation__NameRequired = {
  name: "NAME_REQUIRED",
};

type PracticeFormViolation__NameTaken = {
  name: "NAME_TAKEN",
};

type PracticeFormViolation__RegularityInDaysRequired = {
  name: "REGULARITY_IN_DAYS_REQUIRED",
};

export type PracticeFormViolation =
  | PracticeFormViolation__RegularityInDaysRequired
  | PracticeFormViolation__NameTaken
  | PracticeFormViolation__NameRequired;
