// @flow strict

import * as React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { DelimiterComponent } from "ui/delimiter/delimiterComponents";
import { ActivityControlsComponent } from "application/activityControls/activityControlsComponents";
import { ActivityContentComponent } from "application/activityContent/activityContentComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { useRootProcess } from "application/root/rootHooks";
import {
  createActivityRoute,
  createActivityRoutePattern,
} from "../../activity/activityUtils";
import { useState } from "../../state/stateHooks";

export const RootScreenComponent = (): React.Node => {
  const { migrationComplete } = useState();

  useRootProcess();

  const routePatterns = [
    createActivityRoutePattern("PROJECTS_MANAGEMENT"),
    createActivityRoutePattern("PROJECT_MANAGEMENT"),
    createActivityRoutePattern("PRACTICES_MANAGEMENT"),
    createActivityRoutePattern("PRACTICE_MANAGEMENT"),
    createActivityRoutePattern("SETTINGS_MANAGEMENT"),
    createActivityRoutePattern("NAVIGATION"),
  ];

  if (!migrationComplete) {
    return null;
  }

  return (
    <Switch>
      <Route path={routePatterns}>
        <GridLayoutComponent mainAxis="y">
          <ActivityControlsComponent />
          <DelimiterComponent />
          <ActivityContentComponent />
        </GridLayoutComponent>
      </Route>
      <Route path="/">
        <Redirect to={createActivityRoute({ name: "NAVIGATION" })} />
      </Route>
    </Switch>
  );
};
