// @flow strict

import type { SyncService } from "application/sync/syncTypes";

import { devServicesAsyncTimeoutNormal } from "../../services/servicesUtils";
import { createSyncDisabled } from "../syncUtils";

let sync = createSyncDisabled();

export const syncServiceDev: SyncService = {
  async read() {
    await devServicesAsyncTimeoutNormal.start();

    return sync;
  },
  async update(nextValue) {
    await devServicesAsyncTimeoutNormal.start();

    sync = nextValue;
  },
};
