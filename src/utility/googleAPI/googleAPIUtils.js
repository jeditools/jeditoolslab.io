// @flow strict

class GoogleAPIError extends Error {
  name: string = "Google API Error";
}

export class GoogleAPIRequestError extends Error {
  message: string = "Failed to make a request";
}
export class GoogleAPIResponseError extends Error {
  constructor(status: number, message: string) {
    super();

    this.message = message;
    this.status = status;
  }

  status: number;
}

export class GoogleAPILoadingError extends GoogleAPIError {
  message: string = "Failed to load";
}

export class GoogleAPIClientInitError extends GoogleAPIError {
  message: string = "Failed to initialize client";
}
