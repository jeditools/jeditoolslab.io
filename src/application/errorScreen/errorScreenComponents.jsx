// @flow strict

import * as React from "react";

import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { DelimiterComponent } from "ui/delimiter/delimiterComponents";
import { ErrorMessageComponent } from "application/errorMessage/errorMessageComponents";
import { TextComponent } from "ui/text/textComponents";
import { ErrorIconComponent } from "ui/icon";
import { BoxComponent } from "ui/box/boxComponents";
import { useIntlDictionary } from "../intl/intlHooks";

type Props = {
  error: Error,
};

export const ErrorScreenComponent = (props: Props): React.Node => {
  const { error } = props;

  const { errorScreen } = useIntlDictionary();

  return (
    <GridLayoutComponent mainAxis="y">
      <GridLayoutComponent
        mainAxis="x"
        primaryAlignment="apart"
        secondaryAlignment="center"
      >
        <TextComponent size="l" color="critical">
          {errorScreen.titleText}
        </TextComponent>
        <BoxComponent>
          <TextComponent size="l" color="critical">
            <ErrorIconComponent />
          </TextComponent>
        </BoxComponent>
      </GridLayoutComponent>
      <DelimiterComponent />
      <ErrorMessageComponent error={error} />
    </GridLayoutComponent>
  );
};
