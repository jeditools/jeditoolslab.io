// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { useIntlDictionary } from "../intl/intlHooks";
import { TaskNotFoundError } from "../task/taskConstants";
import { ProjectNotFoundError } from "../project/projectConstants";
import { PracticeNotFoundError } from "../practice/practiceErrors";

type Props = {|
  error: Error,
|};

export const ErrorMessageComponent = (props: Props): React.Node => {
  const { error } = props;

  const { errorMessage } = useIntlDictionary();

  const text = React.useMemo(() => {
    if (error instanceof TaskNotFoundError) {
      return errorMessage.taskNotFoundErrorMessage;
    }

    if (error instanceof ProjectNotFoundError) {
      return errorMessage.projectNotFoundErrorMessage;
    }

    if (error instanceof PracticeNotFoundError) {
      return errorMessage.projectNotFoundErrorMessage;
    }

    return errorMessage.generalErrorMessage;
  }, [error, errorMessage]);

  return <TextComponent color="critical">{text}</TextComponent>;
};
