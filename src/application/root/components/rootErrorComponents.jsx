// @flow strict

import * as React from "react";

type Props = {
  error: Error,
};

export const RootErrorComponent = ({ error }: Props): React.Node => (
  <div>
    <h1>
      {error.name}: {error.message}
    </h1>
    <p>{error.stack}</p>
  </div>
);
