// @flow strict

import * as React from "react";

import "./booleanControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  value: boolean,
  disabled?: boolean,
  onValueChange(nextValue: boolean): void,
|};

export const BooleanControlComponent = (props: Props): React.Node => {
  const { value, disabled, onValueChange } = props;

  const handleInputChange = React.useCallback(() => {
    onValueChange(!value);
  }, [onValueChange, value]);

  const className = createClassName("boolean-control")("checkbox");

  return (
    <input
      type="checkbox"
      disabled={disabled}
      className={className}
      checked={value}
      onChange={handleInputChange}
    />
  );
};
