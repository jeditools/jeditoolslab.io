// @flow strict

import type { ThemeService } from "../themeTypes";

import { normalizeStringToThemeMode } from "../themeUtils";

const localStorageKey = "themeMode";

export const themeServiceProd: ThemeService = {
  async getPersistedThemeMode() {
    const persistedThemeMode = localStorage.getItem(localStorageKey);

    if (persistedThemeMode) {
      return normalizeStringToThemeMode(persistedThemeMode);
    }

    return null;
  },
  async setPersistedThemeMode(nextThemeMode) {
    localStorage.setItem(localStorageKey, nextThemeMode);
  },
};
