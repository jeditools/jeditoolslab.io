// @flow strict

import type { Form, FormStatus, FormConstructor } from "utility/form/formTypes";

export const createForm = <DataType, ValidityType>(
  initialData: DataType
): Form<DataType, ValidityType> => ({
  status: "CREATED",
  data: initialData,
});

export const createFormConstructor = <DataType, ViolationType>(
  form: Form<DataType, ViolationType>
): FormConstructor<DataType, ViolationType> => ({
  return() {
    return form;
  },
  changeData(nextData: DataType) {
    if (form.status === "CREATED") {
      return createFormConstructor({
        ...form,
        data: nextData,
      });
    }

    if (form.status === "REJECTED") {
      return createFormConstructor({
        ...form,
        data: nextData,
      });
    }

    throw new IllegalFormModificationError();
  },
  toRejected(violations: ViolationType[]) {
    validateFormStatusTransition(form.status, "REJECTED");

    return createFormConstructor({
      data: form.data,
      status: "REJECTED",
      violations,
    });
  },
  toAccepted() {
    validateFormStatusTransition(form.status, "ACCEPTED");

    return createFormConstructor({
      data: form.data,
      status: "ACCEPTED",
    });
  },
});

const validateFormStatusTransition = (
  prevStatus: FormStatus,
  nextStatus: FormStatus
) => {
  switch (nextStatus) {
    case "REJECTED": {
      if (prevStatus !== "CREATED" && prevStatus !== "REJECTED") {
        throw new IllegalFormModificationError();
      }

      break;
    }
    default:
      break;
  }
};

class IllegalFormModificationError extends TypeError {
  message = "Illegal Form modification";
}
