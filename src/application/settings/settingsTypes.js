// @flow

export type SettingsManagementActivityName = "SETTINGS_MANAGEMENT";

export type SettingsManagementActivity = {|
  name: SettingsManagementActivityName,
|};
