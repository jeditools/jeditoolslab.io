// @flow strict

import type {
  NavigationActivity,
  NavigationActivityName,
} from "../navigation/navigationTypes";
import type {
  PracticeCreationActivity,
  PracticeCreationActivityName,
  PracticeEditingActivity,
  PracticeEditingActivityName,
  PracticeManagementActivity,
  PracticeManagementActivityName,
  PracticesManagementActivity,
  PracticesManagementActivityName,
} from "../practice/practiceTypes";
import type {
  ProjectCreationActivity,
  ProjectCreationActivityName,
  ProjectEditingActivity,
  ProjectEditingActivityName,
  ProjectManagementActivity,
  ProjectManagementActivityName,
  ProjectsManagementActivity,
  ProjectsManagementActivityName,
} from "../project/projectTypes";
import type {
  SettingsManagementActivity,
  SettingsManagementActivityName,
} from "../settings/settingsTypes";
import type {
  TaskCreationActivity,
  TaskCreationActivityName,
  TaskEditingActivity,
  TaskEditingActivityName,
  TaskManagementActivity,
  TaskManagementActivityName,
} from "../task/taskTypes";

export type Activity =
  | ProjectManagementActivity
  | ProjectsManagementActivity
  | ProjectEditingActivity
  | ProjectCreationActivity
  | TaskManagementActivity
  | TaskCreationActivity
  | TaskEditingActivity
  | PracticesManagementActivity
  | PracticeManagementActivity
  | PracticeCreationActivity
  | PracticeEditingActivity
  | NavigationActivity
  | SettingsManagementActivity;

export type ActivityName =
  | ProjectManagementActivityName
  | ProjectsManagementActivityName
  | ProjectEditingActivityName
  | ProjectCreationActivityName
  | TaskManagementActivityName
  | TaskCreationActivityName
  | TaskEditingActivityName
  | PracticesManagementActivityName
  | PracticeManagementActivityName
  | PracticeCreationActivityName
  | PracticeEditingActivityName
  | NavigationActivityName
  | SettingsManagementActivityName;
