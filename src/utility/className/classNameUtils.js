// @flow strict

import { withNaming } from "@bem-react/classname";

export const createClassName: any = withNaming({
  e: "__",
  m: "_",
});
