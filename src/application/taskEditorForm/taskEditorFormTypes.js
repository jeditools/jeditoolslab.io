// @flow

import type { Form } from "utility/form/formTypes";
import type { Task } from "application/task/taskTypes";
import type { ProjectId } from "../project/projectTypes";

type TaskEditorFormViolation__DescriptionRequired = {
  name: "DESCRIPTION_REQUIRED",
};

export type TaskEditorFormViolation =
  TaskEditorFormViolation__DescriptionRequired;

export type TaskEditorFormData = {|
  description: string,
  projectId: ProjectId,
|};

export type TaskEditorFormDataConstructor = {|
  return(): TaskEditorFormData,
  changeDescription(nextValue: string): TaskEditorFormDataConstructor,
  changeProjectId(nextValue: ProjectId): TaskEditorFormDataConstructor,
|};

export type TaskEditorForm = Form<TaskEditorFormData, TaskEditorFormViolation>;

export type TaskEditorFormDataNormalizator = {|
  toTask(): Task,
|};
