// @flow strict

import type { ImportManagerService } from "application/importManager/importManagerTypes";

import { persistedEventMemoryStorageService } from "application/persistedEvent/persistedEventStorageServices/persistedEventMemoryStorageServices";
import {
  createPersistedEventEntriesConstructor,
  typePersistedEventEntries,
} from "application/persistedEvent/persistedEventUtils/persistedEventConstructors";
import { fileService } from "utility/file/fileServices";

export const importManagerServiceDev: ImportManagerService = {
  async uploadData() {
    const files = await fileService.browse();

    for (let file of files) {
      const content = await fileService.readAsText(file);

      const parsedContent = JSON.parse(content);

      const initialPersistedEventEntries =
        typePersistedEventEntries(parsedContent);

      const persistedEventEntries = createPersistedEventEntriesConstructor(
        initialPersistedEventEntries
      )
        .cleanUpObsoleteEvents()
        .sortInChronologicalOrder()
        .get();

      await persistedEventMemoryStorageService.write(persistedEventEntries);
    }
  },
};
