// @flow strict

import {
  GOOGLE_API__SCRIPT_URL,
  GOOGLE_API__KEY,
  GOOGLE_API__CLIENT_ID,
  GOOGLE_API__DISCOVERY_DOCS,
  GOOGLE_API__SCOPE,
} from "utility/googleAPI/googleAPIConstants";
import {
  GoogleAPILoadingError,
  GoogleAPIClientInitError,
  GoogleAPIResponseError,
  GoogleAPIRequestError,
} from "utility/googleAPI/googleAPIUtils";
import { createAsyncCache } from "utility/asyncCache/asyncCacheUtils";
import { documentService } from "utility/document/documentServices";

const getGlobalAPI = () => window.gapi;

const isGlobalAPILoaded = () => window.gapi !== undefined;

const loadGlobalAPI = async () => {
  try {
    await documentService.injectScript(GOOGLE_API__SCRIPT_URL);
  } catch {
    throw new GoogleAPILoadingError();
  }
};

const loadModules = () => {
  return new Promise((resolve, reject) => {
    getGlobalAPI().load("client:auth2", {
      callback() {
        resolve();
      },
      onerror() {
        reject(new GoogleAPILoadingError());
      },
    });
  });
};

const initClient = () => {
  return new Promise((resolve, reject) => {
    getGlobalAPI()
      .client.init({
        apiKey: GOOGLE_API__KEY,
        clientId: GOOGLE_API__CLIENT_ID,
        discoveryDocs: GOOGLE_API__DISCOVERY_DOCS,
        scope: GOOGLE_API__SCOPE,
      })
      .then(
        () => {
          resolve();
        },
        () => {
          reject(new GoogleAPIClientInitError());
        }
      );
  });
};

const loadingProcessCache = createAsyncCache(async () => {
  await loadGlobalAPI();
  await loadModules();
  await initClient();
});

export const googleAPIService = {
  async getAPI(): Promise<any> {
    const apiLoaded = isGlobalAPILoaded();

    if (apiLoaded) {
      return getGlobalAPI();
    }

    await loadingProcessCache.read();

    return getGlobalAPI();
  },
  async handleRequest(thenable: any): Promise<any> {
    return thenable
      .getPromise()
      .catch((err) => {
        throw new GoogleAPIRequestError();
      })
      .then((response) => {
        if (response && (response.status < 200 || response.status > 299)) {
          throw new GoogleAPIResponseError(
            response.status,
            response.statusText
          );
        }

        return response;
      });
  },
};
