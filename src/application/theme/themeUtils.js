// @flow strict

import type { ThemeMode } from "./themeTypes";

class ThemeError extends Error {
  name = "Theme Error";
}

export const normalizeStringToThemeMode = (value: string): ThemeMode => {
  switch (value) {
    case "light":
    case "dark":
    case "auto":
      return value;
    default:
      throw new ThemeError(
        `Failed to normalize local storage value "${value}" to theme mode`
      );
  }
};
