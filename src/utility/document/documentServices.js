// @flow strict

import type { DocumentService } from "utility/document/documentTypes";

import { DocumentServiceScriptInjectionError } from "./documentConstants";
import { getMetaTagElementByName, isScriptInjected } from "./documentUtils";

export const documentService: DocumentService = {
  async injectScript(src) {
    return new Promise((resolve, reject) => {
      const injected = isScriptInjected(src);

      if (injected) {
        return;
      }

      const script = document.createElement("script");

      script.src = src;

      script.addEventListener("load", () => {
        resolve();
      });

      script.addEventListener("error", () => {
        reject(new DocumentServiceScriptInjectionError());
      });

      if (document.head) {
        document.head.appendChild(script);
      } else {
        throw new TypeError();
      }
    });
  },
  async setThemeColor(color) {
    const name = "theme-color";

    let themeColorElement = getMetaTagElementByName(name);

    if (!themeColorElement) {
      themeColorElement = document.createElement("meta");

      themeColorElement.name = name;
    }

    themeColorElement.content = color;
  },
  listenToVisibilityChange(listener) {
    const handleBlur = () => {
      listener(false);
    };

    const handleFocus = () => {
      listener(true);
    };

    const blurEvent = "blur";

    const focusEvent = "focus";

    window.addEventListener(blurEvent, handleBlur);
    window.addEventListener(focusEvent, handleFocus);

    const stopListening = () => {
      window.removeEventListener(blurEvent, handleBlur);
      window.removeEventListener(focusEvent, handleFocus);
    };

    return stopListening;
  },
  listenToConnectionStatusChange(listener) {
    const handleEvent = () => {
      listener(navigator.onLine);
    };

    const onlineEvent = "online";

    const offlineEvent = "offline";

    window.addEventListener(onlineEvent, handleEvent);
    window.addEventListener(offlineEvent, handleEvent);

    const stopListening = () => {
      window.removeEventListener(onlineEvent, handleEvent);
      window.removeEventListener(offlineEvent, handleEvent);
    };

    listener(navigator.onLine);

    return stopListening;
  },
};
