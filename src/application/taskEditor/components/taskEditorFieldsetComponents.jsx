// @flow strict

import type { ProjectId } from "../../project/projectTypes";
import type {
  TaskEditorFormData,
  TaskEditorFormViolation,
} from "../../taskEditorForm/taskEditorFormTypes";

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { TextControlComponent } from "ui/textControl/textControlComponents";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";
import { LabelComponent } from "ui/label/labelComponents";
import { SelectControlComponent } from "ui/selectControl/selectControlComponents.jsx";
import { SelectControlOptionComponent } from "ui/selectControl/selectControlOptionComponents.jsx";
import { useIntlDictionary } from "../../intl/intlHooks";
import { useProjectSelectControl } from "../../project/projectHooks/_useProjectSelectControl";
import { createTaskEditorFormDataConstructor } from "../../taskEditorForm/taskEditorFormUtils";

type Props = {
  disabled: boolean,
  data: TaskEditorFormData,
  violations: TaskEditorFormViolation[],
  onDataChanged(nextData: TaskEditorFormData): void,
};

export const TaskEditorFieldsetComponent = (props: Props): React.Node => {
  const { disabled, data, violations, onDataChanged } = props;

  const { taskEditor: dictionary } = useIntlDictionary();

  const changeTaskDescription = React.useCallback(
    (nextValue: string) => {
      onDataChanged(
        createTaskEditorFormDataConstructor(data)
          .changeDescription(nextValue)
          .return()
      );
    },
    [data, onDataChanged]
  );

  const changeProjectId = React.useCallback(
    (nextValue: ProjectId) => {
      onDataChanged(
        createTaskEditorFormDataConstructor(data)
          .changeProjectId(nextValue)
          .return()
      );
    },
    [data, onDataChanged]
  );

  const projectIdControl = useProjectSelectControl(changeProjectId);

  const hasDescriptionRequiredHint = violations.some(
    (violation) => violation.name === "DESCRIPTION_REQUIRED"
  );

  return (
    <GridLayoutComponent mainAxis="y">
      <GridLayoutComponent mainAxis="y">
        <LabelComponent>
          <GridLayoutComponent mainAxis="y">
            <TextComponent>{dictionary.descriptionLabelText}</TextComponent>
            <TextControlComponent
              placeholder={dictionary.descriptionPlaceholderText}
              value={data.description}
              disabled={disabled}
              multiline
              autoFocus
              onValueChange={changeTaskDescription}
            />
            {hasDescriptionRequiredHint && (
              <TextComponent size="s" color="critical">
                {dictionary.requiredFieldHintText}
              </TextComponent>
            )}
          </GridLayoutComponent>
        </LabelComponent>
      </GridLayoutComponent>
      <GridLayoutComponent mainAxis="y">
        <LabelComponent>
          <GridLayoutComponent mainAxis="y">
            <TextComponent>{dictionary.projectIdLabelText}</TextComponent>
            <SelectControlComponent
              disabled={disabled}
              selectedValue={data.projectId}
              onSelect={projectIdControl.select}
            >
              {projectIdControl.options.map((option) => (
                <SelectControlOptionComponent
                  key={option.value}
                  value={option.value}
                >
                  {option.name}
                </SelectControlOptionComponent>
              ))}
            </SelectControlComponent>
          </GridLayoutComponent>
        </LabelComponent>
      </GridLayoutComponent>
    </GridLayoutComponent>
  );
};
