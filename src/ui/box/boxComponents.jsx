// @flow strict

import * as React from "react";

import "./boxStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  padding?: "vertical" | "horizontal" | "around",
  children: React.Node,
|};

export const BoxComponent = (props: Props): React.Node => {
  const { children, padding = "around" } = props;

  const className = createClassName("box")({ padding });

  return <div className={className}>{children}</div>;
};
