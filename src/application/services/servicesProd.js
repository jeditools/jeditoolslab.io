// @flow strict

import type { Services } from "application/services/servicesTypes";

import { syncServiceProd } from "application/sync/syncServices/syncServiceProd";
import { migrationServiceProd } from "application/migration/migrationServices/migrationServicesProd";
import { persistedEventProdService } from "application/persistedEvent/persistedEventServices/persistedEventProdServices";
import { documentService } from "utility/document/documentServices";
import { projectManagerServiceProd } from "../projectManager/services/projectManagerServiceProd";
import { projectEditorServiceProd } from "../projectEditor/services/projectEditorServiceProd";
import { repositoryServiceProd } from "../repository/services/repositoryServiceProd";
import { exportManagerServiceProd } from "../exportManager/services/exportManagerServiceProd";
import { importManagerServiceProd } from "../importManager/services/importManagerServiceProd";
import { practiceEditorServiceProd } from "../practiceEditor/services/practiceEditorServiceProd";
import { practiceServiceProd } from "../practice/services/practiceServiceProd";
import { practiceManagerServiceProd } from "../practiceManager/services/practiceManagerServiceProd";
import { taskServiceProd } from "../task/services/taskServiceProd";
import { taskEditorServiceProd } from "../taskEditor/services/taskEditorServiceProd";
import { themeServiceProd } from "../theme/services/themeServiceProd";
import { updateServiceProd } from "../update/services/updateServiceProd";
import { projectServiceProd } from "../project/services/projectServiceProd";

export const servicesProd: Services = {
  syncService: syncServiceProd,
  documentService: documentService,
  taskService: taskServiceProd,
  taskEditorService: taskEditorServiceProd,
  themeService: themeServiceProd,
  updateService: updateServiceProd,
  projectService: projectServiceProd,
  projectManagerService: projectManagerServiceProd,
  projectEditorService: projectEditorServiceProd,
  repositoryService: repositoryServiceProd,
  migrationService: migrationServiceProd,
  exportManagerService: exportManagerServiceProd,
  importManagerService: importManagerServiceProd,
  practiceEditorService: practiceEditorServiceProd,
  practiceService: practiceServiceProd,
  practiceManagerService: practiceManagerServiceProd,
  persistedEventService: persistedEventProdService,
};
