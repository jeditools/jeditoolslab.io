// @flow strict

import type { PracticeFormViolation } from "application/practiceForm/practiceFormTypes";
import type { Practice } from "application/practice/practiceTypes";

export const checkPracticeForViolations = (
  practice: Practice
): PracticeFormViolation[] => {
  const { name, regularityInDays } = practice;

  const violations = [];

  if (name.trim().length === 0) {
    violations.push({
      name: "NAME_REQUIRED",
    });
  }

  if (regularityInDays < 1) {
    violations.push({
      name: "REGULARITY_IN_DAYS_REQUIRED",
    });
  }

  return violations;
};
