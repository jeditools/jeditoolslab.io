// @flow

import { AsyncTimeout } from "@draftup/utility.js";

export const devServicesAsyncTimeoutNormal: any = new AsyncTimeout(200);

export const devServicesAsyncTimeoutLong: any = new AsyncTimeout(1000);
