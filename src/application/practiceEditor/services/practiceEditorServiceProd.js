// @flow strict

import type { PracticeEditorService } from "application/practiceEditor/practiceEditorTypes";

import { practiceServiceProd } from "../../practice/services/practiceServiceProd";
import { PracticeWithNameAlreadyExistsError } from "../../practice/practiceErrors";

export const practiceEditorServiceProd: PracticeEditorService = {
  async submit(editedPracticeId, practice, params) {
    const practiceEntries = await practiceServiceProd.getAllPracticeEntries();

    const practiceEntryWithSameName = practiceEntries.find(
      ([, currentPractice]) => currentPractice.name === practice.name
    );

    if (
      practiceEntryWithSameName &&
      practiceEntryWithSameName[0] !== editedPracticeId
    ) {
      throw new PracticeWithNameAlreadyExistsError(practice.name);
    }

    if (editedPracticeId) {
      await practiceServiceProd.putPractice(editedPracticeId, practice);

      return editedPracticeId;
    } else {
      return practiceServiceProd.addPractice(practice);
    }
  },
};
