// @flow strict

import type { ThemeMode } from "../theme/themeTypes";
import type { UpdateStatus } from "../update/updateTypes";
import type { Activity } from "application/activity/activityTypes";
import type { Sync } from "application/sync/syncTypes";

type NextActivityRecieved = {|
  name: "ROOT__NEXT_ACTIVITY_RECIEVED",
  nextActivity: Activity,
|};

type SyncRecieved = {|
  name: "ROOT__SYNC_RECIEVED",
  sync: Sync,
|};

type PersistedThemeModeRecieved = {|
  name: "ROOT__PERSISTED_THEME_MODE_RECIEVED",
  persistedThemeMode: null | ThemeMode,
|};

type UpdateStatusRecieved = {|
  name: "ROOT__UPDATE_STATUS_RECIEVED",
  updateStatus: UpdateStatus,
|};

type MigrationComplete = {|
  name: "ROOT__MIGRATION_COMPLETE",
|};

type DocumentVisibilityChanged = {|
  name: "ROOT__DOCUMENT_VISIBILITY_CHANGED",
  visible: boolean,
|};

export type RootEvent =
  | NextActivityRecieved
  | SyncRecieved
  | PersistedThemeModeRecieved
  | UpdateStatusRecieved
  | MigrationComplete
  | DocumentVisibilityChanged;
