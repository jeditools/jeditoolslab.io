// @flow strict

import type { RepositoryService } from "application/repository/repositoryTypes";

import { practiceServiceProd } from "../../practice/services/practiceServiceProd";
import { projectServiceProd } from "../../project/services/projectServiceProd";
import { taskServiceProd } from "../../task/services/taskServiceProd";

export const repositoryServiceProd: RepositoryService = {
  async getData(query, params) {
    const {
      projectIds,
      taskIds,
      practiceIds,
      allProjects,
      allTasks,
      allPractices,
    } = query;

    let projectEntriesPromise = Promise.resolve([]);

    let taskEntriesPromise = Promise.resolve([]);

    let practiceEntriesPromise = Promise.resolve([]);

    if (allProjects) {
      projectEntriesPromise = projectServiceProd.getAllProjectEntries();
    }

    if (projectIds && projectIds.length > 0) {
      projectEntriesPromise = projectServiceProd.getProjectEntries(projectIds);
    }

    if (allTasks) {
      taskEntriesPromise = taskServiceProd.getAllTaskEntries();
    }

    if (taskIds && taskIds.length > 0) {
      taskEntriesPromise = taskServiceProd.getTaskEntries(taskIds);
    }

    if (allPractices) {
      practiceEntriesPromise = practiceServiceProd.getAllPracticeEntries();
    }

    if (practiceIds && practiceIds.length > 0) {
      practiceEntriesPromise =
        practiceServiceProd.getPracticeEntries(practiceIds);
    }

    const [projectEntries, taskEntries, practiceEntries] = await Promise.all([
      projectEntriesPromise,
      taskEntriesPromise,
      practiceEntriesPromise,
    ]);

    return { projectEntries, taskEntries, practiceEntries };
  },
};
