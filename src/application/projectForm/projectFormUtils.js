// @flow strict

import type { Project } from "../project/projectTypes";
import type { ProjectFormViolation } from "application/projectForm/projectFormTypes";

export const checkProjectForViolations = (
  project: Project
): ProjectFormViolation[] => {
  const { name } = project;

  const violations = [];

  if (name.trim().length === 0) {
    violations.push({
      name: "NAME_REQUIRED",
    });
  }

  return violations;
};
