// @flow strict

import { formatDistanceToNow } from "date-fns";
import { ru } from "date-fns/locale";

import type { ProjectsManagerDictionary } from "application/projectsManager/projectsManagerTypes";

export const projectsManagerDictionaryRU: ProjectsManagerDictionary = {
  noProjectsText: "Нет проектов",
  newProjectLinkText: "Новый проект",
  latestCompletionText(timestamp) {
    return formatDistanceToNow(timestamp, { locale: ru, addSuffix: true });
  },
};
