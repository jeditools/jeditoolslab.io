// @flow strict

import * as React from "react";

import "./selectControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  children: React.Node,
  selectedValue: string,
  onSelect(value: string): void,
  disabled?: boolean,
|};

export function SelectControlComponent(props: Props): React.Node {
  const { children, selectedValue, disabled, onSelect } = props;

  const className = createClassName("select")("wrapper");

  const handleChange = React.useCallback(
    (event: SyntheticEvent<HTMLInputElement>) => {
      const {
        currentTarget: { value: nextValue },
      } = event;

      onSelect(nextValue);
    },
    [onSelect]
  );

  return (
    <select
      className={className}
      value={selectedValue}
      disabled={disabled}
      onChange={handleChange}
    >
      {children}
    </select>
  );
}
