// @flow strict

import type { ProjectService } from "../projectTypes";

import { createProjectService } from "../projectServiceCreators";
import { persistedEventDevService } from "application/persistedEvent/persistedEventServices/persistedEventDevServices";

export const projectServiceDev: ProjectService = createProjectService(
  persistedEventDevService
);
