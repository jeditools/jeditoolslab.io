// @flow

import type { ParagraphBlock } from "./paragraphTypes";

import {
  checkTextIsURL,
  splitTextByNewLines,
  splitTextByURLs,
} from "../../utility/textUtils";
import { createUniqueId } from "../../utility/uniqueId/uniqueIdUtils";

export function createParagraphBlocksFromText(text: string): ParagraphBlock[] {
  const blocks = [];

  const splittedByNewLine = splitTextByNewLines(text);

  splittedByNewLine.forEach((pieceOfText) => {
    const textIsURL = checkTextIsURL(pieceOfText);

    if (textIsURL) {
      blocks.push({
        id: createUniqueId(),
        type: "URL",
        url: new URL(pieceOfText),
      });
    } else {
      const splittedByURL = splitTextByURLs(pieceOfText);

      splittedByURL.forEach((pieceOfLine) => {
        const textIsURL = checkTextIsURL(pieceOfLine);

        if (textIsURL) {
          blocks.push({
            id: createUniqueId(),
            type: "URL",
            url: new URL(pieceOfLine),
          });
        } else {
          blocks.push({
            id: createUniqueId(),
            type: "TEXT",
            text: pieceOfLine,
          });
        }
      });
    }
  });

  return blocks;
}
