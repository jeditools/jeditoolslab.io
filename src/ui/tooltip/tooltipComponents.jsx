// @flow strict

import * as React from "react";

import "./tooltipStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  text: string,
  children: React.Node,
|};

export const TooltipComponent = (props: Props): React.Node => {
  const { text, children } = props;

  const className = createClassName("tooltip")();

  return (
    <div title={text} className={className}>
      {children}
    </div>
  );
};
