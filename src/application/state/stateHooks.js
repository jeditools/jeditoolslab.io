// @flow strict

import React from "react";

import type { State } from "application/state/stateTypes";

import { StateContext } from "application/state/stateContexts";

export const useState = (): State => {
  return React.useContext(StateContext);
};
