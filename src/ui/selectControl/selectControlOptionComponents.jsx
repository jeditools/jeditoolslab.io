// @flow strict

import * as React from "react";

type Props = {|
  children: React.Node,
  value: string,
  disabled?: boolean,
|};

export function SelectControlOptionComponent(props: Props): React.Node {
  const { children, value, disabled } = props;

  return (
    <option value={value} disabled={disabled}>
      {children}
    </option>
  );
}
