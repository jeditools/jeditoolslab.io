// @flow strict

import type { Reducer, ReducerCallback } from "utility/reducer/reducerTypes";

export const createReducer =
  <StateType, EventType>(
    reducer: Reducer<StateType, EventType>,
    callback?: ReducerCallback<StateType, EventType>
  ): Reducer<StateType, EventType> =>
  (prevState, event) => {
    const nextState = reducer(prevState, event);

    if (callback) {
      callback({ prevState, nextState }, event);
    }

    return nextState;
  };
