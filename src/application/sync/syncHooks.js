// @flow strict

import * as React from "react";

import { MINUTE_MS } from "utility/time/timeConstants";
import { useState } from "application/state/stateHooks";
import { useServices } from "../services/servicesHooks";
import { useEventDispatcher } from "../event/eventHooks";

export const useShouldUpdateSync = (): boolean => {
  const { sync } = useState();

  return sync === null;
};

export const useSyncTimetamp = (): null | number => {
  const { sync } = useState();

  return sync?.timestamp || null;
};

export const useSyncEnabled = (): boolean => {
  const { sync } = useState();

  return !!sync && sync.status === "ENABLED";
};

export const useSyncInProgress = (): boolean => {
  const { sync } = useState();

  return !!sync && sync.inProgress;
};

export const useSyncDisabled = (): boolean => {
  const { sync } = useState();

  return !!sync && sync.status === "DISABLED";
};

export const useSyncFailed = (): boolean => {
  const { sync } = useState();

  return !!sync && sync.status === "FAILED";
};

export const useShouldSync = (): boolean => {
  const enabled = useSyncEnabled();

  const timestamp = useSyncTimetamp();

  const { visible } = useState();

  if (enabled) {
    if (timestamp === null) {
      return true;
    }

    if (visible) {
      return false;
    }

    const diffMS = Date.now() - timestamp;

    return diffMS > MINUTE_MS;
  }

  return false;
};

export function useSyncProcess() {
  const { persistedEventService } = useServices();

  const shouldSync = useShouldSync();

  const syncInProgress = useSyncInProgress();

  const dispatch = useEventDispatcher();

  React.useEffect(() => {
    if (shouldSync && !syncInProgress) {
      dispatch({
        name: "SYNC__STARTED",
      });

      persistedEventService
        .sync()
        .then(() => {
          dispatch({
            name: "SYNC__COMPLETED",
          });
        })
        .catch((error) => {
          dispatch({
            name: "SYNC__FAILED",
            error,
          });
        });
    }
  }, [dispatch, persistedEventService, shouldSync, syncInProgress]);
}
