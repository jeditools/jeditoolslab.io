// @flow strict

import type { ActivityControlsDictionary } from "../activityControls/activityControlsTypes";
import type { AppearanceManagerDictionary } from "../appearanceManager/appearanceManagerTypes";
import type { ErrorMessageDictionary } from "../errorMessage/errorMessageTypes";
import type { ErrorScreenDictionary } from "../errorScreen/errorScreenTypes";
import type { ExportManagerDictionary } from "../exportManager/exportManagerTypes";
import type { ImportManagerDictionary } from "../importManager/importManagerTypes";
import type { IndicatorDictionary } from "../indicator/indicatorTypes";
import type { NavigationDictionary } from "../navigation/navigationTypes";
import type { PendingMessageDictionary } from "../pendingMessage/pendingMessageTypes";
import type { PracticeEditorDictionary } from "../practiceEditor/practiceEditorTypes";
import type { PracticeManagerDictionary } from "../practiceManager/practiceManagerTypes";
import type { PracticesManagerDictionary } from "../practicesManager/practicesManagerTypes";
import type { ProjectEditorDictionary } from "../projectEditor/projectEditorTypes";
import type { ProjectManagerDictionary } from "../projectManager/projectManagerTypes";
import type { ProjectsManagerDictionary } from "../projectsManager/projectsManagerTypes";
import type { SyncManagerDictionary } from "../syncManager/syncManagerTypes";
import type { TaskEditorDictionary } from "../taskEditor/taskEditorTypes";
import type { TaskManagerDictionary } from "../taskManager/taskManagerTypes";
import type { TasksManagerDictionary } from "../tasksManager/tasksManagerTypes";
import type { UpdateManagerDictionary } from "../updateManager/updateManagerTypes";

export type IntlDictionary = {|
  taskEditor: TaskEditorDictionary,
  taskManager: TaskManagerDictionary,
  activityControls: ActivityControlsDictionary,
  navigation: NavigationDictionary,
  appearanceManager: AppearanceManagerDictionary,
  errorMessage: ErrorMessageDictionary,
  pendingMessage: PendingMessageDictionary,
  tasksManager: TasksManagerDictionary,
  syncManager: SyncManagerDictionary,
  errorScreen: ErrorScreenDictionary,
  updateManager: UpdateManagerDictionary,
  projectEditor: ProjectEditorDictionary,
  projectsManager: ProjectsManagerDictionary,
  projectManager: ProjectManagerDictionary,
  indicator: IndicatorDictionary,
  exportManager: ExportManagerDictionary,
  importManager: ImportManagerDictionary,
  practicesManager: PracticesManagerDictionary,
  practiceEditor: PracticeEditorDictionary,
  practiceManager: PracticeManagerDictionary,
|};
