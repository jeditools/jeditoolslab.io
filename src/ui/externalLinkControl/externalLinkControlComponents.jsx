// @flow strict

import * as React from "react";

import "./externalLinkControlStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type Props = {|
  children: React.Node,
  to: string,
|};

export const ExternalLinkControlComponent = (props: Props): React.Node => {
  const { children, to } = props;

  const className = createClassName("externalLinkControl")();

  return (
    <a href={to} target="_blank" rel="noreferrer" className={className}>
      {children}
    </a>
  );
};
