// @flow strict

import type { ProjectId } from "../project/projectTypes";
import type {
  TaskEditorFormData,
  TaskEditorFormViolation,
  TaskEditorFormDataConstructor,
  TaskEditorFormDataNormalizator,
} from "application/taskEditorForm/taskEditorFormTypes";

export const createTaskEditorFormData = (
  projectId: ProjectId
): TaskEditorFormData => ({
  description: "",
  projectId,
});

export const createTaskEditorFormDataConstructor = (
  values: TaskEditorFormData
): TaskEditorFormDataConstructor => ({
  return() {
    return values;
  },
  changeDescription(nextData) {
    return createTaskEditorFormDataConstructor({
      ...values,
      description: nextData,
    });
  },
  changeProjectId(nextData) {
    return createTaskEditorFormDataConstructor({
      ...values,
      projectId: nextData,
    });
  },
});

export const validateTaskEditorFormData = (
  values: TaskEditorFormData
): TaskEditorFormViolation[] => {
  const { description } = values;

  const violations: TaskEditorFormViolation[] = [];

  if (description.trim().length === 0) {
    violations.push({
      name: "DESCRIPTION_REQUIRED",
    });
  }

  return violations;
};

export const normalizeTaskEditorFormData = (
  formData: TaskEditorFormData
): TaskEditorFormDataNormalizator => ({
  toTask() {
    return {
      creationTimestamp: Date.now(),
      description: formData.description,
      completionTimestamp: null,
      projectId: formData.projectId,
    };
  },
});
