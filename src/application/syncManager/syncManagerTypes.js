// @flow strict

type SyncEnabled = {|
  name: "SYNC_MANAGER__SYNC_ENABLED",
|};

type SyncDisabled = {|
  name: "SYNC_MANAGER__SYNC_DISABLED",
|};

export type SyncManagerEvent = SyncEnabled | SyncDisabled;

export type SyncManagerDictionary = {|
  sectionLabelText: string,
  inputLabelText: string,
  enabledDescriptionText: string,
  disabledDescriptionText: string,
  disableActionLabelText: string,
  enableActionLabelText: string,
|};
