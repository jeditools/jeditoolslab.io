// @flow strict

export type SyncStatus = "DISABLED" | "ENABLED" | "FAILED";

export type Sync = {|
  status: SyncStatus,
  inProgress: boolean,
  timestamp: null | number,
|};

export type SyncService = {|
  read(): Promise<Sync>,
  update(nextValue: Sync): Promise<void>,
|};

export type SyncEvent =
  | {
      name: "SYNC__STARTED",
    }
  | {
      name: "SYNC__COMPLETED",
    }
  | {
      name: "SYNC__FAILED",
      error: Error,
    };
