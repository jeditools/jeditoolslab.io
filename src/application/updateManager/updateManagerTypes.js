// @flow strict

export type UpdateManagerDictionary = {|
  sectionLabelText: string,
  actionLabelText: string,
  descriptionText: string,
|};
