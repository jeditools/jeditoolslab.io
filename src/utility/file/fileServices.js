// @flow strict

import type { FileService } from "utility/file/fileTypes";

import { FileError } from "utility/file/fileUtils";

export const fileService: FileService = {
  async write(name, content) {
    const { body } = document;

    if (body) {
      const contentBlob = new Blob([content], { type: "octet/stream" });

      const downloadURL = URL.createObjectURL(contentBlob);

      const anchorElement = document.createElement("a");

      body.appendChild(anchorElement);

      anchorElement.style.display = "none";

      anchorElement.href = downloadURL;

      anchorElement.download = name;

      anchorElement.click();

      window.URL.revokeObjectURL(downloadURL);

      body.removeChild(anchorElement);
    } else {
      throw new FileError("Failed to get body DOM node");
    }
  },
  async browse() {
    return new Promise((resolve, reject) => {
      const { body } = document;

      if (body) {
        const inputElement = document.createElement("input");

        body.appendChild(inputElement);

        inputElement.style.display = "none";

        inputElement.type = "file";

        inputElement.click();

        const handleChange = () => {
          resolve(inputElement.files);

          inputElement.removeEventListener("change", handleChange);

          body.removeChild(inputElement);
        };

        inputElement.addEventListener("change", handleChange);
      } else {
        throw new FileError("Failed to get body DOM node");
      }
    });
  },
  async readAsText(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.readAsText(file);

      reader.onloadend = () => {
        if (typeof reader.result === "string") {
          resolve(reader.result);
        } else {
          throw new FileError("Failed to read as text");
        }
      };
    });
  },
};
