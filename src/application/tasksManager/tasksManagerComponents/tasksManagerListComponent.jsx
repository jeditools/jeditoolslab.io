// @flow

import type { TaskId } from "../../task/taskTypes";

import * as React from "react";

import { TaskItemComponent } from "./tasksManagerItemComponent.jsx";
import { GridLayoutComponent } from "ui/gridLayout/gridLayoutComponents";

type Props = {|
  taskIds: TaskId[],
|};

export const TasksManagerListComponent = (props: Props): React.Node => {
  const { taskIds } = props;

  return (
    <GridLayoutComponent mainAxis="y">
      {taskIds.map((taskId) => (
        <TaskItemComponent key={taskId} taskId={taskId} />
      ))}
    </GridLayoutComponent>
  );
};
