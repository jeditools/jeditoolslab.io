// @flow strict

const newLineRegExp = /(?=[\r\n])+/;

const urlRegExp =
  /^(https?:\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])$/i;

const urlSearchRegExp =
  /(https?:\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/i;

export const splitTextByNewLines = (text: string): string[] =>
  text.split(newLineRegExp);

export const splitTextByURLs = (text: string): string[] =>
  text.split(urlSearchRegExp);

export const checkTextIsURL = (text: string): boolean => urlRegExp.test(text);
