// @flow strict

import type {
  PersistedEventEntries,
  PersistedEventEntriesPracticeLense,
} from "application/persistedEvent/persistedEventTypes";

import { createEntriesConstructor } from "utility/entry/entryUtils";
import {
  createPracticeConstructor,
  createPractice,
} from "application/practice/practiceCreators";

export const createPersistedEventEntriesPracticeLense = (
  persistedEventEntries: PersistedEventEntries
): PersistedEventEntriesPracticeLense => ({
  selectPracticeRelatedEventEntries(practiceId) {
    return persistedEventEntries.filter(([id, event]) => {
      switch (event.name) {
        case "PracticeCreated":
        case "PracticeUpdated":
        case "PracticeCompleted":
        case "PracticeDeleted": {
          if (!practiceId || event.practiceId === practiceId) {
            return true;
          }

          return false;
        }
        default:
          return false;
      }
    });
  },
  selectPractice(practiceId) {
    const practiceRelatedEventEntries =
      createPersistedEventEntriesPracticeLense(
        persistedEventEntries
      ).selectPracticeRelatedEventEntries(practiceId);

    const deleted = practiceRelatedEventEntries.some(
      ([id, event]) => event.name === "PracticeDeleted"
    );

    if (!deleted) {
      const practiceConstructor = practiceRelatedEventEntries.reduce(
        (acc = createPracticeConstructor(createPractice()), [id, event]) => {
          switch (event.name) {
            case "PracticeCreated":
            case "PracticeUpdated":
              return acc
                .setName(event.practice.name)
                .setDetails(event.practice.details)
                .setRegularityInDays(event.practice.regularityInDays);
            case "PracticeCompleted":
              return acc.setLatestCompletionTimestamp(event.timestamp);
            default:
              return acc;
          }
        },
        undefined
      );

      return practiceConstructor ? practiceConstructor.get() : null;
    }

    return null;
  },
  selectPracticeEntries() {
    const practicesRelatedEventEntries =
      createPersistedEventEntriesPracticeLense(
        persistedEventEntries
      ).selectPracticeRelatedEventEntries();

    return practicesRelatedEventEntries
      .reduce((acc, [id, event]) => {
        switch (event.name) {
          case "PracticeCreated": {
            const { practiceId: persistedPracticeId } = event;

            const persistedPractice = createPersistedEventEntriesPracticeLense(
              persistedEventEntries
            ).selectPractice(persistedPracticeId);

            if (persistedPractice) {
              return acc.setEntry(persistedPracticeId, persistedPractice);
            }

            return acc;
          }
          default:
            return acc;
        }
      }, createEntriesConstructor([]))
      .return();
  },
});
