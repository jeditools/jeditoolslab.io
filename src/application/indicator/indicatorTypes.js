// @flow strict

export type IndicatorDictionary = {
  updateAvailableTooltipText: string,
  syncDisabledTooltipText: string,
  syncFailedTooltipText: string,
  syncInProcessTooltipText: string,
  offlineTooltipText: string,
};
