// @flow strict

import { EventDispatcherContext } from "../event/eventContexts";
import { stateReducer } from "../state/reducers/stateReducer";
import { StateContext } from "../state/stateContexts";
import { createState } from "../state/stateUtils";

import * as React from "react";

type Props = {|
  children: React.Node,
|};

export const StoreProviderComponent = (props: Props): React.Node => {
  const { children } = props;

  const [state, dispatch] = React.useReducer(stateReducer, null, createState);

  return (
    <EventDispatcherContext.Provider value={dispatch}>
      <StateContext.Provider value={state}>{children}</StateContext.Provider>
    </EventDispatcherContext.Provider>
  );
};
