// @flow strict

import type { TaskId } from "../task/taskTypes";

import { useRouteMatch } from "react-router-dom";

import { createActivityRoutePattern } from "application/activity/activityUtils";

export const useEditedTaskId = (): null | TaskId => {
  const match = useRouteMatch(createActivityRoutePattern("TASK_EDITING"));

  if (!match) {
    return null;
  }

  const {
    params: { taskId },
  } = match;

  if (typeof taskId === "string") {
    return taskId;
  }

  return null;
};
