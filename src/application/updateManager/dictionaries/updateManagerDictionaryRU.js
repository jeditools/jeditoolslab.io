// @flow strict

import type { UpdateManagerDictionary } from "../updateManagerTypes";

export const updateManagerDictRU: UpdateManagerDictionary = {
  sectionLabelText: "Обновление",
  actionLabelText: "Обновить",
  descriptionText:
    'Доступна новая версия приложения. Нажмите "Обновить" чтобы изменения вступили в силу.',
};
