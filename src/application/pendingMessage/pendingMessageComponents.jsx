// @flow strict

import * as React from "react";

import { TextComponent } from "ui/text/textComponents";
import { useIntlDictionary } from "../intl/intlHooks";

export const PendingMessageComponent = (): React.Node => {
  const { pendingMessage } = useIntlDictionary();

  return <TextComponent>{pendingMessage.messageText}</TextComponent>;
};
