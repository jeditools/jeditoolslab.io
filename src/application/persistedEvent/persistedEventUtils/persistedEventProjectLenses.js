// @flow strict

import type {
  PersistedEventEntries,
  PersistedEventEntriesProjectLense,
} from "application/persistedEvent/persistedEventTypes";

import { createEntriesLense } from "../../../utility/entry/entryUtils";
import {
  createProject,
  createProjectConstructor,
} from "../../project/projectCreators";
import { createEntriesConstructor } from "utility/entry/entryUtils";

export const createPersistedEventEntriesProjectLense = (
  persistedEventEntries: PersistedEventEntries
): PersistedEventEntriesProjectLense => ({
  selectProjectRelatedEventEntries(projectId) {
    return persistedEventEntries.filter(([id, event]) => {
      switch (event.name) {
        case "ProjectCreated":
        case "ProjectUpdated":
        case "ProjectDeleted": {
          if (!projectId || event.projectId === projectId) {
            return true;
          }

          return false;
        }
        default:
          return false;
      }
    });
  },
  selectProject(projectId) {
    const projectRelatedEventEntries = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProjectRelatedEventEntries(projectId);

    const deleted = projectRelatedEventEntries.some(
      ([id, event]) => event.name === "ProjectDeleted"
    );

    if (!deleted) {
      const projectConstructor = projectRelatedEventEntries.reduce(
        (
          acc = createProjectConstructor(createProject(Date.now())),
          [id, event]
        ) => {
          switch (event.name) {
            case "ProjectCreated":
              return acc
                .changeName(event.project.name)
                .changeDetails(event.project.details)
                .changeCreationTimestamp(event.timestamp);
            case "ProjectUpdated":
              return acc
                .changeName(event.project.name)
                .changeDetails(event.project.details);
            default:
              return acc;
          }
        },
        undefined
      );

      return projectConstructor ? projectConstructor.return() : null;
    }

    return null;
  },
  selectProjectEntries() {
    const projectsRelatedEventEntries = createPersistedEventEntriesProjectLense(
      persistedEventEntries
    ).selectProjectRelatedEventEntries();

    const deletedProjectIds = [];

    return projectsRelatedEventEntries
      .reduce((acc, [id, event]) => {
        switch (event.name) {
          case "ProjectCreated": {
            const { projectId: persistedProjectId } = event;

            if (!deletedProjectIds.includes(persistedProjectId)) {
              const persistedProject = createPersistedEventEntriesProjectLense([
                [id, event],
              ]).selectProject(persistedProjectId);

              if (persistedProject) {
                return acc.setEntry(persistedProjectId, persistedProject);
              }
            }

            return acc;
          }
          case "ProjectUpdated": {
            const { projectId: persistedProjectId } = event;

            if (!deletedProjectIds.includes(persistedProjectId)) {
              const persistedProject = createPersistedEventEntriesProjectLense([
                [id, event],
              ]).selectProject(persistedProjectId);

              if (persistedProject) {
                const prevProject = createEntriesLense(acc.return()).selectItem(
                  persistedProjectId
                );

                if (prevProject) {
                  const nextProject = createProjectConstructor(prevProject)
                    .changeName(persistedProject.name)
                    .changeDetails(persistedProject.details)
                    .return();

                  return acc.setEntry(persistedProjectId, nextProject);
                }
              }
            }

            return acc;
          }
          case "ProjectDeleted": {
            const { projectId: persistedProjectId } = event;

            if (!deletedProjectIds.includes(persistedProjectId)) {
              deletedProjectIds.push(persistedProjectId);

              return acc.deleteEntry(persistedProjectId);
            }

            return acc;
          }
          default:
            return acc;
        }
      }, createEntriesConstructor([]))
      .return();
  },
  selectDeletedProjectIds() {
    return persistedEventEntries.reduce((acc, [id, event]) => {
      if (event.name === "ProjectDeleted") {
        acc.push(event.projectId);
      }
      return acc;
    }, []);
  },
});
