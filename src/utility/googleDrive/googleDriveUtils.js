// @flow strict

export class GoogleDriveError extends Error {
  name: string = "Google Drive Error";
}

export class GoogleDriveFileNotFoundError extends GoogleDriveError {
  message: string = "File not found error";
}
