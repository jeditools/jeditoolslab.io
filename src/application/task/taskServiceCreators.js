// @flow strict

import type { PersistedEventService } from "application/persistedEvent/persistedEventTypes";
import type { TaskService } from "./taskTypes";

import { createUniqueId } from "utility/uniqueId/uniqueIdUtils";
import { createPersistedEventEntriesTaskLense } from "application/persistedEvent/persistedEventUtils/persistedEventTaskLenses";
import { TaskNotFoundError } from "./taskConstants";

export const createTaskService = (
  persistedEventService: PersistedEventService
): TaskService => ({
  async deleteTask(taskId, params) {
    await persistedEventService.addEvents([
      {
        name: "TaskDeleted",
        timestamp: Date.now(),
        taskId,
      },
    ]);
  },
  async completeTask(taskId, params) {
    await persistedEventService.addEvents([
      {
        name: "TaskCompleted",
        timestamp: Date.now(),
        taskId,
      },
    ]);
  },
  async addTask(task, params) {
    const taskId = createUniqueId();

    await persistedEventService.addEvents([
      {
        name: "TaskCreated",
        timestamp: Date.now(),
        taskId,
        task: {
          description: task.description,
          projectId: task.projectId,
        },
      },
    ]);

    return taskId;
  },
  async getTask(taskId, params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    const task = createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTask(taskId);

    if (!task) {
      throw new TaskNotFoundError(taskId);
    }

    return task;
  },
  async getAllTaskEntries(params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    return createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskEntries();
  },
  async getTaskEntries(taskIds, params) {
    const persistedEventEntries = await persistedEventService.getEventEntries();

    return createPersistedEventEntriesTaskLense(
      persistedEventEntries
    ).selectTaskEntries(taskIds);
  },
  async editTask(taskId, task, params) {
    await persistedEventService.addEvents([
      {
        name: "TaskUpdated",
        timestamp: Date.now(),
        taskId,
        task: {
          description: task.description,
          projectId: task.projectId,
        },
      },
    ]);

    return taskId;
  },
});
