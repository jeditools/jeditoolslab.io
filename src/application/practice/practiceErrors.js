// @flow strict

import type { PracticeId } from "application/practice/practiceTypes";

class PracticeError extends Error {
  name: string = "Practice Error";
}

export class PracticeNotFoundError extends PracticeError {
  constructor(practiceId: PracticeId) {
    super();

    this.message = `Practice with id ${practiceId} was not found.`;
  }
}

export class IllegalPracticeModification extends TypeError {
  message: string = "Illegal practice modification.";
}

export class PracticeWithNameAlreadyExistsError extends PracticeError {
  constructor(practiceName: string) {
    super();

    this.message = `Practice with name "${practiceName}" already exists.`;
  }
}
