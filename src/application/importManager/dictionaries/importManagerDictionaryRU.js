// @flow strict

import type { ImportManagerDictionary } from "application/importManager/importManagerTypes";

export const importManagerDictRU: ImportManagerDictionary = {
  sectionLabelText: "Импорт данных",
  actionLabelText: "Загрузить данные",
  descriptionText:
    "Вы можете загрузить ранее скачанные данные. Имейте в виду, что будут перезаписаны текущие данные приложения",
};
