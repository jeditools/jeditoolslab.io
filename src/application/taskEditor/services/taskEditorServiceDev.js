// @flow strict

import type { TaskEditorService } from "../taskEditorTypes";

import { normalizeTaskEditorFormData } from "application/taskEditorForm/taskEditorFormUtils";
import { taskServiceDev } from "../../task/services/taskServiceDev";
import { projectServiceDev } from "../../project/services/projectServiceDev";
import { devServicesAsyncTimeoutNormal } from "../../services/servicesUtils";

export const taskEditorServiceDev: TaskEditorService = {
  async getData(editedTaskId) {
    await devServicesAsyncTimeoutNormal.start();

    const projectEntries = await projectServiceDev.getAllProjectEntries();

    let editedTaskEntry = null;

    if (editedTaskId) {
      const editedTask = await taskServiceDev.getTask(editedTaskId);

      editedTaskEntry = [editedTaskId, editedTask];
    }

    return { projectEntries, editedTaskEntry };
  },
  async submitFormData(editedTaskId, formData) {
    await devServicesAsyncTimeoutNormal.start();

    const task = normalizeTaskEditorFormData(formData).toTask();

    let taskId;

    if (editedTaskId) {
      taskId = await taskServiceDev.editTask(editedTaskId, task);
    } else {
      taskId = await taskServiceDev.addTask(task);
    }

    return taskId;
  },
};
