// @flow strict

import React from "react";

import { useNextActivity } from "application/activity/activityHooks";
import { useEventDispatcher } from "application/event/eventHooks";
import { useServices } from "application/services/servicesHooks";
import {
  useSyncEnabled,
  useShouldUpdateSync,
} from "application/sync/syncHooks";
import { useConnectionProcess } from "../connection/connectionHooks";
import { useSyncProcess } from "../sync/syncHooks";
import { useSafeAsyncController } from "../../utility/safeAsyncController/safeAsyncControllerHooks";

export const useRootProcess = () => {
  // Shared

  const { wrapAsyncSafely } = useSafeAsyncController();

  const dispatch = useEventDispatcher();

  const {
    syncService,
    themeService,
    updateService,
    migrationService,
    documentService,
  } = useServices();

  const syncEnabled = useSyncEnabled();

  const shouldUpdateSync = useShouldUpdateSync();

  // Activity process

  const nextActivity = useNextActivity();

  React.useLayoutEffect(() => {
    if (nextActivity) {
      dispatch({
        name: "ROOT__NEXT_ACTIVITY_RECIEVED",
        nextActivity,
      });
    }
  }, [dispatch, nextActivity]);

  React.useLayoutEffect(() => {
    if (nextActivity) {
      window.scrollTo({ top: 0 });
    }
  }, [dispatch, nextActivity]);

  // Sync status process

  React.useLayoutEffect(() => {
    if (shouldUpdateSync) {
      wrapAsyncSafely(
        syncService.read().then((value) => {
          dispatch({
            name: "ROOT__SYNC_RECIEVED",
            sync: value,
          });
        })
      );
    }
  }, [syncService, dispatch, wrapAsyncSafely, shouldUpdateSync]);

  // Theme process

  React.useLayoutEffect(() => {
    wrapAsyncSafely(
      themeService.getPersistedThemeMode().then((value) => {
        dispatch({
          name: "ROOT__PERSISTED_THEME_MODE_RECIEVED",
          persistedThemeMode: value,
        });
      })
    );
  }, [themeService, dispatch, wrapAsyncSafely]);

  // Update process

  React.useLayoutEffect(() => {
    wrapAsyncSafely(
      updateService.subscribe((value) => {
        dispatch({
          name: "ROOT__UPDATE_STATUS_RECIEVED",
          updateStatus: value,
        });
      })
    );
  }, [themeService, dispatch, wrapAsyncSafely, updateService]);

  // MigrationProcess

  React.useLayoutEffect(() => {
    wrapAsyncSafely(
      migrationService.complete().then(() => {
        dispatch({
          name: "ROOT__MIGRATION_COMPLETE",
        });
      })
    );
  }, [dispatch, wrapAsyncSafely, migrationService, syncEnabled]);

  // Visibility process

  React.useEffect(() => {
    return documentService.listenToVisibilityChange((visible) => {
      dispatch({
        name: "ROOT__DOCUMENT_VISIBILITY_CHANGED",
        visible,
      });
    });
  }, [dispatch, documentService]);

  // Connection status process

  React.useEffect(() => {
    return documentService.listenToConnectionStatusChange((online) => {
      dispatch({
        name: "CONNECTION__STATUS_CHANGED",
        online,
      });
    });
  }, [dispatch, documentService]);

  useConnectionProcess();

  useSyncProcess();
};
