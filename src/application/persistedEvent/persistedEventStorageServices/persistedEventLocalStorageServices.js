// @flow strict

import type { PersistedEventStorageService } from "application/persistedEvent/persistedEventTypes";

import { PERSISTED_EVENT__STORAGE_KEY } from "application/persistedEvent/persistedEventConstants";

export const persistedEventLocalStorageService: PersistedEventStorageService = {
  async write(persistedEventEntries) {
    const persistedEventEntriesString = JSON.stringify(persistedEventEntries);

    localStorage.setItem(
      PERSISTED_EVENT__STORAGE_KEY,
      persistedEventEntriesString
    );
  },
  async read() {
    const eventEntriesString = localStorage.getItem(
      PERSISTED_EVENT__STORAGE_KEY
    );

    if (eventEntriesString) {
      const eventEntries = JSON.parse(eventEntriesString);

      return eventEntries;
    } else {
      await persistedEventLocalStorageService.write([]);

      return persistedEventLocalStorageService.read();
    }
  },
};
