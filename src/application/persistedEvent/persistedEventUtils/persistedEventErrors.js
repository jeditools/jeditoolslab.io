// @flow strict

export class PersistedEventError extends Error {
  name: string = "Persisted Event Error";
}
