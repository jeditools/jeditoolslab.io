// @flow strict

import * as React from "react";

import "./textStyles.css";

import { createClassName } from "utility/className/classNameUtils";

type TextSize = "s" | "l";

type TextColor = "secondary" | "accent" | "critical";

type Props = {|
  children: React.Node,
  size?: TextSize,
  color?: TextColor,
|};

export const TextComponent = (props: Props): React.Node => {
  const { children, size = "m", color = "primary" } = props;

  const className = createClassName("text")({ size, color });

  return <span className={className}>{children}</span>;
};
